layui.use(['laydate', 'form', 'layer', 'table', 'laytpl', 'jquery'], function () {
    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        laytpl = layui.laytpl,
        table = layui.table,
        laydate = layui.laydate,
        $ = layui.jquery;

    // 当前页面Bogy对象
    var $body = $('body');

    form.on('submit(export)',function(data){
        var searchData = $(".search-form").serializeArray()
            ,search = {}
            ,param = '';

        searchData.forEach(function(v){
            search[v.name] = v.value?v.value:'';
        })

        if(search.ord_route){
            search.ord_route = selectMul().values;
        }
        for(let i in search){

            if(typeof search[i]=='object'){
                search[i] = search[i].join(',');
            }
            param += param?'&'+i+'='+search[i]:i+'='+search[i];
        }
        window.location = $(".search-form").attr('action')+(param?'?'+param:'');
        return false;
    })

})

//select多选
function selectMul(){
    var vals = [],
        texts = [];
    $('select[multiple] option:selected').each(function() {
        vals.push($(this).val());
        texts.push($(this).text());
    })
    console.log({text:texts,values:vals});
    return {text:texts,values:vals};
}

function searchData() {
    var searchData = $(".search-form").serializeArray()
        ,search = {};

    searchData.forEach(function(v){
        search[v.name] = v.value?v.value:'';
    })

    if(search.ord_route){
        search.ord_route = selectMul().values;
    }
    return search;
}