<?php
/**
 * Created by PhpStorm.
 * User: pw
 * Date: 2018/12/3
 * Time: 16:28
 */
return [
    'jd' => [
        'access_token' => '82e1a563d8fc46fd845f2298a84e7d78djlm',//一年有效
        'appKey' => '559E8281467C031094A39E3F8A70D0F0',//
        'appSecret' => '3a1b14cf1c4f4ad09402d757a010449b',//
        'customerCode'=>'020K292574',//
        'salePlat'=>'0030001',//销售平台
        'wareHouseCode'=>1,//发货仓
        'api'=>[
            'orderApi'=>'jingdong.ldop.waybill.receive',//下单
            'waybillTrailApi'=>'jingdong.ldop.receive.trace.get',//物流轨迹
            'checkJdApi'=>'jingdong.etms.range.check',//是否京配
            'waybillCancel'=>'jingdong.ldop.receive.order.intercept',//拦截运单
        ],
    ],


//    'waybill_status' => ['出库','入库','派件中','签收'],

    'excel_module'=>[
        'order'=>'订单',
    ],

    'excel' => [
        'order' =>[
            'path'=>'/excel/order/' ,
            'model'=>[
                '订单号',
                '运单号',
                '通路',
                '订单日期',
                '姓名',
                '电话',
                '地址',
                '产品编号',
                '产品',
                '数量',
                '订单金额',
                '代收货款金额',
                '规格',
            ],
            'import' => [
                'A' => 'ord_num',
                'B' => 'waybill_num',
                'C' => 'ord_route',
                'D' => 'ord_date|datetime',
//                'C' => 'ord_time|time',
                'E' => 'receiver',
                'F' => 'phone',
                'G' => 'addr',
                'H' => 'good_num',
                'I' => 'good_name',
                'J' => 'quantity',
                'K' => 'ord_money',
                'L' => 'collection_payment',
                'M' => 'param',
//                'K' => 'ord_money',
//                'L' => 'parcel_insurance',//是否保价
//                'M' => 'insurance_prc',//保价金额
//                'N' => 'signReturn',//签还单
//                'O' => 'collection_payment',//代收货款
            ],
        ],
        'waybill'=>[
            'path'=> '/excel/waybill/',
            'model'=>[
                    '订单号','运单号','平台订单号','换单单号','状态','付费方式','是否超区','超区原因','取件码','代收货款',
                    '下单人','下单账号','下单人部门','寄件人','寄件人手机','寄件人公司','收件人','收件人手机',
                    '收件人地址','收件人公司','包裹数量','备注','下单时间','业务类型','物品名','生鲜温层',
                    '返单类型','是否保价','保价金额','预估运费','预计送达','下单重量(kg)','是否拒收','状态更新时间',
                ],
            'import' => [
                'A' => 'ord_num',
                'B' => 'waybill_num',
                'C' => 'platform_num',
                'D' => 'change_num',
                'E' => 'status_text',
                'F' => 'pay_type',
                'G' => 'over_reason',
                'H' => 'over_area',
                'I' => 'pickup_code',
                'J' => 'collection_payment',
                'K' => 'order_by',
                'L' => 'waybill_account',
                'M' => 'order_by_department',
                'N' => 'sender',
                'O' => 'sender_phone',
                'P' => 'sender_company',
                'Q' => 'reciever',
                'R' => 'reciever_phone',
                'S' => 'reciever_addr',
                'T' => 'reciever_company',
                'U' => 'package_num',
                'V' => 'remark',
                'W' => 'waybill_at|datetime',
                'X' => 'business_type',
                'Y' => 'good_name',
                'Z' => 'fresh',
                'AA' => 'waybill_back',
                'AB' => 'parcel_insurance',
                'AC' => 'insurance_prc',
                'AD' => 'predict_fee',
                'AE' => 'predict_arrive_at|datetime',
                'AF' => 'weight',
                'AG' => 'refuse',
                'AH' => 'update_at|datetime',
            ],
        ],
        'finance' =>[
            'path'=>'/excel/finance/' ,
            'model'=>[
                '计费时间',
                '订单号',
                '应付',
            ],
            'import' => [
                'A' => 'balance_at|datetime',
                'B' => 'ord_num',
                'C' => 'collect_fee',
            ],
        ],
        'finance_good' =>[
            'path'=>'/excel/finance_good/' ,
            'model'=>[
                '订单号',
                '商品编码',
                '商品单价',
                '商品数量',
                '订单实付金额',
                '结算时间',
            ],
            'import' => [
                'A' => 'ord_num',
                'B' => 'spu_num',
                'C' => 'good_price',
                'D' => 'good_quantity',
                'E' => 'balance_money',
                'F' => 'balance_at|datetime',
            ],
        ],
        'trade' =>[
            'path'=>'/excel/trade/' ,
            'model'=>[
                '运单号码','退回件关联运单号','揽收时间','计费重量','费用类型'/*,'实际体积(cm3)'*/,'应付金额','结算金额','运单最终状态'
            ]
            ,
            'import' => [
                'A' => 'waybill_num',
                'B' => 'back_waybill_num',//退回件关联运单号
                'C' => 'balance_at|datetime',//揽收时间
                'D' => 'balance_weight',//计费重量
                'E' => 'fee_type',//费用类型
                'F' => 'fee',//费用金额
                'G' => 'balance_fee',//结算金额
                'H' => 'jd_trade_type',//运单最终状态
//                'L' => 'balance_volume_weight',//体积重
//                'N' => 'trade_fee',
//                'O' => 'service_fee',
//                'P' => 'save_fee',
//                'R' => 'poundage',
//                'S' => 'package_fee',//包装费2019
//                'T' => 'addition_fee',//高峰期附加费2019
//                'V' => 'damages',//赔偿费2019
//                'W' => 'pay',//应付2019
//                'U' => 'discount',//折扣费2019
//                'S' => 'discount',//折扣费2018
//                'T' => 'damages',//赔偿费2018
//                'U' => 'pay',//应付2018
//                'balance_volume',//体积
            ],
        ],
        'waybillSF'=>[
            'path'=> '/excel/waybillSF/',
            'model'=>[
                '订单号','快递名称','运单号'
            ],
            'import' => [
                'A' => 'ord_num',
                'B' => 'deliver_name',
                'C' => 'waybill_num',
            ],
        ],
        'product'=>[
            'path'=> '/excel/product/',
            'model'=>[
                '商品名','商品类型','商品编码','供应商'/*,'规格'*/
            ],
            'import' => [
                'A' => 'spu_name',
                'B' => 'spu_type',
                'C' => 'spu_num',
                'D' => 'supplier_id'
            ],
        ],
    ],


    'waybill_end'=>[
        '拒收',
        '换单打印',
        '妥投',
        '下单取消',
        '终止揽收',
        '上门接货退货完成',
        '退货运单打印',
    ],

    'trade_status'=>[
        -2=>'已取消',
        -1=>'拒收',
        0=>'待出库',
        1=>'在途',
        2=>'妥投',
    ],

    'balance'=>[
        1=>'是',
        0=>'否',
    ],

    'yORn'=>[
        1=>'是',
        0=>'否',
    ],

    'log_type'=>[
        1=>'商品库存',
    ],

    'ord_type'=>[
        1=>'销售单',
        2=>'换货单',
        3=>'预售单',
        4=>'取消单',
        5=>'有赞异常单',
        6=>'异常单',
        7=>'非电购订单',
    ],

    'ord_route'=>[
        1=>'佛山TV',
//        2=>'外呼通路',
//        3=>'电商通路',
//        4=>'其他通路',
//        5=>'东莞通路',
        5=>'顺德TV',
        6=>'有赞',

    ],

    'wx_pay_route'=>[
        6,7,9
    ],

    'callType'=>[
        1=>'呼入',
        2=>'呼出',
    ],

    'callResult'=>[
        1=>'接通',
        2=>'无人接听',
        3=>'电话忙',
        4=>'空号',
        5=>'关机',
        6=>'停机',
        7=>'其他错误',
        21=>'坐席全忙挂机',
        22=>'坐席未接听',
        23=>'坐席拒绝',
        24=>'坐席未登录挂机',
        25=>'用户挂机',
        26=>'坐席被抢接',
        28=>'未按转人工键挂机',
        29=>'用户留言',
        30=>'振铃中挂机',
        31=>'转接中挂机',
        32=>'转接外线成功',
        33=>'转接外线失败',
    ],

    'jd_fee_type'=>[
//        'trade_fee'=>'运费-收派服务费',
        'trade_fee'=>'快递运费',
        'back_trade_fee'=>'拒收快递运费',
//        'poundage'=>'代收货款手续费-收派服务费',
        'poundage'=>'快递代收手续费',
        'save_fee'=>'保价费-收派服务费',
        'damages'=>'丢失赔偿',
        'trade_damages'=>'运费赔偿-收派服务费',
    ],

    'traffic_status'=>[
        1=>'已处理',
        2=>'未处理',
        3=>'已回访',
        4=>'已回访未接通',
    ],

    'traffic_type'=>[
//        1=>'售前',
//        2=>'售后',
//        3=>'外呼',
        0=>'未知',
        1=>'订购成功',
        2=>'订购失败',
        3=>'物流投诉',
        4=>'质量客诉',
        5=>'售后咨询',
        6=>'退换货',
        7=>'重复来电',
        9=>'物流咨询',
        8=>'其他',
    ],

    'time'=>[
        'date'=>'日期',
        'hour'=>'时段',
    ],

    'out_instruction'=>[
        -1=>'取消出库',
        0=>'待出库',
        1=>'分拣完成',
        2=>'出库完成',
    ],

    'is_cancel'=>[
        '待拦截',
        '已拦截',
    ],

    'balance_type'=>[
        1=>'代销',
        2=>'代收代付',
    ],

    'show_phone_user_id'=>[
        1,2
    ],

    'finance_type'=>[
        1=>'finance',
        2=>'finance_good',
        3=>'trade',
    ],

    'traffic_number'=>[
        '075763551116',
        '075763550462',
        '075706'
    ],

    'promise_time_type'=>[
        1=>'特惠送',
        2=>'特快送',
        4=>'城际闪送',
        5=>'同城当日达',
        6=>'次晨达',
        7=>'微小件',
        8=>'生鲜专送',
        16=>'生鲜特快',
        17=>'生鲜特惠',
    ],

    'trans_type'=>[
        1=>'陆运',
        2=>'航空',
    ],

    'traffic'=>[
        'on_work'=>'09:00:00 - 18:00:00',
        'off_work'=>'18:00:00',
    ],

    'spu_type'=>[
        1=>'电购商品',
        2=>'非电购商品',
    ],

    'traffic_tag'=>[
        1=>'订购成功',
        2=>'订购失败',
        3=>'物流投诉',
        4=>'质量投诉',
        6=>'物流咨询',
        5=>'其他',
    ],

    'pay_mode'=>[
        1=>'代收货款',
        2=>'微信支付',
    ],

    'domain'=>'http://test.yy-erp.cn',
    'traffic_ip'=>'http://112.91.155.117:8090/',
    'limit'=>100,

];