<?php

return [
    // 内置Html Console 支持扩展
    'fs_tv' => [
        'client_id' => '66d88dfcb68463b49b',
        'client_secret' => '91a0b23bc50611312920e8fd17913ac8',
        'debug' => true, // 调试模式
        'kdt_id' => '6022023', // 店铺ID(仅自用模式下填写)
        'exception_as_array' => true, // 错误返回数组还是异常
        'version' => '4.0.0',
        'log' => [
            'name' => 'youzan',
            'file' => $_SERVER['DOCUMENT_ROOT'].'/../runtime/log/youzan.log',
            'level'      => 'debug',
            'permission' => 0777,
        ]
    ],
];