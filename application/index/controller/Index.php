<?php

namespace app\index\controller;


use app\common\service\JdService;
use app\common\service\WechatPaymentService;
use think\Controller;
use Endroid\QrCode\QrCode;

class Index extends Controller {

    /**
     * 网站首页
     * @return mixed
     */
    public function index() {
        $res = (new WechatPaymentService())->order([
            'body'=>'测试',
            'out_trade_no'=>time().uniqid().rand(10000,99999),
            'product_id'=>'a',
            'total_fee'=>1,
//            'notify_url'=>'',
            'trade_type'=>'NATIVE',
//            'openid'=>'',
        ]);
        dump($res);
        $qrCode = new QrCode($res['code_url']);
//        header('Content-Type: '.$qrCode->getContentType());
        echo '<img src="'.$qrCode->writeDataUri().'" width="250">';
//		return $this->fetch();
    }
}
