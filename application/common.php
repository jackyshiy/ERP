<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

use app\common\model\Route;
use app\common\service\NodeService;
//use think\Request;
use app\common\service\YouZanService;
use think\facade\Request;
use app\common\service\AuthService;
use think\facade\Cache;
use app\common\model\Config;
use app\common\model\SystemLog;
use Overtrue\Pinyin\Pinyin;
use app\common\model\Order;
use app\common\model\OrdDetail;
use app\common\model\Waybill;
use app\common\model\Member;
use app\common\model\MemberAddress;
use app\common\model\Products;
use app\common\model\Spu;
use app\common\model\OrgModel;
use app\common\model\Warehouse;
use app\common\model\Traffic;
use app\common\service\JdService;

if (!function_exists('check_login')) {

    /**
     * 检测前端用户是否登录
     */
    function check_login() {
        if (empty(session('user'))) {
            return false;
        } else {
            return true;
        }
    }
}

if (!function_exists('auth')) {

    /**
     * 权限节点判断
     * @param $node 节点
     * @return bool （true：有权限，false：无权限）
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    function auth($node) {
        return AuthService::checkNode($node);
    }
}

if (!function_exists('parseNodeStr')) {

    /**
     * 驼峰转下划线规则
     * @param string $node
     * @return string
     */
    function parseNodeStr($node) {
        $tmp = [];
        foreach (explode('/', $node) as $name) {
            $tmp[] = strtolower(trim(preg_replace("/[A-Z]/", "_\\0", $name), "_"));
        }
        return trim(join('/', $tmp), '/');
    }
}

if (!function_exists('password')) {

    /**
     * 密码加密算法
     * @param $value 需要加密的值
     * @param $type  加密类型，默认为md5 （md5, hash）
     * @return mixed
     */
    function password($value) {
        $value = sha1('blog_') . md5($value) . md5('_encrypt') . sha1($value);
        return sha1($value);
    }

}

if (!function_exists('__buildData')) {

    /**
     * 构建数据
     * @param $data   模型数据
     * @param $method 模型方法
     */
    function __buildData(&$data, $method) {
        foreach ($data as &$vo) {
            $vo->$method;
        }
    }
}

if (!function_exists('alert')) {

    /**
     * 弹出层提示
     * @param string $msg  提示信息
     * @param string $url  跳转链接
     * @param int    $time 停留时间 默认2秒
     * @param int    $icon 提示图标
     * @return string
     */
    function alert($msg = '', $url = '', $time = 3, $icon = 6) {
        $success = '<meta name="renderer" content="webkit">';
        $success .= '<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">';
        $success .= '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">';
        $success .= '<script type="text/javascript" src="/static/plugs/jquery/jquery-2.2.4.min.js"></script>';
        $success .= '<script type="text/javascript" src="/static/plugs/layui-layer/layer.js"></script>';
        if (empty($url)) {
            $success .= '<script>$(function(){layer.msg("' . $msg . '", {icon: ' . $icon . ', time: ' . ($time * 1000) . '});})</script>';
        } else {
            $success .= '<script>$(function(){layer.msg("' . $msg . '",{icon:' . $icon . ',time:' . ($time * 1000) . '});setTimeout(function(){self.location.href="' . $url . '"},2000)});</script>';
        }
        return $success;
    }
}

if (!function_exists('msg_success')) {

    /**
     * 成功时弹出层提示信息
     * @param string $msg  提示信息
     * @param string $url  跳转链接
     * @param int    $time 停留时间 默认2秒
     * @param int    $icon 提示图标
     * @return string
     */
    function msg_success($msg = '', $url = '', $time = 3, $icon = 1) {
        return alert($msg, $url, $time, $icon);
    }
}

if (!function_exists('msg_error')) {

    /**
     * 失败时弹出层提示信息
     * @param string $msg  提示信息
     * @param string $url  跳转链接
     * @param int    $time 停留时间 默认2秒
     * @param int    $icon 提示图标
     * @return string
     */
    function msg_error($msg = '', $url = '', $time = 3, $icon = 2) {
        return alert($msg, $url, $time, $icon);
    }
}

if (!function_exists('clear_menu')) {

    /**
     * 清空菜单缓存
     */
    function clear_menu() {
        Cache::clear('menu');
    }
}


if (!function_exists('clear_basic')) {

    /**
     * 清空菜单缓存
     */
    function clear_basic() {
        Cache::clear('basic');
    }
}

if (!function_exists('get_ip')) {

    /**
     * 获取用户ip地址
     * @return array|false|string
     */
    function get_ip() {
        $ip = false;
        if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ips = explode(", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
            if ($ip) {
                array_unshift($ips, $ip);
                $ip = false;
            }
            for ($i = 0; $i < count($ips); $i++) {
                if (!eregi("^(10│172.16│192.168).", $ips[$i])) {
                    $ip = $ips[$i];
                    break;
                }
            }
        }
        return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
    }
}

if (!function_exists('get_location')) {

    /**
     * 根据ip获取地理位置
     * @param string $ip
     * @return mixed
     */
    function get_location($ip = '') {
        empty($ip) && $ip = get_ip();
        $url = "http://ip.taobao.com/service/getIpInfo.php?ip={$ip}";
        $ret = file_get_contents($url);
        $arr = json_decode($ret, true);
        return $arr['data'];
    }
}

if (!function_exists('install_substring')) {

    /**
     * 格式化安装配置信息
     * @param     $str
     * @param     $lenth
     * @param int $start
     * @return string
     */
    function install_substring($str, $lenth, $start = 0) {
        $len = strlen($str);
        $r = [];
        $n = 0;
        $m = 0;

        for ($i = 0; $i < $len; $i++) {
            $x = substr($str, $i, 1);
            $a = base_convert(ord($x), 10, 2);
            $a = substr('00000000 ' . $a, -8);

            if ($n < $start) {
                if (substr($a, 0, 1) == 0) {
                } elseif (substr($a, 0, 3) == 110) {
                    $i += 1;
                } elseif (substr($a, 0, 4) == 1110) {
                    $i += 2;
                }
                $n++;
            } else {
                if (substr($a, 0, 1) == 0) {
                    $r[] = substr($str, $i, 1);
                } elseif (substr($a, 0, 3) == 110) {
                    $r[] = substr($str, $i, 2);
                    $i += 1;
                } elseif (substr($a, 0, 4) == 1110) {
                    $r[] = substr($str, $i, 3);
                    $i += 2;
                } else {
                    $r[] = ' ';
                }
                if (++$m >= $lenth) {
                    break;
                }
            }
        }
        return join('', $r);
    }
}

if (!function_exists('parse_sql')) {

    /**
     * 格式化导入的sql语句
     * @param string $sql
     * @param int    $limit
     * @return array|string
     */
    function parse_sql($sql = '', $limit = 0) {
        if ($sql != '') {
            // 纯sql内容
            $pure_sql = [];

            // 多行注释标记
            $comment = false;

            // 按行分割，兼容多个平台
            $sql = str_replace(["\r\n", "\r"], "\n", $sql);
            $sql = explode("\n", trim($sql));

            // 循环处理每一行
            foreach ($sql as $key => $line) {
                // 跳过空行
                if ($line == '') {
                    continue;
                }

                // 跳过以#或者--开头的单行注释
                if (preg_match("/^(#|--)/", $line)) {
                    continue;
                }

                // 跳过以/**/包裹起来的单行注释
                if (preg_match("/^\/\*(.*?)\*\//", $line)) {
                    continue;
                }

                // 多行注释开始
                if (substr($line, 0, 2) == '/*') {
                    $comment = true;
                    continue;
                }

                // 多行注释结束
                if (substr($line, -2) == '*/') {
                    $comment = false;
                    continue;
                }

                // 多行注释没有结束，继续跳过
                if ($comment) {
                    continue;
                }

                // sql语句
                array_push($pure_sql, $line);
            }

            // 只返回一条语句
            if ($limit == 1) {
                return implode($pure_sql, "");
            }

            // 以数组形式返回sql语句
            $pure_sql = implode($pure_sql, "\n");
            $pure_sql = explode(";\n", $pure_sql);
            return $pure_sql;
        } else {
            return $limit == 1 ? '' : [];
        }
    }
}

if (!function_exists('curl')) {

    /**
     * 模拟请求
     * @return \app\common\service\CurlService
     */
    function curl() {
        return new \tool\Curl();
    }
}

if (!function_exists('upload')) {

    /**
     * 上传文件
     * @return \app\common\service\CurlService
     */
    function upload($file,$path,$validate=[],$rule='date') {

        // 移动到框架应用根目录/uploads/ 目录下

        if(!$file){
            return __error('没有上传文件');
        }

        $info = $file->validate($validate)->rule($rule)->move($path);
//        dd($info->error);


        if($info){

            // 成功上传后 获取上传信息
            return __successData('上传成功',['ext'=>$info->getExtension(),'saveName'=>$info->getSaveName(),'fileName'=>$info->getFilename(),]);

        }else{

            // 上传失败获取错误信息
            return __error('上传失败:'.$file->getError());

        }
    }
}

if (!function_exists('excelImport')) {

    /**
     * 导入excel
     * @return \app\common\service\CurlService
     */
    function excelImport($file,$match=[],$limit=0,$row=2) {
        if(!$file){
            return __error('文件不存在');
        }
        $ext = config('excel_ext');
        $file_ext = pathinfo($file)['extension'];
        if(!in_array($file_ext,$ext)){
            return __error('文件类型错误');
        }
        if($file_ext=='xlsx'){
            $reader = PHPExcel_IOFactory::createReader('Excel2007');
        }elseif($file_ext=='xls'){
            $reader = PHPExcel_IOFactory::createReader('Excel5');
        }else{
            return __error('无法读取文件');
        }
        //载入excel文件
        $excel = $reader->load($file,$encode = 'utf-8');
        //读取第一张表
        $sheet = $excel->getSheet(0);
        //获取总行数
        $row_num = $sheet->getHighestRow();
        //获取总列数
        $col_num = $sheet->getHighestColumn();
        $data = []; //数组形式获取表格数据
//        dd($match);
        for($i=$row;$i<=$row_num;$i++){
            if($match){
//                dd($match);
                foreach (array_keys($match) as $v){
                    if($row==1){
                        $arr[] = trim($excel->getActiveSheet()->getCell($v.$i)->getFormattedValue());
                    }else{

                        if(strpos($match[$v],'|')!=false){
                            $type = explode('|',$match[$v]);
                            $format = '';
                            switch ($type[1]){
                                case 'time':
                                    $format = 'H:i:s';
                                    break;
                                case 'date':
                                    $format = 'Y-m-d';
                                    break;
                                case 'datetime':
                                    $format = 'Y-m-d H:i:s';
                                default:
                                    break;
                            }
                            $val = trim($excel->getActiveSheet()->getCell($v.$i)->getValue());
//                            dd($val);
                            if(is_numeric($val)){
                                $arr[$type[0]] = gmdate($format,PHPExcel_Shared_Date::ExcelToPHP($val));
                            }elseif(strpos($val,'.')!=false){
                                $arr[$type[0]] = str_replace('.','-',$val);
                            }else{
                                $arr[$type[0]] = $val;
                            }
//                        if(!$val)
//                            return __error('数据日期格式错误');

                        }else{
                            $arr[$match[$v]] = trim($excel->getActiveSheet()->getCell($v.$i)->getFormattedValue());
                        }
                    }



                }
                if($limit>1){
                    $data[] = $arr;
                }else{
                    $data = $arr;
                }
//                dd($data);die;
            }else{
                $col_num = PHPExcel_Cell::columnIndexFromString($col_num);
                for($j=1;$j<=$col_num;$j++){
                    $data[] = trim($excel->getActiveSheet()->getCell(PHPExcel_Cell::stringFromColumnIndex($j).$i)->getValue());
                }
            }

            if($limit && $i-$row>=$limit-1){
                break;
            }
        }
        return __successData('',['total'=>$row_num,'data'=>$data]);
    }
}

if (!function_exists('excelExport')) {

    /**
     * 导出excel
     * @return \app\common\service\CurlService
     */
    function excelExport($file,$match,$limit=0) {

    }
}

if (!function_exists('put_csv')) {

    /**
     * 导出excel
     * @return \app\common\service\CurlService
     */
    function put_csv($list,$title,$match=[]) {
        $file = fopen('php://output',"a");
        $limit = 500;
        $calc = 0;
        foreach ($title as $v){
            $tit[] = iconv('UTF-8', 'GB2312//IGNORE',$v);
        }
        fputcsv($file,$tit);
        foreach ($list as $v){
            $calc++;
            if($limit == $calc){
                ob_flush();
                flush();
                $calc = 0;
            }

            $v = array_replace(array_flip($match),(array)$v);
//            halt($v);
            foreach($v as $t){
                $tarr[] = iconv('UTF-8', 'GB2312//IGNORE',$t);
            }
            fputcsv($file,$tarr);

            unset($tarr);
        }
        unset($list);
        fclose($file);
        exit();
    }
}

if (!function_exists('setSearchTime')) {

    /**
     * 导出excel
     * @return \app\common\service\CurlService
     */
    function setSearchTime($time,$totime=true) {
        if(strpos($time,' - ')!==false){
            $time = explode(' - ',$time);
//            dd($time);die;
            if($totime){
                if(strpos($time[0],' ')!==false && strpos($time[1],' ')!==false){
                    $tt0 = explode(' ',$time[0]);
                    $tt1 = explode(' ',$time[1]);

                    $start = str_replace(':','',$tt0[1])>0?strtotime($time[0]):strtotime($tt0[0]);
                    $end = str_replace(':','',$tt1[1])>0?strtotime($time[1]):strtotime($tt1[0].' 23:59:59');
                    return [$start,$end];
                }
                return [strtotime($time[0]),strtotime($time[1].' 23:59:59')];
            }

            return [$time[0],$time[1]];
        }else{
            return strtotime($time);
        }
    }
}

function setWhere($where='',$add,$condition='AND'){
    return $where?$where." $condition $add":$add;
}

if (!function_exists('yesOrNo')) {

    /**
     * 导出excel
     * @return \app\common\service\CurlService
     */
    function yesOrNo($val) {
        if($val==1){
            return '是';
        }else{
            return '否';
        }
    }
}

function setUrl($json,$secret,$method){
    $url = 'https://api.jd.com/routerjson?';
    $param = [
        'access_token'=>$secret['token'],
        'app_key'=>$secret['app_key'],
        'method'=>$method,
        'timestamp'=>date('Y-m-d H:i:s'),
        'v'=>'2.0',
        '360buy_param_json'=>json_encode($json,JSON_UNESCAPED_UNICODE),
    ];
    $str = '';
    $attr = [];
    ksort($param);
//    dd($param);die;
    foreach($param as $k=>$v){
        $str .= "$k$v";
        $attr[] = urlencode($k).'='.urlencode($v);
    }
//    dd($attr);die;
    $sign = strtoupper(md5(config('jd')['appSecret'].$str.config('jd')['appSecret']));
    $url .= implode('&',$attr).'&sign='.$sign;
    return $url;
}

function goCurl($url,$post='',$cookie='', $returnCookie=0){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE); // https请求 不验证证书和hosts
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($curl, CURLOPT_ENCODING, "");
    curl_setopt($curl, CURLOPT_ACCEPT_ENCODING, "gzip,deflate");
//    curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)');
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
//    curl_setopt($curl, CURLOPT_REFERER, "http://XXX");
    if($post) {
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post));
     }
     if($cookie) {
         curl_setopt($curl, CURLOPT_COOKIE, $cookie);
     }
     curl_setopt($curl, CURLOPT_HEADER, $returnCookie);
     curl_setopt($curl, CURLOPT_TIMEOUT, 10);
     curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
     $data = curl_exec($curl);
     if (curl_errno($curl)) {
         $data = curl_error($curl);
     }
     curl_close($curl);
//     if($returnCookie){
//         list($header, $body) = explode("\r\n\r\n", $data, 2);
//         preg_match_all("/Set\-Cookie:([^;]*);/", $header, $matches);
//         $info['cookie']  = substr($matches[1][0], 1);
//         $info['content'] = $body;
//         return $info;
//     }else{
//         return $data;
//    }

    return $data;
}

/**
 * 京东下单
 *
 * @param $data
 */
function JDwaybillOrder($data,$secret){
//    dump($data['parcel_insurance']);
    $rows = [
        "salePlat"=> $data['salePlat'],//销售平台
        "customerCode"=> $data['customerCode'],//商家编码
        "orderId"=> $data['orderId'],//订单号
        "thrOrderId"=> "",
        "senderName"=> $data['senderName'],//寄件人
        "senderAddress"=> $data['senderAddress'],//寄件人地址
        "senderTel"=> '',
        "senderMobile"=> $data['senderMobile'],//寄件人电话
        "senderPostcode"=> "",
        "receiveName"=> $data['reciever'],//收件人
        "receiveAddress"=> $data['addr'],//收件人地址
        "province"=> "",
        "city"=> "",
        "county"=> "",
        "town"=> "",
        "provinceId"=> "",
        "cityId"=> "",
        "countyId"=> "",
        "townId"=> "",
        "siteType"=> "",
        "siteId"=> "",
        "siteName"=> "",
        "receiveTel"=> $data['receiveTel'],
        "receiveMobile"=> $data['phone'],//收件人电话
        "postcode"=> "",
        "packageCount"=> $data['package_num'],//包裹数量
        "weight"=> $data['weight'],//重量
        "vloumLong"=> $data['product_length'],//长cm
        "vloumWidth"=> $data['product_width'],//宽cm
        "vloumHeight"=> $data['product_height'],//高cm
        "vloumn"=> $data['volume'],//体积cm3
        "description"=> $data['product_name'],//商品描述
        "collectionValue"=> $data['collection_payment']>0?1:0,//是否代收货款
        "collectionMoney"=> $data['collection_payment'],//代收货款
        "guaranteeValue"=> isset($data['parcel_insurance'])?$data['parcel_insurance']:0,//是否保价
        "guaranteeValueAmount"=> isset($data['insurance_prc'])?$data['insurance_prc']:0,//保价金额
        "signReturn"=> isset($data['signReturn'])?$data['signReturn']:0,//签还单
        "aging"=> "",
        "transType"=> "",
        "remark"=> '可开箱验货后付款'.' 商品['.$data['product_name'].','.$data['param'].']',
        "goodsType"=> "",
        "orderType"=> "",
        "shopCode"=> "",
        "orderSendTime"=> "",
        "warehouseCode"=> "",
        "areaProvId"=> "",
        "areaCityId"=> "",
        "shipmentStartTime"=> "",
        "shipmentEndTime"=> "",
        "idNumber"=> "",
        "addedService"=> "",
        "extendField1"=> "",
        "extendField2"=> "",
        "extendField3"=> "",
        "extendField4"=> "",
        "extendField5"=> "",
        "senderCompany"=> "",
        "receiveCompany"=> "",
        "goods"=> $data['product_name'],//商品类目，名称
        "goodsCount"=> $data['quantity'],//商品数量
        "freight"=> ""
    ];
//    halt($rows);
//    dd(json_encode($rows,JSON_UNESCAPED_UNICODE));die;
    $url = setUrl($rows,$secret,config('jd')['api']['orderApi']);
//    dd($url);die;
    $res = goCurl($url);
    $res = json_decode($res,true)?:$res;
//    dd($res);die;
    $log = [
        'curl_url'=>urldecode($url),
        'curl_data'=>$rows,
        'response_data'=>$res,
    ];
    curlLog($log);
    if(isset($res['jingdong_ldop_waybill_receive_responce'])){
        if($res['jingdong_ldop_waybill_receive_responce']['code']>0)
            return false;

        return $res['jingdong_ldop_waybill_receive_responce']['receiveorderinfo_result'];
    }
    return false;
}

/**
 * 京东物流查询
 *
 * @param $waybill_num
 * @param int $just_update
 */
function JDwaybillTrade($waybill_num,$secret){
    $rows = [
        "customerCode"=> $secret['customer_code'],
        "waybillCode"=> $waybill_num
    ];

    $url = setUrl($rows,$secret,config('jd')['api']['waybillTrailApi']);
//    dd($url);die;
    $res = json_decode(goCurl($url),true);
//    halt($res);
    if(isset($res['jingdong_ldop_receive_trace_get_responce'])){
        if($res['jingdong_ldop_receive_trace_get_responce']['code']>0)
            return false;
        return $res['jingdong_ldop_receive_trace_get_responce']['querytrace_result'];
    }
    return false;
}


/**
 * 是否京配
 *
 * @param $waybill_num
 * @param int $just_update
 */
function JDcheck($data,$secret){
    $rows = [
        "customerCode"=> $data['customerCode'],
        "orderId"=> $data['ord_num'],//$data['ord_num'],
        "goodsType"=> 1,
        "receiveAddress"=> $data['addr'],//$data['addr'],
        'wareHouseCode'=>$data['ware_house_code'],
    ];
//    dump($rows);
    $url = setUrl($rows,$secret,config('jd')['api']['checkJdApi']);
//    dd($url);die;
    $res = json_decode(goCurl($url),true);
//    halt($res);
    $log = [
        'curl_url'=>urldecode($url),
        'curl_data'=>$rows,
        'response_data'=>$res,
    ];
    curlLog($log);
    if(isset($res['jingdong_etms_range_check_responce'])){
        if($res['jingdong_etms_range_check_responce']['code']>0)
            return false;
        return $res['jingdong_etms_range_check_responce']['resultInfo'];
    }
    return false;
}

function getWeek($dateTime){
    if(!strtotime($dateTime))
        return false;
    return config('week')[date('N',strtotime($dateTime))];
}

function jd_code($param){
    $url = 'https://oauth.jd.com/oauth/authorize?response_type=code&client_id='.$param['app_key'].'&redirect_uri='.config('site_url').'&state='.$param['customer_code'];
    $url = 'https://oauth.jd.com/oauth/authorize?response_type=code&client_id=C105BD5C634BA27F48946BFE6D3046DB&redirect_uri=www.yy-erp.com&state=020K348342';
//    dump($url);
    $res = goCurl($url);
//    dump($res);
    return $res;
}

function jd_token($param){
    $code = jd_code($param);
    $url = 'https://oauth.jd.com/oauth/authorize?response_type='.$code.'&client_id='.$param['app_key'].'&redirect_uri='.config('site_url').'&state='.$param['customer_code'];
    $res = goCurl($url);
    return $res;
}

function jd_refresh_token($param){
    $url = 'https://oauth.jd.com/oauth/token?client_id='.$param['app_key'].'&client_secret='.$param['app_secret'].'&grant_type=refresh_token&refresh_token='.$param['token'];
    $res = json_decode(mb_convert_encoding(goCurl($url), 'utf-8','GB2312'),true);
    halt($res);
    if($res['code']>0)
        return false;
    return true;
}

function org($id){
    $field = [
        'jd_app_key app_key',
        'jd_app_secret app_secret',
        'jd_token token',
        'jd_customer_code customer_code',
        'org_sender_addr addr',
        'jd_ware_house_code ware_house_code',
        'org_sender senderName',
        'org_sender_addr senderAddress',
        'org_sender_phone senderMobile',
    ];
    $org = \app\common\model\OrgModel::field($field)->find($id)->toArray();
//    $org['ord_num'] = '123';
    return ['row'=>$org,'secret'=>['token'=>$org['token'],'app_key'=>$org['app_key']]];
}

function arr_filter(array $arr){
    if(count($arr)==0)
        return [];
    foreach ($arr as $k=>$v){
        if(!is_array($v))
            continue;
        $arr[$k] = array_filter($v);
        arr_filter($arr[$k]);
    }
    return array_filter($arr);
}

/**
 * 成功时返回的信息
 * @param $msg 消息
 * @return \think\response\Json
 */
function __successData($msg,$data=[],$isPage=0,$count=0,$limit=10,$page=1) {
    if($isPage)
        return [
            'code' => 0,
            'msg' => $msg,
            'data'=>$data,
            'count'=>$count,
            'info'=>[
                'limit'=>$limit,
                'page'=>$page,
                'page_sum'=>ceil($count / $limit),
            ]
        ];

    return [
        'code' => 0,
        'msg' => $msg,
        'data'=>$data,
    ];
}

/**
 * 错误时返回的信息
 * @param $msg 消息
 * @return \think\response\Json
 */
function __error($msg,$data=[]) {
    return ['code' => 1, 'msg' => $msg,'data'=>$data,];
}

/**
 * 成功时返回的信息
 * @param $msg 消息
 * @return \think\response\Json
 */
function __success($msg, $data = '',$isPage = 0,$count=0,$limit=0,$page=0,$totalPage=0)
{
    return $isPage
        ?['code' => 0, 'msg' => $msg, 'data' => $data,'count'=>$count,'info'=>['limit'=>$limit,'page_current'=>$page,'page_sum'=>$totalPage]]
        :['code' => 0, 'msg' => $msg, 'data' => $data];
}

function getAddrName($id){
    $row = \app\common\model\SystemAddress::where('id',$id)->value('name');
    return $row?:'';
}

function getAddrId($name){
    $row = \app\common\model\SystemAddress::where('name',$name)->value('id');
    return $row?:'';
}

/**
 * 库存记录
 * @param $data
 * @return bool
 */
function stock_log($data){
    $stock_log = [
        'log_type'=>'stock',
        'product_id'=>$data['sku_id'],
        'num'=>$data['stock'],
        'log_detail'=>$data['detail']//"商品 $data[spu_name] ，首次添加 ，良品库存 $data[stock] ，不良品库存0",
    ];
    $r = SystemLog::create(save_common_field($stock_log));
    return $r?true:false;
}

/**
 * 中文转拼音
 *
 * @param $str                  汉字
 * @param string $delimiter     分隔符
 * @return string
 */
function toPinYin($str,$delimiter=''){
    $pinyin = new Pinyin();
    return $pinyin->permalink($str,$delimiter);
}

function setSkuParam(array $data,array $spu_param){

    foreach ($data as $k=>$v){
        $data[$k]['param'] = '';
        if($spu_param)
            foreach($spu_param as $key=>$val){
                $data[$k]['param'] .= $val['name'].':'.$v['sku_param'][toPinYin($val['name'])].' ';
            }
        $data[$k]['param'] = $data[$k]['param']?:'无';
    }
    return $data;
}

/**
 * 接口调用记录
 *
 * @param $log      记录数据
 */
function curlLog($log){
//    $row = [
//        'curl_url'=>$log['curl_url'],
//        'curl_data'=>$log['curl_data'],
//        'response_data'=>$log['response_data'],
//    ];
    \app\common\model\CurlLog::create($log);
}

/**
 * 运单取消
 *
 * @param $data     运单信息
 * @param $secret   接口签名
 * @return bool
 */
function waybill_cancel($waybill_id,$cancel_reason='拦截'){
    $waybill = Waybill::find($waybill_id);
    if(!$waybill)
        return false;
    if($waybill['status']==2)
        return false;
    $secret = getJDapiSecret($waybill['deliver_id']);
    $rows = [
        "vendorCode"=> $secret['customer_code'],
        "deliveryId"=> $waybill['waybill_num'],//$data['ord_num'],
        "interceptReason"=> $cancel_reason,
    ];
//    dump(config('jd')['api']['waybillCancel']);
    $url = setUrl($rows,$secret,config('jd')['api']['waybillCancel']);
//    dd($url);die;
    $res = json_decode(goCurl($url),true);
//    halt($res);
    if(isset($res['jingdong_ldop_receive_order_intercept_responce'])){
        if($res['jingdong_ldop_receive_order_intercept_responce']['code']>0)
            return false;
        return $res['jingdong_ldop_receive_order_intercept_responce']['resultInfo'];
    }
    return false;
}

/**
 * 查找京东车队接口签名信息
 *
 * @param $delivery_id      车队ID
 * @return array
 */
function getJDapiSecret($delivery_id){
    $delivery = \app\common\model\OrgModel::find($delivery_id);
    $secret = [
        'token'=>$delivery['jd_token'],
        'app_key'=>$delivery['jd_app_key'],
        'customer_code'=>$delivery['jd_customer_code'],
    ];
    return $secret;
}

/**
 * 运费比对数据处理
 *
 * @param $data     计算运费数据
 * @return mixed
 */
function charge($data){
    $weight = ceil($data['weight']);
    $volume = $data['spu_volume'];
    $volume_weight = ceil($volume / 8000);
    $data['fee_weight'] = $weight > $volume_weight ? $weight : $volume_weight;
    $data['is_gz'] = strpos($data['reciever_addr'], '广州') !== false?1:0;

    //运费
    $data['trade_fee'] =  $data['is_gz']? 10 + ($data['fee_weight'] - 1) * 2  : 12 + ($data['fee_weight'] - 1) * 2 ;

    //代收货款手续费
    $poundage = $data['collection_payment'] * (time()>=strtotime(date('2019-12-01 00:00:00'))?0.01:0.008);
    $data['trade_poundage'] = 0<$poundage && $poundage < 1 ? 1 : round($poundage, 1);
    //保价服务费
//    $data['sys_service_fee'] = $data['parcel_insurance'] ? 1 : 0;

    return $data;
}

function tradeCharge($data){
    if($data['fee_type']=='trade_fee'){
        $secret = getJDapiSecret($data['deliver_id']);
        $logistics = JDwaybillTrade($data['waybill_num'],$secret);
        $time = count($logistics['data'])>0?$logistics['data'][0]['opeTime']:'';//jd揽收时间

        if(date('Y-m-d',strtotime($data['balance_at'])) == date('Y-m-d',strtotime($time)) || $data['jd_trade_type']=='妥投' || $data['jd_trade_type']==''){//揽收出库
            $data['sys_fee'] =  $data['trade_fee'];
        }elseif(date('Y-m-d',strtotime($data['balance_at'])) > date('Y-m-d',strtotime($time)) && $data['jd_trade_type']=='拒收'){//拒收
            $data['sys_fee'] =  $data['trade_fee']*0.5;
        }
        if($data['status']==-2){
            $data['sys_fee'] = 0;
        }
        $data['sys_fee'] = isset($data['sys_fee'])?$data['sys_fee']:$data['trade_fee'];
        $data['sys_trade_discount'] = round($data['trade_fee']*0.4,1);
        $data['discount'] = round($data['fee']*0.4,1);
        $data['normal'] = $data['sys_fee'] - $data['fee'] == 0?1:0;
    }elseif($data['fee_type']=='poundage'){
        $data['sys_fee'] = $data['trade_poundage'];
        $data['normal'] = $data['sys_fee'] - $data['fee'] == 0?1:0;
    }
//    halt($data);
    return $data;
}

/**
 * 隐藏电话号码部分数字
 *
 * @param $phone    11位130xxxxxxxx,12位0130xxxxxxxx手机号码，7位xxxxxxx,8位xxxxxxxx,13位xxxx-xxxxxxxx固话号码
 * @return string
 */
function hidePhone($phone){
    $phone = trim($phone);

    if(!$phone || session('user.id')==1 || \app\common\model\User::where('id',session('user.id'))->value('see_order')){
        return $phone;
    }

    if(strpos($phone,'-')!==false){//固话
        $str = explode('-',$phone);
        $length = strlen($str[1]);

        return $str[0].'-'.substr($str[1],0,4).str_repeat('*',$length-4);
    }else{
        $length = strlen($phone);
        if(1<=$length && $length<=8){//固话
            return substr($phone,0,4).str_repeat('*',$length-4<0?0:$length-4);
        }else{//手机
            return substr($phone,0,3).'****'.substr($phone,6,$length-7<0?0:$length-7);
        }
    }

}

/**
 * 预售单处理
 *
 * @param $id                       订单ID
 * @return \think\response\Json
 */
function bookCharge($waybill){
    $order = Order::where('ord_id',$waybill['ord_id'])->find();//订单信息

    if(!$order)
        return __error('订单信息不存在');

    if($order['ord_type']==4)
        return __error('订单已取消');

//    if($order['out_instruction']!=1)
//        return __error('分拣完成状态的订单才能推送');

//    $ord_type = $order->ord_type;

//    $waybill = Waybill::where('ord_id',$order['ord_id'])->find();//运单信息
//            if(!$waybill)
//                return __error('运单信息不存在，请重新下单');

//    if($waybill['waybill_num'])
//        return __error('已有运单号，不需要重新推送');
    if(!$waybill['sku_id'])
        return __error('商品信息不存在');
    if(!$waybill['wh_id'])
        return __error('仓库信息不存在');

    $wh = Warehouse::find($waybill['wh_id']);//仓库信息
    if(!$wh)
        return __error('仓库信息不存在');

    $deliver = OrgModel::find($waybill['deliver_id']);
    if(!$deliver)
        return __error('车队信息不存在');

    $order_detail = OrdDetail::where('ord_id',$waybill['ord_id'])->whereIn('sku_id',$waybill['sku_id'])->select();//订单商品
    $member = Member::find($order['member_id']);//客户信息
    $address = MemberAddress::where('address_id',$order['address_id'])->find();//地址信息
    $addr = getAddrName($address['province']).getAddrName($address['city']).getAddrName($address['county']).getAddrName($address['street']).$address['detail'];

    $param = [];
    $weight = $length = $height = $width = $volume = 0;
    $spu_name = [];
    foreach ($order_detail as $v){
        $sku = Products::find($v['sku_id'])->toArray();
//        if($sku['stock']<=0)
//            return __error('商品库存不足');
        $spu = Spu::find($sku['spu_id']);
        $weight += $spu['weight'];
        $length += $spu['spu_length'];
        $height += $spu['spu_height'];
        $width += $spu['spu_width'];
        $volume += round($spu['spu_length']*$spu['spu_height']*$spu['spu_width'],0);
        $spu_name[] = $spu['spu_name'];
        $str = '';
        if($spu['spu_param']){
            foreach ($spu['spu_param'] as $val){
                $str .= $sku['sku_param'][toPinYin($val['name'])];
//                $param[] = $sku['sku_param'][toPinYin($val['name'])].'*'.$v['quantity'];
            }
        }
        $param[] = $spu['spu_name'].($str?:'').'*'.$v['quantity'];
    }

    switch ($waybill['deliver_id']){
        case 1:
            $rows = [
                'customerCode'=>$deliver['jd_customer_code'],
                'orderId'=>$order['ord_num'],
                'ord_num'=>$order['ord_num'],
                'receiveAddress'=>$addr,
                'ware_house_code'=>$wh['jd_ware_house_code'],
                'salePlat'=>$deliver['jd_sale_plat'],
                'senderName'=>$wh['wh_person'],
                'senderAddress'=>$wh['wh_address'],
                'senderMobile'=>$wh['wh_phone'],
                'reciever'=>$member['member_name'],
                'addr'=>$addr,
                'receiveTel'=>$member['member_tel'],
                'phone'=>$member['member_phone'],
                'quantity'=>$waybill['good_count'],
                'weight'=>$weight,
                'product_length'=>$length,
                'product_width'=>$width,
                'product_height'=>$height,
                'volume'=>$volume,
                'product_name'=>implode('|',$param),
                'collection_payment'=>$order['collection_payment'],
//                        'parcel_insurance'=>1,//是否保价
//                        'insurance_prc'=>300,//保价金额
//                        'signReturn'=>1,//签还单
                'param'=>implode('|',$param),//商品规格
                'package_num'=>$waybill['package_num'],
            ];

            if(!$waybill['waybill_num']){
                $jdOrd = (new JdService())->order($rows);
                if(!empty($jdOrd['code']))
                    return $jdOrd;

//            $row['status'] = 0;
//            $row['status_text'] = '待出库';
                $waybill['waybill_num'] = $jdOrd['deliveryId'];
                $waybill['promise_time_type'] = $jdOrd['promiseTimeType'];
                $waybill['pre_sort_result'] = $jdOrd['preSortResult'];
                $waybill['trans_type'] = $jdOrd['transType'];
            }

            $waybill['waybill_at'] = time();
            $waybill['weight'] = $weight;
            $waybill['spu_length'] = $length;
            $waybill['spu_width'] = $width;
            $waybill['spu_height'] = $height;
            $waybill['spu_volume'] = $volume;
//                $row['deliver_id'] = $order['deliver_id'];
//                $row['ord_id'] = $order['ord_id'];
//                $row['sku_id'] = $order['sku_id'];
//                $row['spu_id'] = $order['spu_id'];
//                $row['ord_num'] = $order['ord_num'];
//                $row['org_waybill'] = Waybill::where('ord_num',$order['org_ord'])->value('waybill_num');
//                $row['collection_payment'] = $rows['collection_payment'];
//                $row['reciever'] = $rows['reciever'];
//                $row['reciever_phone'] = $rows['phone'];
//                $row['reciever_tel'] = $rows['receiveTel'];
//                $row['reciever_addr'] = $rows['addr'];
//                $row['package_num'] = $rows['package_num'];
//                $row['spu_length'] = $spu['spu_length'];
//                $row['spu_width'] = $spu['spu_width'];
//                $row['spu_height'] = $spu['spu_height'];
//                $row['weight'] = $spu['weight'];
//                $row['good_count'] = $order['quantity'];
//                $row['quantity'] = $order['quantity'];
            $waybill = charge($waybill);

            if(!empty($waybill['waybill_id'])){
                $r = Waybill::update($waybill,['waybill_id'=>$waybill['waybill_id']]);
            }else{
                $r = Waybill::create($waybill);
            }

            if($r){
                //库存处理
                if($order['ord_type'] == 3){//预售单
                    Order::where('ord_id',$waybill['ord_id'])->update(['ord_type'=>1]);
                    //库存操作
                    foreach($order_detail as $v){
                        Products::where('product_id',$v['sku_id'])->setDec('stock',$v['quantity']);
                    }
                }

                //有赞发货处理
//                $r = OrdDetail::yzSend($order->ord_route,$order->ord_id,$order->ord_num,$deliver->youzan_id,$waybill['waybill_num']);
                $route = Route::where(['yz_send_good'=>1,'id'=>$order->ord_route])->find();
                if($route){
                    $oid = OrdDetail::where('ord_id',$order->ord_id)->whereIn('sku_id',$waybill['sku_id'])->column('oid');
                    $r = (new YouZanService())->sendGoods([
                        'out_stype'=>(string)$deliver->youzan_id,
                        'out_sid'=>$waybill['waybill_num'],
                        'tid'=>$order->ord_num,
                        'oids'=>implode(',',$oid),
                    ]);
                    if($r)
                        Waybill::update(['yz_send'=>1],['waybill_id'=>$waybill['waybill_id']]);
                }

                return __success('推送成功');
            }else{
                return __error('推送成功，保存数据库失败，请记录运单号：'.$waybill['deliveryId']);
            }
            break;

        default:
            return __error('没有对应车队配置');
            break;
    }
}

function checkJdSend(){
    return false;
//    return Request::domain() == config('domain');
}

function checkPhone($phone){
    if(strpos($phone,'-')!==false){
        $phone = explode('-',$phone);
        if(is_numeric($phone))
            return true;
    }else{
        if(is_numeric($phone))
            return true;
    }

    return false;
}

function wordExport($file_name,$data,$image=[]){
    $template = new \PhpOffice\PhpWord\TemplateProcessor('./static/word/waybill_read.docx');
    $file_path = './uploads/word/';
    if (!is_dir($file_path)){
        mkdir($file_path,0777,true);
    }

    foreach ($data as $k=>$v){
        $template->setValue($k,$v);
    }
    if($image)
        foreach ($data as $k=>$v){
            $template->setImageValue($k,$v);
        }

    $template->saveAs($file_path.$file_name);
    if(is_file($file_path.$file_name))
        return $file_path.$file_name;

    return false;
}

function getTrafficData($query=[]){
    $url = config('traffic_ip').'/public/findcalllistreport.html';
    if($query)
        $query = http_build_query($query);

    $rows = json_decode(goCurl($url.'?'.$query),true);

    return $rows;
}

function trafficDownload($connectionid){
    $url = config('traffic_ip').'/public/getListenPubFileUrl.html?connectionid='.$connectionid;

    $rows = json_decode(goCurl($url),true);

    return $rows;
}

function jsonWithoutUnicode($data){
    return json_encode($data,JSON_UNESCAPED_UNICODE);
}

function set_ord_num(){
    return 'QH'.date('YmdHis').rand(1000,9999);
}

function date_numbers($start,$end,$type='d'){
    //转化日期格式为2018-8-28
    $start_m = date('Y-m-d',$start);
    $end_m = date('Y-m-d',$end);
    $date1 = explode('-',$start_m);
    $date2 = explode('-',$end_m);
    if ($type=='y'){
        //取绝对值，避免因年份大小产生的负值
        $number= abs($date1[0] - $date2[0]);
    }elseif ($type=='m'){
        //判断月份大小，进行相应加或减
        if($date1[1]<$date2[1]){
            $number= abs($date1[0] - $date2[0]) * 12 + abs($date1[1] - $date2[1]);
        }else{
            $number= abs($date1[0] - $date2[0]) * 12 - abs($date1[1] - $date2[1]);
        }
    }else{
        $time = $end-$start;
        $number = abs(intval($time/(3600*24)));
    }
    return $number;
}

//呼入状态处理
function trafficStatusByCalled($traffic,$on_work,$off_work){
    $noon = '12:00:00';
    $on_work = explode(' - ',$on_work);
    if($traffic['code']){//已处理
        $traffic['status'] = 1;
    }else{
//        $between = [];
//        if(strtotime($traffic['callTime'])>strtotime(date("Y-m-d $off_work",strtotime($traffic['callTime'])))){//非工作时间
//            $date = date("Y-m-d $off_work",strtotime($traffic['callTime']));
//            $between = [
//                $date,
//                date("Y-m-d $noon",strtotime($date."+1 day"))
//            ];
//        }elseif (strtotime($traffic['callTime'])>=strtotime(date("Y-m-d $on_work[0]",strtotime($traffic['callTime'])))){//工作时间
//            $date = date("Y-m-d $on_work[0]",strtotime($traffic['callTime']));
//            $between = [
//                $date,
//                date("Y-m-d $on_work[1]",strtotime($date))
//            ];
//        }elseif(strtotime(date('Y-m-d 00:00:00',strtotime($traffic['callTime'])))<=strtotime($traffic['callTime'])
//            && strtotime($traffic['callTime'])<strtotime(date("Y-m-d $on_work[0]",strtotime($traffic['callTime'])))){//当天00:00到工作开始时间
//            $date = date("Y-m-d $on_work[0]",strtotime($traffic['callTime']));
//            $between = [
//                $date,
//                date("Y-m-d $noon",strtotime($date))
//            ];
//        }
//        if(!$between)
//            halt($traffic);

        $between = [$traffic['callTime'],date("Y-m-d H:i:s",strtotime($traffic['callTime'])+24*3600)];

        $reply = Traffic::field("count(if(callResult='接通',true,null)) reply,count(if(callResult!='接通',true,null)) replyNoAnswer")
            ->where('callType',2)
            ->where('called',$traffic['called'])
            ->whereBetween('callTime',$between)
            ->find()
        ;
        if(!$reply['reply'] && !$reply['replyNoAnswer']){//未处理
            $traffic['status'] = 2;
        }else{
            $traffic['status'] = $reply['reply']?3:4;//已回访或已回访未接通
        }
    }
    if(!is_array($traffic)){
        $traffic->save();
    }else{
        return $traffic;
    }

}

//根据呼出处理呼入状态处理
function trafficStatusByCall($traffic,$on_work,$off_work){
    $on_work = explode(' - ',$on_work);
    $start = date("Y-m-d $on_work[0]",strtotime($traffic['callTime']));
    $noon = date('Y-m-d 12:00:00',strtotime($traffic['callTime']));
    $end = date("Y-m-d $on_work[1]",strtotime($traffic['callTime']));
    $where = [];
    if(strtotime($start)<=strtotime($traffic['callTime']) && strtotime($traffic['callTime'])<=strtotime($noon)){//当天8：30-12:00呼出
        $where = "((callTime > '".date("Y-m-d $off_work",strtotime($end."-1 day"))."' && callTime < '$start') OR (callTime between '$start' AND '$end'))";
//        halt($where);
//        $where = [//前一天大于18:30 - 今天8:29
//            ['callTime','>',date('Y-m-d H:i:s',strtotime($end."+1 day"))],
//            ['callTime','<',$start],
//        ];
//        $where2 = [//今天8:30 - 18:30
//            ['callTime','between',[$start,$end]],
//        ];
    }elseif (strtotime($start)<=strtotime($traffic['callTime']) && strtotime($traffic['callTime'])<=strtotime($end)){//当天8：30-18:30呼出
        $where = [//今天8:30 - 18:30
            ['callTime','between',[$start,$end]],
        ];
    }

    $call = Traffic::where('callType',1)
        ->where('repeat',0)
        ->where('status',2)
        ->where('called',$traffic['called'])
        ->where($where)
//        ->fetchSql()
        ->select();

    if(count($call)>0){
        foreach ($call as $v){
            $v->status = $traffic['callResult']=='接通'?3:4;
            $v->save();
        }
    }

}

function fillHole($data,$i=0,$line=23){
    if(!$data)
        return [];

//    dump($data);
//    dump($i);
//    halt($line);
//    halt(array_values($data));
    $keys = array_keys(current($data));

    for ($i;$i<=$line;$i++){
        if(!isset($data[$i])){
            foreach($keys as $v){
                $data[$i][$v] = $v=='callTime'?$i:0;
            }
        }
    }
    ksort($data);
    return $data;
}

function setTotalAndAvg($rows,$title='title',$field=[],$withoutRows=false,$start='',$end=''){
    $total = $avg = [$title=>'合计'];
    $avg = [$title=>'平均'];
    $field = $field?:[$title];
    $rows = is_array($rows)?$rows:$rows->toArray();
    foreach($rows as $v){
        foreach ($v as $k=>$val){
            if(in_array($k,$field))
                continue;
            $total[$k] = isset($total[$k])?$total[$k]+$val:$val;
        }
    }
//                halt($total);
    foreach ($total as $k=>$v){
        if(in_array($k,$field))
            continue;
        $avg[$k] = round($v/count($rows));
    }
    $total['start'] = $start;
    $total['end'] = $end;
    return $withoutRows?[$total,$avg]:array_merge([$total,$avg],$rows);
}

function getEnum($v,$config,$default='未知'){
    return isset(config($config)[$v])?config($config)[$v]:$default;
}

/**
 * 获取查询条件
 * @param $select array 查询条件
 * @return array
 */
function get_query_condition($where = [],$select=[],$table=''){
    foreach ($select as $key => $value) {

        if ((is_string($value)&&trim($value) === '') || $key=='jd')
            continue;
        $key = substr_count($key,'-')>0?str_replace('-','.',$key):$key;//substr_replace($key,".",strpos($key,'-'),strlen('-'))
        $key = strpos($key,'.')!==false?$key:($table?"$table.$key":$key);
        if ($key == 'status') {
            $where[] = [$key, '=', $value];
        } elseif (strpos($key,'_at')!==false) {
            $value_list = explode(" - ", $value);
            $start = strtotime("$value_list[0] 00:00:00");
            $end = strtotime("$value_list[1] 23:59:59");
            $where[] = [$key, 'BETWEEN', [$start, $end]];
        }elseif (strpos($key,'Time')!==false) {
            $value_list = explode(" - ", $value);
            $start = "$value_list[0] 00:00:00";
            $end = "$value_list[1] 23:59:59";
            $where[] = [$key, 'BETWEEN', [$start, $end]];
        }elseif (preg_match('/.*id.*/',$key) && is_numeric($value)) {
            $where[] = [$key, '=', $value];
        }elseif(is_array($value)||strpos($value,',')){
            $where[] = [$key,'in',$value];
        }elseif(is_numeric($value)){
            $where[] = [$key,'=',$value];
        } else {
            $where[] = [$key, 'LIKE', '%' . $value . '%'];
        }
    }
    return $where;
}

function get_class_name($class){
    return (new \ReflectionClass($class))->getShortName();
}

function throw_error($msg){
    exit(json_encode(__error($msg)));
}