<?php
// +----------------------------------------------------------------------
// | 99PHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018~2020 https://www.99php.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Mr.Chung <chung@99php.cn >
// +----------------------------------------------------------------------

namespace app\common\service;


use think\Model;

/**
 * 模型基础数据服务
 * Class ModelService
 * @package service
 */
class ModelService extends Model {

    protected $updateTime = 'update_at';

    protected $createTime = 'create_at';

    /**
     * 主键定义
     * @var string
     */
    protected $pk = 'id';

    protected $type = [
        'delete_at'=>'timestamp:Y-m-d H:i:s',
        'ord_at'=>'timestamp:Y-m-d H:i:s',
        'waybill_at'=>'timestamp:Y-m-d H:i:s',
        'program_start'=>'timestamp:Y-m-d',
        'program_end'=>'timestamp:Y-m-d',
        'finance_at'=>'timestamp:Y-m-d H:i:s',
        'trade_at'=>'timestamp:Y-m-d H:i:s',
    ];

    /**
     * 软删除操作
     * @param $id 列ID
     * @param bool $type 删除类型 （false:软删除，true:真实删除）
     * @return bool|ModelService
     * @throws \Exception
     */
    public function del($id, $type = false) {
        if ($type) {
            $del = $this->where($this->pk, $id)->delete();
        } else {
            $del = $this->where($this->pk, $id)->update(['is_deleted' => 1,'delete_by'=>session('user.id'),'delete_at'=>time()]);
        }
        if ($del >= 1) return __success('删除成功');
        return __error('删除失败，请检查！');
    }

    /**
     * 新增软删除
     *
     * @param array $where  条件
     * @param array $row    更新字段
     * @param bool $type    true删除数据库记录 false软删除
     * @return \think\response\Json
     */
    public static function deleteRow($where=[], $type = false) {
        if ($type) {
            $del = self::where($where)->delete();
        } else {
            $del = self::where($where)->update(del_common_field(session('user.id')));
        }
        if ($del >= 1) return __success('删除成功');
        return __error('删除失败，请检查！');
    }

    public static function baseModelSource($field='*',$where=[])
    {
        $table = self::getTable();
        $source = self::field($field)->where($table.'.is_deleted','0')->where($where);
        return $source;
    }

    /**
     * 软删除操作
     * @param      $id   列ID
     * @param bool $type 删除类型 （false:软删除，true:真实删除）
     * @return bool|ModelService
     * @throws \Exception
     */
    public function delData($id, $type = false) {
        is_array($id) ? $model = self::whereIn(self::getPk(), $id) : $model = self::where(self::getPk(), $id);
        if ($type) {
            $del = $model->delete();
        } else {
            $del = $model->update(['is_deleted' => 1,'delete_by'=>session('user.id'),'delete_at'=>time()]);
        }
        if ($del >= 1) return __success('删除成功');
        return __error('删除失败，请检查！');
    }
}