<?php

namespace app\common\service;

use app\common\model\CurlLog;
use app\common\model\OrgModel;
use app\common\validate\Jd;

/**
 * 京东服务类
 *
 * Class JdService
 * @package app\common\service
 */
class JdService {

    private $access_token,//京东token
            $app_secret,//京东secret
            $app_key,//京东app_key
            $salePlat,//京东销售平台
            $customerCode;//京东商家编码

    protected $validate;

    public function __construct($config=[])
    {
        $config = $config?:OrgModel::find(1)->toArray();
        $this->access_token = $config['jd_token'];
        $this->app_secret = $config['jd_app_secret'];
        $this->app_key = $config['jd_app_key'];
        $this->salePlat = $config['jd_sale_plat'];
        $this->customerCode = $config['jd_customer_code'];
        $this->validate = new Jd();
    }

    /**
     * 接口url处理
     *
     * @access private
     * @param array $param      参数
     * @param string $method    调用接口
     * @return string
     */
    private function setUrl($param,$method)
    {
        $url = 'https://api.jd.com/routerjson?';
        $param = [
            'access_token'=>$this->access_token,
            'app_key'=>$this->app_key,
            'method'=>$method,
            'timestamp'=>date('Y-m-d H:i:s'),
            'v'=>'2.0',
            '360buy_param_json'=>json_encode($param,JSON_UNESCAPED_UNICODE),
        ];
        $str = '';
        $attr = [];
        ksort($param);

        foreach($param as $k=>$v){
            $str .= "$k$v";
            $attr[] = urlencode($k).'='.urlencode($v);
        }

        $sign = strtoupper(md5($this->app_secret.$str.$this->app_secret));
        $url .= implode('&',$attr).'&sign='.$sign;
        return $url;
    }

    /**
     * 接口请求
     *
     * @param $param    参数
     * @param $url      调用接口
     * @return mixed
     */
    private function request($param,$url,$log=1){
        $res = json_decode(goCurl($this->setUrl($param,$url)),true);

        if($log)
            CurlLog::log([
                'curl_url'=>urldecode($url),
                'curl_data'=>$param,
                'response_data'=>$res,
            ]);

        return $res;
    }

    /**
     * 下达运单
     *
     * @param $data         运单参数
     * @return bool|mixed
     */
    public function order($data)
    {
        $jp = $this->check($data);//检测京配
        if($jp['code'])
            return $jp;

        $url = 'jingdong.ldop.waybill.receive';

        $rows = [
            "salePlat"=> $this->salePlat,//销售平台
            "customerCode"=> $this->customerCode,//商家编码
            "orderId"=> $data['ord_num'],//订单号
            "thrOrderId"=> "",
            "senderName"=> $data['senderName'],//寄件人
            "senderAddress"=> $data['senderAddress'],//寄件人地址
            "senderTel"=> '',
            "senderMobile"=> $data['senderMobile'],//寄件人电话
            "senderPostcode"=> "",
            "receiveName"=> $data['reciever'],//收件人
            "receiveAddress"=> $data['addr'],//收件人地址
            "province"=> "",
            "city"=> "",
            "county"=> "",
            "town"=> "",
            "provinceId"=> "",
            "cityId"=> "",
            "countyId"=> "",
            "townId"=> "",
            "siteType"=> "",
            "siteId"=> "",
            "siteName"=> "",
            "receiveTel"=> $data['receiveTel'],
            "receiveMobile"=> $data['phone'],//收件人电话
            "postcode"=> "",
            "packageCount"=> $data['package_num'],//包裹数量
            "weight"=> $data['weight'],//重量
            "vloumLong"=> $data['product_length'],//长cm
            "vloumWidth"=> $data['product_width'],//宽cm
            "vloumHeight"=> $data['product_height'],//高cm
            "vloumn"=> $data['volume'],//体积cm3
            "description"=> $data['product_name'],//商品描述
            "collectionValue"=> $data['collection_payment']>0?1:0,//是否代收货款
            "collectionMoney"=> $data['collection_payment'],//代收货款
            "guaranteeValue"=> isset($data['parcel_insurance'])?$data['parcel_insurance']:0,//是否保价
            "guaranteeValueAmount"=> isset($data['insurance_prc'])?$data['insurance_prc']:0,//保价金额
            "signReturn"=> isset($data['signReturn'])?$data['signReturn']:0,//签还单
            "aging"=> "",
            "transType"=> "",
            "remark"=> '可开箱验货后付款'.' 商品['.$data['product_name'].','.$data['param'].']',
            "goodsType"=> "",
            "orderType"=> "",
            "shopCode"=> "",
            "orderSendTime"=> "",
            "warehouseCode"=> "",
            "areaProvId"=> "",
            "areaCityId"=> "",
            "shipmentStartTime"=> "",
            "shipmentEndTime"=> "",
            "idNumber"=> "",
            "addedService"=> "",
            "extendField1"=> "",
            "extendField2"=> "",
            "extendField3"=> "",
            "extendField4"=> "",
            "extendField5"=> "",
            "senderCompany"=> "",
            "receiveCompany"=> "",
            "goods"=> $data['product_name'],//商品类目，名称
            "goodsCount"=> $data['quantity'],//商品数量
            "freight"=> ""
        ];

        $validate = $this->validate->scene('jd_order')->check($rows);
        if (true !== $validate) return __error($this->validate->getError());

        if(empty($rows['receiveTel'])&&empty($rows['receiveMobile']))
            return __error('缺少收件人联系方式');

        $res = $this->request($rows,$url);

        if(isset($res['jingdong_ldop_waybill_receive_responce'])){
            if($res['jingdong_ldop_waybill_receive_responce']['code']>0)
                return __error('推送失败');

            $waybill_info = $res['jingdong_ldop_waybill_receive_responce']['receiveorderinfo_result'];
            if(!empty($waybill_info['deliveryId'])){
                return $waybill_info;
            }else{
                $error = isset($waybill_info['resultMessage'])?$waybill_info['resultMessage']:'';
                return __error('推送失败：'.$error);
            }
        }
        return __error('系统错误');
    }

    /**
     * 检测京配
     *
     * @param $data                 运单参数
     * @example
     * @return \think\response\Json
     */
    public function check($data)
    {
        $url = 'jingdong.etms.range.check';
        $rows = [
            "customerCode"=> $this->customerCode,
            "orderId"=> $data['ord_num'],//$data['ord_num'],
            "goodsType"=> 1,
            "receiveAddress"=>$data['addr'],//$data['addr'],
            'wareHouseCode'=>$data['ware_house_code'],
        ];

        $validate = $this->validate->scene('jd_check')->check($rows);
        if (true !== $validate) exit(alert($this->validate->getError(),'',3,2));

        $res = $this->request($rows,$url,0);

        if(isset($res['jingdong_etms_range_check_responce'])){
            if($res['jingdong_etms_range_check_responce']['code']>0)
                return __error('检测京配失败');
            $is_jd = $res['jingdong_etms_range_check_responce']['resultInfo'];
            if(!$is_jd || $is_jd['rcode']!=100){
                $error = isset($is_jd['rmessage'])?$is_jd['rmessage']:'';
                return __error($error);
            }else{
                return __success('可以京配');
            }
        }
        return __error('系统错误');
    }

    /**
     * 物流轨迹
     *
     * @param $waybill_num  运单号
     * @return bool|mixed
     */
    public function trade($waybill_num)
    {
        $url = 'jingdong.ldop.receive.trace.get';
        $rows = [
            "customerCode"=> $this->customerCode,
            "waybillCode"=> $waybill_num
        ];

        $validate = $this->validate->scene('jd_trade')->check($rows);
        if (true !== $validate) exit(alert($this->validate->getError(),'',3,2));

        $res = $this->request($rows,$url,0);

        if(isset($res['jingdong_ldop_receive_trace_get_responce'])){
            if($res['jingdong_ldop_receive_trace_get_responce']['code']>0)
                exit(alert('获取轨迹失败'));
            return $res['jingdong_ldop_receive_trace_get_responce']['querytrace_result']['data'];
        }
        exit(alert('系统错误'));
    }

    /**
     * 拦截运单
     *
     * @param $waybill_num      运单号
     * @param $cancel_reason    拦截原因
     * @return bool|mixed
     */
    public function cancel($waybill_num,$cancel_reason='取消')
    {
        $url = 'jingdong.ldop.delivery.provider.cancelWayBill';
        $rows = [
            'userPin'=>'',//用户唯一标识,与下单一致可不填写
            "customerCode"=> $this->customerCode,//	商家编码
            "waybillCode"=> $waybill_num,//运单号
            'source'=>'JOS',//来源
            "cancelReason"=> $cancel_reason,//取消原因
            'operatorName'=>'琼花',//操作人
        ];

        $res = $this->request($rows,$url);

        if(isset($res['jingdong_ldop_delivery_provider_cancelWayBill_responce'])){
            if($res['jingdong_ldop_delivery_provider_cancelWayBill_responce']['code']>0)
                return false;
            return $res['jingdong_ldop_delivery_provider_cancelWayBill_responce']['responseDTO'];
        }
        return false;
    }

    /**
     * 拦截运单
     *
     * @param $waybill_num      运单号
     * @param $cancel_reason    拦截原因
     * @return bool|mixed
     */
    public function intercept($waybill_num,$cancel_reason='拦截')
    {
        $url = 'jingdong.ldop.receive.order.intercept';
        $rows = [
            "vendorCode"=> $this->customerCode,
            "deliveryId"=> $waybill_num,
            "interceptReason"=> $cancel_reason,
        ];

        $res = $this->request($rows,$url);

        if(isset($res['jingdong_ldop_receive_order_intercept_responce'])){
            if($res['jingdong_ldop_receive_order_intercept_responce']['code']>0)
                return false;
            return $res['jingdong_ldop_receive_order_intercept_responce']['resultInfo'];
        }
        return false;
    }

    public function read($waybill_num)
    {
        $url = 'jingdong.ldop.waybill.generalQuery';
        $rows = [
            "customerCode"=> $this->customerCode,
            "deliveryId"=> $waybill_num,
        ];

        $res = $this->request($rows,$url);
        halt($res);
    }
}
