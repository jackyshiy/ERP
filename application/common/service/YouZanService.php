<?php

// +----------------------------------------------------------------------
// | Think.Admin
// +----------------------------------------------------------------------
// | 版权所有 2014~2017 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: http://think.ctolog.com
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | github开源项目：https://github.com/zoujingli/Think.Admin
// +----------------------------------------------------------------------

namespace app\common\service;

use think\facade\Config;
use Hanson\Youzan\Youzan;

class YouZanService {

    private $logistics_id = 138;

    public $youzan;

    public function __construct($config=[])
    {
        $this->youzan = new Youzan($config?:config('youzan.fs_tv'));
    }

    //订单搜索
    public function order($param=[])
    {
        $result = $this->youzan->request('youzan.trades.sold.get',$param);
        return $this->result($result);
    }

    //发货
    public function sendGoods($param)
    {
//        $param['out_stype'] = (string)$this->logistics_id;
//        $param['is_no_express'] = 0;
//        dump($param);
        $result = $this->youzan->setVersion('3.0.0')->request('youzan.logistics.online.confirm',$param);
//        halt($result);
        if(isset($result['gw_err_resp']))
            if(isset($result['gw_err_resp']['err_code']) && $result['gw_err_resp']['err_code']!=200){
                return false;
            }
        if($result['code']==200 ){
            if($result['data']['is_success'])
                return true;
            return false;
        }elseif($result['code']==102570007 || $result['code']==102570020){
            return true;
        }else{
            return false;
        }
//        return $this->result($result);
    }

    //结果输出
    private function result($result)
    {
        if(isset($result['code']) && $result['code']!=200){
            return false;
        }



        return $result['data'];
    }

    public function logistics($param=[])
    {
        $result = $this->youzan->setVersion('3.0.0')->request('youzan.logistics.express.get',$param);
        return $result['data']['allExpress'];
    }
}