<?php

namespace app\common\model;

use app\common\service\ModelService;

class Products extends ModelService
{
    /**
     * 主键定义
     * @var string
     */
    protected $pk = 'product_id';

    protected $type = [
        'sku_param'=>'json'
    ];

    public function spu()
    {
        return $this->belongsTo('Spu','spu_id','spu_id')->joinType('left');
    }
    
    public function searchNameAttr($query, $value, $data)
    {
        $query->where('product_name|product_num','like', "%$value%" );
        if (isset($data['sort'])) {
            $query->order($data['sort']);
        }
    }

    public static function stock($id,$num,$type='stock')
    {
        $stock = self::where('product_id',$id)->value($type);
        if($stock===null)
            return __error('找不到商品信息');
        if($num>0){
            $r = self::where('product_id',$id)->setInc($type,$num);
        }elseif($num<0){
            $r = self::where('product_id',$id)->setDec($type,abs($num));
        }elseif($num==0){
            return __error('请输入变化数量');
        }

        return $r?__successData('操作成功',['stock'=>$stock]):__error('操作失败');
    }
}
