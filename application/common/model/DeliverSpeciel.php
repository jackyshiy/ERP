<?php

namespace app\common\model;

use app\common\service\ModelService;
use think\Model;

class DeliverSpeciel extends ModelService
{
    //

    public function spu()
    {
        return $this->hasOne('Spu','spu_id','spu_id')->joinType('left')->bind('spu_name,spu_num');
    }

    public function warehouse()
    {
        return $this->hasOne('Warehouse','wh_id','wh_id')->joinType('left')->bind('wh_name');
    }

    public function route()
    {
        return $this->hasOne('Route','id','route_id')->joinType('left')->bind('route_name');
    }

    public function deliver()
    {
        return $this->hasOne('OrgModel','org_id','deliver_id')->joinType('left')->bind('org_name');
    }

}
