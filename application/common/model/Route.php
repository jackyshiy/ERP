<?php

namespace app\common\model;

use app\common\service\ModelService;
use think\Model;

class Route extends ModelService
{
    public function order()
    {
        return $this->hasMany('Order','ord_route','id');
    }

    public static function getRoute()
    {
        return self::where('is_deleted',0)->where('status',1)->select();
    }

    public function getPayModeAttr($value)
    {
        return $value?explode(',',$value):[];
    }

    public function setPayModeAttr($value)
    {
        return implode(',',$value);
    }
}
