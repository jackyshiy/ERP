<?php

namespace app\common\model;

use app\common\service\ModelService;
use app\common\service\YouZanService;
use think\Model;

class OrdDetail extends ModelService
{

    public function sku()
    {
        return $this->hasOne('Products','product_id','sku_id')->joinType('left')->bind('sku_param');
    }

    public function spu()
    {
        return $this->hasOne('Spu','spu_id','spu_id')->joinType('left')->bind('spu_name');
    }

    /**
     * 有赞发货推送
     *
     * @param $order_route
     * @param $order_id
     * @param $order_num
     * @param $deliver_type
     * @param $waybill_num
     */
    public static function yzSend($order_route,$order_num,$deliver_type,$waybill_num,$oid)
    {
        $route = Route::where(['yz_send_good'=>1])->column('id');
        if(!in_array($order_route,$route))
            return false;
//        $oids = self::leftJoin('waybill','waybill.ord_id = ord_detail.ord_id')
//            ->where('ord_detail.ord_id',$order_id)
//            ->where('find_in_set(ord_detail.sku_id,waybill.sku_id)')
//            ->column('oid');
        $r = (new YouZanService())->sendGoods([
            'out_stype'=>(string)$deliver_type,
            'out_sid'=>$waybill_num,
            'tid'=>$order_num,
            'oids'=>$oid,
        ]);
        return $r?true:false;
    }

    public static function goods_balance($where)
    {
        $field = [
            'supplier_num',
            'supplier_name',
            'spu.balance_type',
            'ord_detail.spu_num',
            'spu_name',
            'route_name',
            'ord_detail.price',
//            'products.org_price',
            'tax',
            'ord_detail.quantity',
//            'order.is_finance',
        ];

        $row = self::leftJoin('order','order.ord_id = ord_detail.ord_id')
            ->join('balance','balance.ord_id = order.ord_id')
            ->leftJoin('route','route.id = order.ord_route')
            ->leftJoin('spu','spu.spu_id = ord_detail.spu_id')
            ->leftJoin('supplier','supplier.id = spu.supplier_id')
            ->field($field)
            ->where($where)
            ->order('detail_id desc');
        return $row;
    }

    public static function goods_balance_gather($where)
    {
        $field = [
            'supplier_num',
            'supplier_name',
            'spu.balance_type',
            'ord_detail.spu_num',
            'spu_name',
//            "group_concat(distinct route_name SEPARATOR '|') route_name",
            'route_name',
            'spu.org_price spu_cost',
            'products.org_price sku_cost',
            'ord_detail.price',
            'tax',
            'sum(ord_detail.quantity) quantity',
            'ord_detail.spu_id',
            'route.id route_id',
        ];

        $rows = self::leftJoin('order','order.ord_id = ord_detail.ord_id')
            ->join('balance','balance.ord_id = order.ord_id')
            ->leftJoin('route','route.id = order.ord_route')
            ->leftJoin('spu','spu.spu_id = ord_detail.spu_id')
            ->leftJoin('products','products.product_id = ord_detail.sku_id')
            ->leftJoin('supplier','supplier.id = spu.supplier_id')
            ->field($field)
            ->where($where)
            ->whereNotNull('route.id')
            ->whereNotNull('supplier_name')
            ->group('supplier.id,ord_detail.spu_id,route.id')
//            ->having('count>1')
            ->select()->each(function ($v){
                $spu_rule = SpuRule::where(['spu_id'=>$v['spu_id'],'route_id'=>$v['route_id']])->find();
                $v['cost'] = $spu_rule && $spu_rule['cost']?$spu_rule['cost']:($v['spu_cost']>0?$v['spu_cost']:$v['sku_cost']);
                $v['price'] = $spu_rule && $spu_rule['price']?$spu_rule['price']:$v['price'];
                $v['money'] = $v['price']*$v['quantity'];
                $v['cost_total'] = round($v['cost']*$v['quantity'],2);
                $v['balance_type'] = getEnum($v['balance_type'],config('balance_type'));
            });

        return $rows;
    }
}
