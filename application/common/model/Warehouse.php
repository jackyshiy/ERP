<?php

namespace app\common\model;

use app\common\service\ModelService;
use think\Model;

class Warehouse extends ModelService
{
    protected $pk = 'wh_id';

    public function deliver()
    {
        return $this->hasOne('OrgModel','org_id','org_id')->joinType('left')->bind('org_name');
    }
}
