<?php

namespace app\common\model;

use app\common\service\ModelService;
use app\common\model\OrdDetail;

class Order extends ModelService
{
    protected $pk = 'ord_id';

    public function waybill()
    {
        return $this->hasMany('Waybill','ord_id','ord_id');
    }

    public function product()
    {
       return $this->hasManyThrough('Products','OrdDetail','sku_id','ord_id','ord_id');
    }

    public function route()
    {
        return $this->belongsTo('Route','ord_route','id')->joinType('left')->bind('route_name');
    }

    public function payLog()
    {
        return $this->hasOne('WxpayLog','ord_id','ord_id')->joinType('left')->bind('return_code,result_code');
    }

    public function address()
    {
        return $this->hasOne('MemberAddress','address_id','address_id')->joinType('left');
    }

    public function searchKeywordAttr($query, $value, $data)
    {
        $query->where('ord_num','like', "%$value%" );
        if (isset($data['sort'])) {
            $query->order($data['sort']);
        }
    }

    public function searchOrdNumAttr($query, $value, $data)
    {
        $query->where('ord_num','like', "%$value%" );
        if (isset($data['sort'])) {
            $query->order($data['sort']);
        }
    }

    public static function book($sku_id='')
    {
        $order = self::where('ord_type',3)->select();
        foreach ($order as $v){
            if(!$v['sku_id'])
                continue;
            $flag = false;
            $sku_id = explode(',',$v['sku_id']);
            foreach ($sku_id as $val){
                $stock = Products::where('product_id',$val)->value('stock');
                $quantity = OrdDetail::where('ord_id',$v['ord_id'])->where('sku_id',$val)->value('quantity');
                if($quantity>0 && $stock>=$quantity){
                    $r = Products::stock($val,0-$quantity);
                    $flag = $r?true:false;
                }else{
                    $flag = false;
                }
            }
            if($flag){
                self::update(['ord_type'=>1],['ord_id'=>$v['ord_id']]);
            }

        }
    }

    public static function basic($where,$whereSql)
    {
        return self::with('route')->leftJoin('waybill','waybill.ord_id = order.ord_id')
            ->leftJoin('system_user','system_user.id = order.order_by')
            ->leftJoin('system_user delete','delete.id = order.delete_by')
            ->leftJoin('member','member.member_id = order.member_id')
            ->leftJoin('ord_detail','ord_detail.ord_id = order.ord_id')
            ->leftJoin('products','products.product_id = ord_detail.sku_id')
            ->leftJoin('spu','spu.spu_id = ord_detail.spu_id')
            ->leftJoin('balance','balance.ord_id = order.ord_id')
            ->where($where)
            ->where($whereSql);
    }

    public static function lists($where,$whereSql,$limit)
    {
        $field = [
            'order.ord_id',
            'ord_at',
            'group_concat(DISTINCT waybill.waybill_num) waybill_num',
            'waybill.waybill_id',
            'group_concat(DISTINCT spu_name) spu_name',
            'spu.spu_num',
            'status_text',
            'order.is_finance',
            'is_trade',
            'system_user.username',
            'delete.username deleter',
            'order.delete_at',
            'waybill.reciever waybill_receiver',
            'waybill.reciever_phone waybill_phone',
            'order.ord_num',
            'member_phone',
            'member_tel',
            'member_name',
            'ord_route',
            'ord_type',
            'order.quantity',
            'ord_money',
            'order.collection_payment',
            'org_ord',
            'is_cancel',
            'pay_mode',
        ];

        $rows = self::basic($where,$whereSql)->field($field)->order('order.ord_at','desc')
            ->group('order.ord_id')
            ->paginate($limit)
            ->each(function($val){
                $val['is_finance'] = $val['is_finance']==1?'是':'否';
                $val['is_trade'] = $val['is_trade']==1?'是':'否';
                $val['ord_type_text'] = getEnum($val['ord_type'],'ord_type');
                $val['status_text'] = $val['status_text']?$val['status_text']:'待出库';
//                    $val['ord_route'] = getEnum($val['ord_route'],'ord_route','无');
                $val['member_phone'] = $val['member_phone']?hidePhone($val['member_phone']):'';
                $val['member_tel'] = $val['member_tel']?hidePhone($val['member_tel']):'';
//                    $val['out_instruction'] = getEnum($val['out_instruction'],'out_instruction');
                $val['is_cancel'] = getEnum($val['is_cancel'],'is_cancel','无');
            })
        ;
        return $rows;
    }
}
