<?php

namespace app\common\model;

use app\common\service\ModelService;
use think\Model;

class SystemLog extends ModelService
{
    protected $pk = 'log_id';

    protected $table = 'system_log';

    public static function log($data)
    {
        $stock_log = [
            'log_type'=>'stock',
            'product_id'=>$data['sku_id'],
            'num'=>$data['stock'],
            'log_detail'=>$data['detail']//"商品 $data[spu_name] ，首次添加 ，良品库存 $data[stock] ，不良品库存0",
        ];
        $r = SystemLog::create(save_common_field($stock_log));
        return $r?true:false;
    }
}
