<?php

namespace app\common\model;

use app\common\service\ModelService;
use think\Model;

class Member extends ModelService
{
    protected $pk = 'member_id';
}
