<?php

namespace app\common\model;

use app\common\service\ModelService;
use think\Model;

class CurlLog extends ModelService
{
    protected $pk = 'curl_id';

    protected $type = [
        'curl_data'=>'json',
        'response_data'=>'json'
    ];

    public static function log($log)
    {
        $r = self::create($log);
        return $r?true:false;
    }
}
