<?php

namespace app\common\model;

use app\common\service\ModelService;

class Waybill extends ModelService
{
    protected $pk = 'waybill_id';

    protected $type = [
        'back_at'=>'timestamp:Y-m-d H:i:s',
        'recieved_at'=>'timestamp:Y-m-d H:i:s',
        'refund_at'=>'timestamp:Y-m-d H:i:s',
        'cancel_at'=>'timestamp:Y-m-d H:i:s',
        'waybill_at'=>'timestamp:Y-m-d H:i:s',
        'verify_at'=>'timestamp:Y-m-d H:i:s',
        'pre_sort_result'=>'json',
    ];
//    protected $readonly = ['update_at', 'status_text','stauts'];

    public function ord()
    {
        return $this->belongsTo('Order','ord_id','ord_id')->joinType('left');
    }

    public function deliver()
    {
        return $this->hasOne('OrgModel','org_id','deliver_id')->joinType('left')
            ->bind('org_name,org_sender,org_sender_phone,org_sender_addr,jd_customer_code');
    }

    public function warehouse()
    {
        return $this->hasOne('Warehouse','wh_id','wh_id')->joinType('left')
            ->bind('wh_address,wh_person,wh_phone');
    }
//
//    public function balance()
//    {
//        return $this->hasOne('Balance','waybill_num','waybill_num');
//    }

    public static function basic($where=[],$whereSql='')
    {
        return self::with('deliver')
            ->leftJoin('order','waybill.ord_id = order.ord_id')
            ->leftJoin('ord_detail','ord_detail.ord_id = waybill.ord_id')
            ->leftJoin('products','products.product_id = ord_detail.sku_id')
            ->leftJoin('spu','spu.spu_id = waybill.spu_id')
            ->leftJoin('member','member.member_id = order.member_id')
            ->where($where)
            ->where($whereSql);
    }
}
