<?php

namespace app\common\model;

use app\common\service\ModelService;
use think\Model;

class Traffic extends ModelService
{
    protected $pk = 'traffic_id';

    public function spu()
    {
        return $this->hasOne('Spu','spu_id','spu_id')->joinType('left')->bind('spu_name');
    }
    
    public function setTrafficTypeAttr($value)
    {
        return $value?implode(',',$value):[];
    }

    public function getTrafficTypeAttr($value)
    {
        return $value?explode(',',$value):'';
    }

    public static function efficiency($callTime='',$on_work)
    {
        $on_work = explode(' - ',$on_work);
        if($callTime){
            $time = explode(' - ',$callTime);
            $t = date('t',strtotime($time[1]));
            $start = $time[0].'-01 00:00:00';
            $end = $time[1]."-$t 23:59:59";
        }else{
            $t = date('t');
            $start = date('Y-m-01 00:00:00');
            $end = date("Y-m-$t 23:59:59");
        }

        $field = [
            "date(callTime) callTime",
            'count(if(callType=1,true,null)) in_total',//呼入总数
            "count(if(callTime between CONCAT(date(callTime),' ".$on_work[0]."') and CONCAT(date(callTime),' ".$on_work[1]."') && callType=1,true,null)) in_work",//工作时间呼入总数
            "count(if(callType=1 && code is not null,true,null)) in_effect",//呼入总接通数
            "count(if(callTime between CONCAT(date(callTime),' ".$on_work[0]."') and CONCAT(date(callTime),' ".$on_work[1]."') && callType=1 && code is not null,true,null)) in_effect_work",//工作时间呼入接通数
            'count(if(callType=2,true,null)) out_total',//呼出总数
            "count(if(callType=2 && callResult='接通',true,null)) out_total_effect",//呼出接通
        ];

        $where = [
            ['callTime','between',[$start,$end]],
        ];
        if(!empty(config('traffic_number')))
            $where[] = ['caller','in',config('traffic_number')];

        $rows = Traffic::field($field)->where($where)->group('date(callTime)')->select()
            ->each(function ($v){
                $v['in_not_work'] = $v['in_total'] - $v['in_work'];//非公呼入
                $v['in_rate'] = $v['in_work']?round($v['in_effect_work']/$v['in_work']*100,0):0;//呼入接通率
                $v['in_not_effect'] = $v['in_total'] - $v['in_effect'];//呼入损失量
                $v['out_rate'] = $v['out_total']?round($v['out_total_effect']/$v['out_total']*100,0):0;
            });

        if(count($rows)>0){
            $rows = setTotalAndAvg($rows,'callTime');
        }

        return $rows;
    }

    public static function hour_count($callTime='',$notPickup=false,$hour='',$on_work)
    {
        $on_work = explode(' - ',$on_work);

        if($callTime){
            $time = explode(' - ',$callTime);
            $start = $time[0].' 00:00:00';
            $end = $time[1]." 23:59:59";
        }else{
            $start = date('Y-m-d 00:00:00');
            $end = date('Y-m-d 23:59:59');
        }

        $where = [
//            ['callTime','between',[$start,$end]],
        ];

        if(!empty(config('traffic_number')))
            $where[] = ['caller','in',config('traffic_number')];

        if($notPickup){
            $field = [
                "hour(callTime) callTime",
                'count(if(status=1,true,null)) deal',//已处理
                'count(if(status=2,true,null)) unDeal',//未处理
                'count(if(status=3,true,null)) response',//已回访
                'count(if(status=4,true,null)) responseNotPickup',//已回访未接通
            ];
            $where[] = ['callType','=',1];
            $where[] = ['repeat','=',0];
        }else{
            $field = [
                "hour(callTime) callTime",
                'count(if(callType=1,true,null)) in_total',//呼入总数
                "count(if(callTime between CONCAT(date(callTime),' ".$on_work[0]."') and CONCAT(date(callTime),' ".$on_work[1]."') && callType=1,true,null)) in_work",//工作时间呼入总数
                "count(if(callType=1 && code is not null,true,null)) in_effect",//呼入接通数
                'count(if(callType=2,true,null)) out_total',//呼出总数
                "count(if(callType=2 && callResult='接通',true,null)) out_total_effect",//呼出接通
            ];
        }

        if($hour)
            array_push($field,'date(callTime) date');

        $group = $hour?'date(callTime),hour(callTime)':'hour(callTime)';

        $rows = Traffic::field($field)->where($where)->group($group);

        $i = 0;
        $line = 23;
        if($hour){
            $hour = explode(' - ',$hour);
            $hours = [];
            foreach ($hour as $v){
                $hours[] = abs(explode(':',$v)[0]);
            }
            $i = $hours[0];
            $line = $hours[1]-1;
            $rows->where("hour(callTime) BETWEEN $i AND $line")
            ->whereBetween('callTime',[date('Y-m-d',strtotime($start))." $hour[0]",date('Y-m-d',strtotime($end))." $hour[1]"]);
//                $where[] = ['hour(callTime)','between',$hours];
        }else{
            $rows->whereBetween('callTime',[$start,$end]);
        }

        $rows = $rows
//            ->fetchSql()
            ->select();
//        halt($rows);
        if($notPickup){
            $data = [];
            if(!$hour){
                foreach ($rows->toArray() as $v){
                    if(isset($data[$v['callTime']])){
                        foreach ($v as $k=>$val){
                            if($k=='callTime')
                                continue;
                            $data[$v['callTime']][$k] = isset($data[$v['callTime']][$k])?$data[$v['callTime']][$k]+$val:$val;
                        }
                    }else{
                        $data[$v['callTime']] = $v;
                    }

                }
                $rows = fillHole($data,$i,$line);

                if(count($rows)>0){
                    $rows = setTotalAndAvg($rows,'callTime',['callTime'],false,$start,$end);
                }
            }else{
                foreach ($rows->toArray() as $v){
                    $data[$v['date']][$v['callTime']] = $v;
//                    if(isset($data[$v['date']])){
//
//                    }else{
//                        $data[$v['date']][$v['callTime']]
//                    }
                }

                foreach ($data as $k=>$v){
                    $data[$k] = fillHole($v,$i,$line);
                }
//                halt($data);
                $rows = [];
                foreach ($data as $k=>$v){
                    foreach($v as $val){
                        $val['date'] = $k;
                        $rows[] = $val;
                    }
                }
                if(count($rows)>0){
                    $rows = setTotalAndAvg($rows,'callTime',['callTime','date'],false,$start,$end);
                }

//                halt($rows);
//                $rows = ['top'=>$title,'rows'=>$data];
            }

        }else{
            $rows->each(function ($v){
                $v['in_not_work'] = $v['in_total'] - $v['in_work'];//非公呼入
                $v['in_rate'] = $v['in_total']?round($v['in_effect']/$v['in_total']*100,0):0;//呼入接通率
                $v['in_not_effect'] = $v['in_total'] - $v['in_effect'];//呼入损失量
                $v['out_rate'] = $v['out_total']?round($v['out_total_effect']/$v['out_total']*100,0):0;
            });
            $data = [];
            if(!$hour){
                foreach ($rows->toArray() as $v){
                    if(isset($data[$v['callTime']])){
                        foreach ($v as $k=>$val){
                            if($k=='callTime')
                                continue;
                            $data[$v['callTime']][$k] = isset($data[$v['callTime']][$k])?$data[$v['callTime']][$k]+$val:$val;
                        }
                    }else{
                        $data[$v['callTime']] = $v;
                    }

                }
                $rows = fillHole($data,$i,$line);

                if(count($rows)>0){
                    $rows = setTotalAndAvg($rows,'callTime',['callTime'],false,$start,$end);
                }
            }else{
                foreach ($rows->toArray() as $v){
                    $data[$v['date']][$v['callTime']] = $v;
//                    if(isset($data[$v['date']])){
//
//                    }else{
//                        $data[$v['date']][$v['callTime']]
//                    }
                }

                foreach ($data as $k=>$v){
                    $data[$k] = fillHole($v,$i,$line);
                }
//                halt($data);
                $rows = [];
                foreach ($data as $k=>$v){
                    foreach($v as $val){
                        $val['date'] = $k;
                        $rows[] = $val;
                    }
                }
                if(count($rows)>0){
                    $rows = setTotalAndAvg($rows,'callTime',['callTime','date'],false,$start,$end);
                }

//                halt($rows);
//                $rows = ['top'=>$title,'rows'=>$data];
            }

//            if(count($rows)>0){
//                $rows = setTotalAndAvg($rows,'callTime');
//            }
        }

        return $rows;
    }
}
