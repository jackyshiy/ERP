<?php

namespace app\common\model;

use app\common\service\ModelService;

class Program extends ModelService
{
    protected $pk = 'program_id';

    public function product()
    {
        return $this->hasOne('Products','product_id','product_id');
    }
}
