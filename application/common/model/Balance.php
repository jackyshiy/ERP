<?php

namespace app\common\model;

use app\common\service\ModelService;

class Balance extends ModelService
{
    protected $pk = 'balance_id';

    protected $type = [
        'balance_at'=>'',
    ];
}
