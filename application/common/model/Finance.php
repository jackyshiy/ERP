<?php

namespace app\common\model;

use app\common\service\ModelService;

class Finance extends ModelService
{
    protected $type = [
        'balance_at'=>'timestamp:Y-m-d',
        'finance_at'=>'timestamp:Y-m-d',
        'trade_at'=>'timestamp:Y-m-d',
        'balance_json'=>'json',
    ];

    protected $table='balance';

    protected $id = 'balance_id';

    public function waybill()
    {
        return $this->hasOne('Waybill','waybill_num','waybill_num');
    }
}
