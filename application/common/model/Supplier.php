<?php

namespace app\common\model;

use app\common\service\ModelService;
use think\Model;

class Supplier extends ModelService
{
    public function spu()
    {
        return $this->hasMany('Spu','sup_id');
    }

    public function user()
    {
        return $this->hasOne('User','id','user_id')->joinType('left')->bind('username');
    }
}
