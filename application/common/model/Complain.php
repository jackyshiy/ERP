<?php

namespace app\common\model;

use app\common\service\ModelService;
use think\Model;

class Complain extends ModelService
{
    protected $pk = 'complain_id';

    protected $type = [
        'finished_at'=>'timestamp:Y-m-d H:i:s',
    ];
}
