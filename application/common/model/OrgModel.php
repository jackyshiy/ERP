<?php

namespace app\common\model;

use app\common\service\ModelService;
use think\Model;

class OrgModel extends ModelService
{
    protected $pk = 'org_id';

    protected $table = 'org';

    public function warehouse()
    {
        return $this->hasOne('Warehouse','org_id','org_id')->joinType('left');
    }
}
