<?php

namespace app\common\model;

use app\common\service\ModelService;
use think\Model;

class MemberAddress extends ModelService
{
    protected $pk = 'address_id';

    public function province()
    {
        return $this->hasOne('SystemAddress','id','province')->joinType('left')->bind(['province_name'=>'name']);
    }
    public function city()
    {
        return $this->hasOne('SystemAddress','id','city')->joinType('left')->bind(['city_name'=>'name']);
    }
    public function county()
    {
        return $this->hasOne('SystemAddress','id','county')->joinType('left')->bind(['county_name'=>'name']);
    }
    public function street()
    {
        return $this->hasOne('SystemAddress','id','street')->joinType('left')->bind(['street_name'=>'name']);
    }
}
