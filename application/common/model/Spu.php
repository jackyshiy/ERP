<?php

namespace app\common\model;

use app\common\service\ModelService;
use think\Model;

class Spu extends ModelService
{
    protected $pk = 'spu_id';

    protected $field = true;

    protected $type = [
        'spu_param'=>'json'
    ];

    public function supplier()
    {
        return $this->belongsTo('Supplier','supplier_id','id')->joinType('left')->bind('supplier_name');
    }

    public function sku()
    {
        return $this->hasMany('Products','spu_id','spu_id');
    }
}
