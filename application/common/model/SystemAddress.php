<?php

namespace app\common\model;

use app\common\service\ModelService;
use think\Model;

class SystemAddress extends ModelService
{

    public static function getNameById($id)
    {
        return self::where('id',$id)->value('name');
    }

    public static function getIdByName($name)
    {
        return self::whereLike('name',$name)->value('id');
    }
}
