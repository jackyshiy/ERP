<?php
// +----------------------------------------------------------------------
// | 99PHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018~2020 https://www.99php.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Mr.Chung <chung@99php.cn >
// +----------------------------------------------------------------------

namespace app\common\controller;

use think\Controller;
use think\exception\HttpResponseException;
use app\common\service\AuthService;

/**
 * 后台基础控制器
 * Class AdminController
 * @package controller
 */
class AdminController extends Controller {

    protected $model = null;

    /**
     * 开启登录控制
     * @var bool
     */
    protected $is_login = '';

    /**
     * 开启权限控制
     * @var bool
     */
    protected $is_auth = '';

    /**
     * 网站栏目
     * @var string
     */
    protected $nav = '';

    /**
     * 网站标题
     * @var string
     */
    protected $title = '';

    protected $field = '';
    /**
     * 初始化数据
     * Index constructor.
     */
    public function __construct() {
//        dump(session('user'));die;
        parent::__construct();
        list($this->nav, $this->title, $this->is_login, $this->is_auth,) = ['', '', true, true];

        //判断是否已登录
        if ($this->is_login == true && empty(session('user.id'))) {
            $data = ['type' => 'error', 'code' => 1, 'msg' => '抱歉，您还没有登录获取访问权限！', 'url' => url('@admin/login')];
            throw new HttpResponseException($this->request->isAjax() ? json($data) : redirect($data['url']));
        }

        //判断是否有权限进行访问
        if ($this->is_auth == true && AuthService::checkNode() == false) {
            $data = ['type' => 'error', 'code' => 1, 'msg' => '抱歉，您暂无该权限，请联系管理员！', 'url' => url('@admin')];
            throw new HttpResponseException($this->request->isAjax() ? json($data) : exit(msg_error($data['msg'], $data['url'])));
        }

        //判断是否有权限访问节点
    }

    /**
     * 首页
     *
     * @return mixed|\think\response\Json
     */
    public function index()
    {
        if($this->request->isAjax()){
            $limit = $this->request->param('limit',config('limit'));
            $search = $this->request->param('search',[]);
            $where = [
                [$this->model->getTable().'.is_deleted','=',0]
            ];
            $rows = $this->model->where(get_query_condition($where,$search),$this->model->getTable());

            $rows = $this->__list($rows)->paginate($limit);
            $rows = $this->__each($rows);

            return __success('success',$rows->items(),1,$rows->total(),$limit,$rows->currentPage(),$rows->lastPage());
        }

        $var = [

        ];
        return $this->fetch('',$var);
    }

    /**
     * 添加页
     *
     * @return mixed|\think\response\Json
     */
    public function create()
    {
        if($this->request->isAjax()){
            $post = $this->request->post();

            $validate = $this->validate($post, 'app\common\validate\\'.get_class_name($this->model).'.add');
            if (true !== $validate) return __error($validate);

            $this->model->create(save_common_field($this->__beforeSave('create')));

            return __success('操作成功');
        }

        $var = $this->__beforeDisplay('form');
        return $this->fetch('form',$var);
    }

    /**
     * 编辑页
     *
     * @param $id
     * @return mixed|\think\response\Json
     */
    public function update($id)
    {
        if($this->request->isAjax()){
            $post = $this->request->post();

            $validate = $this->validate($post, 'app\common\validate\\'.get_class_name($this->model).'.edit');
            if (true !== $validate) return __error($validate);

            $this->model->update(save_common_field($this->__beforeSave('update'),1));

            return __success('操作成功');
        }

        $var = [
            'data'=>$this->model->find($id),
        ];

        $extra = $this->__beforeDisplay('form');
        $var = !empty($extra)?array_merge($var,$extra):$var;

        return $this->fetch('form',$var);
    }

    /**
     * 软删除
     *
     * @return \think\response\Json
     */
    public function delete()
    {
        $id = $this->request->param('id');
        if(!$id)
            return __error('缺少删除ID');
        $this->__beforeDelete($id);
        //执行删除操作
        return $this->model->delData($id);
    }

    /**
     * 回收站
     *
     * @return mixed|\think\response\Json
     */
    public function dustbin()
    {
        if($this->request->isAjax()){
            $limit = $this->request->param('limit',config('limit'));
            $search = $this->request->param('search',[]);
            $where = [
                [$this->model->getTable().'.is_deleted','=',1]
            ];
            $rows = $this->model->where(get_query_condition($where,$search,$this->model->getTable()));

            $rows = $this->__list($rows)->paginate($limit);

            return __success('success',$rows->items(),1,$rows->total(),$limit,$rows->currentPage(),$rows->lastPage());
        }

        $var = [

        ];
        return $this->fetch('',$var);
    }

    /**
     * 还原
     *
     * @return \think\response\Json
     */
    public function restore()
    {
        $id = $this->request->get('id');
        if(!$id)
            return __error('请选择需要还原的信息');
        try{
            $this->model->where($this->model->getPk(),'in',$id)->update(['is_deleted'=>0]);
        }catch (\Exception $e){
            return __error($e->getMessage());
        }
        return __success('还原成功');
    }

    /**
     * 真删除
     *
     * @return \think\response\Json
     */
    public function destroy()
    {
        $id = $this->request->get('id');
        if(!$id)
            return __error('请选择需要彻底删除的信息');
        try{
            $this->model->where($this->model->getPk(),'in',$id)->delete();
        }catch (\Exception $e){
            return __error($e->getMessage());
        }
        return __success('删除成功');
    }

    /**
     * 状态开关     1是2否
     *
     * @return \think\response\Json
     */
    public function status()
    {
        $param = $this->request->param();
        $validate = $this->validate($param, 'app\common\validate\Common.status');
        if (true !== $validate) return __error($validate);

        $status = $this->model->where('id',$param['id'])->value('status');
        $this->model->update([$param['name']=>$status==1?2:1],['id'=>$param['id']]);

        return __success('修改成功');
    }

    protected function __beforeSave($action)
    {
        return $this->request->param();
    }

    protected function __beforeDelete($id)
    {
        return [];
    }

    protected function __beforeDisplay($action,$data=[])
    {
        return $data;
    }

    protected function __list($model,$where=[])
    {
        return $model->where($where);
    }

    protected function __each($rows){
        return $rows;
    }
}