<?php

namespace app\common\validate;

use think\Validate;

class DeliverSpeciel extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'=>'require',
        'route_id'=>'require',
        'spu_id'=>'require',
        'wh_id'=>'require',
        'deliver_id'=>'require',
        'remark'=>'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require'=>'缺少ID',
        'route_id.require'=>'请选择通路',
        'spu_id.require'=>'请选择商品',
        'wh_id.require'=>'请选择仓库',
        'deliver_id.require'=>'请选择车队',
        'remark.require'=>'请填写备注',
    ];

    protected $scene = [
        'add'=>['route_id','spu_id'],

        'edit'=>['route_id','spu_id','id'],

        'del'=>['id'],
    ];

}
