<?php

namespace app\common\validate;

use think\Validate;

class Jd extends Validate
{
    protected $rule = [
        'waybill_id'=>'require',
        'customerCode'=>'require',
        'waybillCode'=>'require',
        'orderId'=>'require',
        'senderName'=>'require',
        'senderAddress'=>'require',
        'senderMobile'=>'require',
        'receiveName'=>'require',
        'receiveAddress'=>'require',
        'receiveTel'=>'require',
        'receiveMobile'=>'require',
        'packageCount'=>'require',
        'weight'=>'require',
        'vloumLong'=>'require',
        'vloumWidth'=>'require',
        'vloumHeight'=>'require',
        'vloumn'=>'require',
        'description'=>'require',
        'collectionValue'=>'require',
        'collectionMoney'=>'require',
        'guaranteeValue'=>'require',
        'guaranteeValueAmount'=>'require',
        'signReturn'=>'require',
        'remark'=>'require',
        'goods'=>'require',
        'goodsCount'=>'require',
        'wareHouseCode'=>'require',
//        'signReturn'=>'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'waybill_id.require'=>'缺少运单ID',
        'customerCode.require'=>'缺少客户编码',
        'waybillCode.require'=>'缺少客户运单号',
        'orderId.require'=>'缺少订单号',
        'senderName.require'=>'缺少寄件人',
        'senderAddress.require'=>'缺少寄件人地址',
        'senderMobile.require'=>'缺少寄件人电话',
        'receiveName.require'=>'缺少收件人',
        'receiveAddress.require'=>'缺少收件人地址',
        'receiveTel.require'=>'缺少收件人手机',
        'receiveMobile.require'=>'缺少收件人固话',
        'packageCount.require'=>'缺少包裹数量',
        'weight.require'=>'缺少重量',
        'vloumLong.require'=>'缺少长度',
        'vloumWidth.require'=>'缺少宽度',
        'vloumHeight.require'=>'缺少高度',
        'vloumn.require'=>'缺少体积',
        'description.require'=>'缺少描述',
        'collectionValue.require'=>'缺少代收货款信息',
        'collectionMoney.require'=>'缺少是否代收款金额',
        'guaranteeValue.require'=>'缺少保价信息',
        'guaranteeValueAmount.require'=>'缺少保价金额',
        'signReturn.require'=>'缺少签还单信息',
        'remark.require'=>'缺少备注',
        'goods.require'=>'缺少商品信息',
        'goodsCount.require'=>'缺少商品数量',
        'wareHouseCode.require'=>'缺少仓库编码',
    ];

    protected $scene = [
        'jd_trade'=>['customerCode','waybillCode',],

        'jd_order'=>[
            'orderId','senderName','senderAddress','senderMobile','receiveName','receiveAddress','packageCount','weight',
            'vloumLong','vloumWidth','vloumHeight','vloumn','description','collectionValue','collectionMoney',
            'guaranteeValue','guaranteeValueAmount','signReturn','remark','goods','goodsCount',
        ],

        'jd_check'=>['customerCode','orderId','receiveAddress','wareHouseCode',],
    ];
}
