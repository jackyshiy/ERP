<?php

namespace app\common\validate;

use think\Validate;

class SpuRule extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    'spu_num'=>'require',
        'wh_id'=>'require',
        'route_id'=>'require',
        'deliver_id'=>'require',
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        'spu_num.require'=>'请填写商品编码',
        'wh_id.require'=>'请选择仓库',
        'route_id.require'=>'请选择通路',
        'deliver_id.require'=>'请选择车队',
    ];

    protected $scene = [
        'save'=>['spu_num','wh_id','route_id','deliver_id',],
    ];
}
