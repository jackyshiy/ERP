<?php

namespace app\common\validate;

use app\common\model\User;
use think\Validate;

class Supplier extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    'id'=>'require',
        'supplier_name'=>'require',
        'supplier_num'=>'require',
        'supplier_master'=>'require',
        'supplier_contact'=>'require',
        'remark'=>'require',
        'user_id'=>'require|exist',
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        'id.require'=>'缺少更新ID',
        'supplier_name.require'=>'请填写供应商名',
        'supplier_num.require'=>'请填写供应商编码',
        'supplier_master.require'=>'请填写负责人',
        'supplier_contact.require'=>'请填写联系电话',
        'remark.require'=>'请填写备注',
        'user_id.require'=>'请选择管理员',
    ];

    protected $scene = [
        'add'=>['supplier_name','supplier_num','supplier_master','supplier_contact',],

        'edit'=>['supplier_name','supplier_num','supplier_master','supplier_contact','id'],

        'del'=>['id'],
    ];

    public function exist($value,$rule,$data=[])
    {
        $user = User::find($value);
        if(empty($user)) return '暂无账户数据，请稍后再试！';
        if($user->status!=1) return '账户已停用！';
        return true;
    }
}
