<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/10/11
 * Time: 2:59
 */

namespace app\common\validate;


use think\Validate;

/**
 * 公共验证规则
 * Class Common
 * @package app\admin\validate
 */
class Common extends Validate {
    /**
     * 验证规则
     * @var array
     */
    protected $rule = [
        'id'    => 'require|number',
        'name' => 'require',
        'value' => 'require|max:30',
    ];

    /**
     * 错误提示
     * @var array
     */
    protected $message = [
        'id.require'    => '缺少ID',
        'name.require' => '缺少字段',
        'value.require' => '修改值必须',
    ];

    /**
     * 验证场景
     * @var array
     */
    protected $scene = [
        //修改字段值
        'status' => ['id', 'name'],
    ];
}