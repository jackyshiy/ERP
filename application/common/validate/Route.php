<?php

namespace app\common\validate;

use think\Validate;

class Route extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'=>'require',
        'route_name'=>'require',
        'route_desc'=>'require',
        'remark'=>'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require'=>'缺少更新ID',
        'route_name.require'=>'请填写通路名',
        'remark.require'=>'请填写备注',
    ];

    protected $scene = [
        'add'=>['route_name',],

        'edit'=>['route_name','id'],

        'del'=>['id'],
    ];

}
