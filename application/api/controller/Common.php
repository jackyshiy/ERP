<?php

namespace app\api\controller;

use app\common\controller\ApiController;
use app\common\model\Order;
use app\common\model\OrgModel;
use app\common\model\Products;
use app\common\model\Spu;
use app\common\model\SystemAddress;
use app\common\model\WxpayLog;
use app\common\service\WechatPaymentService;
use think\Controller;
use think\Request;
use Overtrue\Pinyin\Pinyin;

class Common extends ApiController
{
    public function get_addr($id,$order='city')
    {
        $rows = SystemAddress::where('pid',$id);
        if($order == 'city'){
            $rows = $rows->orderRaw("field(name,'东莞市','佛山市') desc")->select();
        }else{
            $rows = $rows->select();
        }
        if(!$rows)
            return json(__error('获取失败'));
        return json(__successData('success',$rows));
    }

    public function upload(Request $request)
    {
//        {
//            "code": 0 //0表示成功，其它失败
//            ,"msg": "" //提示信息 //一般上传失败后返回
//            ,"data": {
//                    "src": "图片路径"
//            ,"title": "图片名称" //可选
//            }
//        }
        $path = './uploads/spu/';
        $r = upload($request->file('file'),$path,['ext'=>'jpeg,png,gif,jpg']);
        $r['data']['src'] = $request->domain().$path.$r['data']['saveName'];
        return $r;
    }

    /**
     * 根据通路ID获取车队
     *
     * @param $id                       通路ID
     * @return \think\response\Json
     */
    public function get_sender($id)
    {
        $rows = OrgModel::where("find_in_set($id,route_id)")->find();

        if(!$rows)
            return __error('没有车队信息');


        return __successData('success',$rows);
    }

    public function get_spu($id)
    {
        $rows = Spu::find($id);

//        dump($rows);
        if(!$rows)
            return __error('没有数据');
//        if(!$rows['on_sale'])
//            return __error('商品已下架');

        if($rows->spu_param){
            $rows->spu_param = array_values($rows->spu_param);
            foreach($rows->spu_param as $k=>$v){
                if($k>1){
                    $rows['sku_count'] *= count($v['value']);
                }else{
                    $rows['sku_count'] = count($v['value']);
                }

            }
        }

        return __successData('success',$rows);
    }

    public function get_sku(Request $request)
    {
        $pinyin = new Pinyin();
        $get = $request->get();
        $spu_id = $get['spu_id'];
        unset($get['spu_id']);
        $where = [['spu_id','=',$spu_id]];
        foreach ($get as $k=>$v){
            $key = $pinyin->permalink($k,'')?:$k;
            $where[] = ['sku_param->'.$key,'=',$v];
        }
        $sku = Products::where($where)->select()->toArray();
//        dump($sku);
        if(count($sku)>1)
            return __error('请继续其他规格参数');
        $sku = $sku[0];
        if(!$sku)
            return __error('没有商品信息');
        if(!$sku['on_sale'])
            return __error('商品已下架',$sku);
        if($sku['stock']<=0)
            return __error('商品库存不足',$sku);

        return __successData('success',$sku);
    }

    public function wxpay_notify()
    {
        $app = (new WechatPaymentService())->app();
        $response = $app->handlePaidNotify(function ($message, $fail) {
            $order = Order::with('pay_log')->where('out_trade_no',$message['out_trade_no'])->find();
            if(!$order || $order->result_code == 'SUCCESS')
                return true;

            //检查订单支付状态
            $wx_order = (new WechatPaymentService())->order_check($message['out_trade_no']);
            if(empty($wx_order))
                $fail('订单查询失败');
            if($wx_order['return_code']=='SUCCESS'){
                if ($wx_order['result_code'] === 'SUCCESS' && $wx_order['trade_state'] === 'SUCCESS' && $message[''] == $wx_order[''] && $message[''] == $order->ord_money){
                    $message['ord_id'] = $order->ord_id;
                    $message['request'] = $message;
                    WxpayLog::create($message);
                    return true;
                }
            }else{
                $fail('通信失败，请稍后再通知我');
            }
        });

        $response->send();
    }
}
