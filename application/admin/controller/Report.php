<?php

namespace app\admin\controller;

use app\common\controller\AdminController;
use think\Request;
use app\common\model\Waybill;
use app\common\model\Products;
use app\common\model\Order;
use app\common\model\Program;
use app\common\model\Spu;
use app\common\model\Complain;
use app\common\model\Traffic;

class Report extends AdminController
{
    public function forward()
    {
        if($this->request->isAjax()){
            $param = $this->request->param('select.waybill_at');
            $order_field = $this->request->param('field');
            $order = $this->request->param('order');
            $orderby = $order_field && $order ? "$order_field $order" :'y,m,d asc';

//            dd($param);
            $days = date('t');
            $waybill_at_default = date('Y-m-01').' - '.date('Y-m-'.$days);
            $waybill_at = $param?setSearchTime($param):setSearchTime($waybill_at_default);
//            dd($waybill_at);
            if(!$waybill_at[0] || !$waybill_at[1])
                return __error('日期格式错误');
            if($waybill_at[1]<$waybill_at[0])
                return __error('结束时间不能大于开始时间');


            $field = [
                "FROM_UNIXTIME(waybill_at,'%Y') y",//年
                "FROM_UNIXTIME(waybill_at,'%m') m",//月
                "FROM_UNIXTIME(waybill_at,'%d') d",//日
                //"FROM_UNIXTIME(waybill_at,'%Y-%m-%d') title",
                'count(1) ck',//出库
                'if(isnull(sum(ord_money)),0,sum(ord_money)) ord_money',//总订单金额
                'sum(waybill.collection_payment) collection_payment',//总COD
                'count(status=2 or null) tt',//妥投
                'sum(if(status=2,ord_money,0)) ttMoney',//妥投订单金额
                'sum(if(status=2,waybill.collection_payment,0)) ttCod',//妥投代收货款金额
                'count(status=-1 or null) js',//拒收
                'sum(if(status=-1,ord_money,0)) jsMoney',//拒收订单金额
                'sum(if(status=-1,waybill.collection_payment,0)) jsCod',//拒收妥投代收货款金额
                'count(status!=0 or null) rck',//实际出库
                'sum(if(status!=0,ord_money,0)) rckMoney',//实际出库订单金额
                'sum(if(status!=0,waybill.collection_payment,0)) rckCod',//实际出库妥投代收货款金额
                'count(status=1 or null) zt',//在途
                'sum(if(status=1,ord_money,0)) ztMoney',//在途订单金额
                'sum(if(status=1,waybill.collection_payment,0)) ztCod',//在途妥投代收货款金额
                'count(status=-2 or null) qx',//取消
                'sum(if(status=-2,ord_money,0)) qxMoney',//取消订单金额
                'sum(if(status=-2,waybill.collection_payment,0)) qxCod',//取消妥投代收货款金额

            ];

            $where = [
                ['waybill_at','between time',$waybill_at],
            ];

//            dd($where);
            $row = Waybill::leftJoin('order','order.ord_num = waybill.ord_num')
                ->field($field)
                ->where($where)
                ->group('y,m,d')
                ->order($orderby)
//                ->fetchSql()
                ->select()
                ->toArray()
            ;

            $total['ck'] = $total['tt'] = $total['ttMoney'] = $total['ttCod'] = $total['js'] = $total['jsMoney'] = $total['jsCod']
                = $total['zt'] = $total['ztMoney'] = $total['ztCod'] = $total['qx'] = $total['qxMoney'] = $total['qxCod']
                = $total['rck'] = $total['rckMoney'] = $total['rckCod'] = $total['ord_money'] = $total['collection_payment']= 0;
            $total['title'] = '合计';
            $average = [];
            $average['title'] = '平均';
            $c = count($row);
            foreach($row as $k=>$v){
                $row[$k]['title'] = "$v[y]-$v[m]-$v[d]";
                $total['ck'] += $v['ck'];
                $total['tt'] += $v['tt'];
                $total['ttMoney'] += $v['ttMoney'];
                $total['ttCod'] += $v['ttCod'];
                $total['js'] += $v['js'];
                $total['jsMoney'] += $v['jsMoney'];
                $total['jsCod'] += $v['jsCod'];
                $total['zt'] += $v['zt'];
                $total['ztMoney'] += $v['ztMoney'];
                $total['ztCod'] += $v['ztCod'];
                $total['qx'] += $v['qx'];
                $total['qxMoney'] += $v['qxMoney'];
                $total['qxCod'] += $v['qxCod'];
                $total['rck'] += $v['rck'];
                $total['rckMoney'] += $v['rckMoney'];
                $total['rckCod'] += $v['rckCod'];
                $total['ord_money'] += $v['ord_money'];
                $total['collection_payment'] += $v['collection_payment'];
            }
//            dd($total);
            foreach ($total as $k=>$v){
//                echo round($v/$c,2).'<br>';
                if($k!='title')
                    $average[$k] = $c?round($v/$c,2):0;
            }

//            dd($average);
//            dd(array_merge([$average,$total],$row));die;
            return __successData('success',array_merge([$average,$total],$row));
        }

        return $this->fetch();
    }


    public function program_efficiency()
    {
        if($this->request->isAjax()){
            $param = $this->request->param('select.ord_at');
            $order_field = $this->request->param('field');
            $order = $this->request->param('order');
            $orderby = $order_field && $order ? "$order_field $order" :'title,program_id asc';
            $search = $this->request->param('select','');

            $days = date('t');
            $ord_at_default = date('Y-m-01').' - '.date('Y-m-'.$days);
            $ord_at = $param?setSearchTime($param):setSearchTime($ord_at_default);

            if(!$ord_at[0] || !$ord_at[1])
                return __error('日期格式错误');
            if($ord_at[1]<$ord_at[0])
                return __error('结束时间不能大于开始时间');

            $where = [
                ['ord_at','between time',$ord_at],
                ['order.is_deleted','=',0],
                ['order.program_id','>',0]
            ];
            $whereSql = [];

            if($search){
                foreach($search as $k=>$v){
                    if($v=='') continue;
                    if($k == 'keyword'){
                        $where[] = ['program_name','like',"%$v%"];
                        continue;
                    }
                    if($k == 'ord_at'){
                        $where[] = ['ord_at','between time',setSearchTime($v)];
                        continue;
                    }
                    if($k == 'week'){
                        $whereSql = setWhere($whereSql,"WEEKDAY(FROM_UNIXTIME(ord_at,'%Y-%m-%d')) = ".($v-1));
//                        $where[] = ["WEEKDAY(FROM_UNIXTIME(ord_at,'%Y-%m-%d'))",'=',$v-1];
                        continue;
                    }
                    $where[] = [$k,'in',$v];
                }
            }

            $field = [
                "FROM_UNIXTIME(ord_at,'%Y') y",//年
                "FROM_UNIXTIME(ord_at,'%m') m",//月
                "FROM_UNIXTIME(ord_at,'%d') d",//日
                "FROM_UNIXTIME(ord_at,'%Y-%m-%d') title",
                'order.spu_id',
                'order.program_id',
                'GROUP_CONCAT(order.program_id) program_ids',
                'program_name',
                'channel',
                'ord_at',
                'present_at',
                'duration',
                'position',
                'org_price',
                'price',
                'profit_ratio',
                'profit_goal',
                'sum(quantity) quantity',
                'program_start',
                'program_end',
                'count(1) ck',//出库
                'if(isnull(sum(ord_money)),0,sum(ord_money)) ord_money',//总订单金额
                'sum(waybill.collection_payment) collection_payment',//总COD
                'count(status=2 or null) tt',//妥投
                'sum(if(status=2,ord_money,0)) ttMoney',//妥投订单金额
                'sum(if(status=2,waybill.collection_payment,0)) ttCod',//妥投代收货款金额
                'count(status=-1 or null) js',//拒收
                'sum(if(status=-1,ord_money,0)) jsMoney',//拒收订单金额
                'sum(if(status=-1,waybill.collection_payment,0)) jsCod',//拒收妥投代收货款金额
                'count(status=0 or null) zt',//在途
                'sum(if(status=0,ord_money,0)) ztMoney',//在途订单金额
                'sum(if(status=0,waybill.collection_payment,0)) ztCod',//在途妥投代收货款金额
                'count(status=-2 or null) qx',//取消
                'sum(if(status=-2,ord_money,0)) qxMoney',//取消订单金额
                'sum(if(status=-2,waybill.collection_payment,0)) qxCod',//取消妥投代收货款金额

            ];

            $rows = Order::leftJoin('spu','spu.spu_id = order.spu_id')
                ->leftJoin('program','program.program_id = order.program_id')
                ->leftJoin('waybill','waybill.ord_id = order.ord_id')
                ->where($where)
                ->where($whereSql)
                ->field($field)
                ->group('title,program_id')
                ->order($orderby)
//                ->fetchSql()
                ->select()
                ->toArray()
            ;

            $data = $program_ids = $title = [];
            $flag = '';
            foreach($rows as $k=>&$v){
                if($flag&&$flag != $v['title']){
                    $program = Program::where('program_id','not in',$program_ids)
                        ->where(strtotime($flag).' between program_start and program_end')
                        ->select()
                        ->each(function($v)use($flag){
                            $v['title'] = $flag;
                            $v['week'] = getWeek($flag);
                        })
                        ->toArray();
                    if($program)
                        $data = array_merge($data,$program);
                    $flag = '';
                    $program_ids = [];
                }
                if(!$v['org_price']){
                    $product = Products::where('spu_id',$v['spu_id'])->find();
                    $v['org_price'] = $product['org_price'];
                    $v['price'] = $product['price'];
                }
                $v['week'] = getWeek("$v[y]-$v[m]-$v[d]");
                $v['count'] = $v['quantity'];
                $v['totalMoney'] = $v['price']*$v['quantity'];//档位订购金额
                $v['profitMoney'] = bcmul(($v['price'] - $v['org_price']),$v['quantity'],2);//档位毛利额
                $v['totalMin'] = $v['duration']?round($v['price']*$v['quantity']/$v['duration'],2):0;//分钟产值
                $v['profitMin'] = $v['duration']?round(($v['price'] - $v['org_price'])*$v['quantity']/$v['duration'],2):0;//分钟毛利额
                $v['profitGoal'] = $v['duration']?round($v['profit_goal']/$v['duration'],2):0;//档位毛利目标/分钟
                $v['goalTask'] = $v['duration']?round(($v['price'] - $v['org_price'])*$v['quantity']/$v['duration']/($v['profit_goal']/$v['duration']),2):0;//档位毛利达成率
                $v['orgProfitMin'] = $v['duration']?bcmul(($v['price'] - $v['org_price'])*$v['quantity']/$v['duration'],$v['profit_ratio'],2):0;//还原档位毛利额/分钟
                $v['orgProfit'] = bcmul(($v['price'] - $v['org_price'])*$v['quantity'],$v['profit_ratio'],2);//还原档位毛利额
                $data[] = $v;
                $program_ids[] = $v['program_id'];
                $flag = $v['title'];

                if(array_search($v['title'],$title)===false){
                    $title[] = $v['title'];
                }
            }

            if(count($title)==1){
                $program = Program::where('program_id','not in',$program_ids)
                    ->where(strtotime($flag).' between program_start and program_end')
                    ->select()
                    ->each(function($v)use($flag){
                        $v['title'] = $flag;
                        $v['week'] = getWeek($flag);
                    })
                    ->toArray();
                if($program)
                    $data = array_merge($data,$program);
            }

//            halt($data);

            return __successData('success',$data);
        }

        $var = [
            'week'=>config('week'),
            'channel'=>Program::group('channel')->column('channel'),
            'position'=>Program::group('position')->column('position'),
            'duration'=>Program::group('duration')->order('duration')->column('duration'),
            'present_at'=>Program::group('present_at')->order('present_at')->column('present_at'),
        ];

        return $this->fetch('',$var);
    }

    /*public function program_efficiency1()
    {
        if($this->request->isAjax()){
            $param = $this->request->param('select.ord_at');
            $order_field = $this->request->param('field');
            $order = $this->request->param('order');
            $orderby = $order_field && $order ? "$order_field $order" :'y,m,d,program_id,ord_id asc';
            $search = $this->request->param('select','');

            $days = date('t');
            $ord_at_default = date('Y-m-01').' - '.date('Y-m-'.$days);
            $ord_at = $param?setSearchTime($param):setSearchTime($ord_at_default);

            if(!$ord_at[0] || !$ord_at[1])
                return __error('日期格式错误');
            if($ord_at[1]<$ord_at[0])
                return __error('结束时间不能大于开始时间');


            $where = [
                ['ord_at','between time',$ord_at],
                ['products.is_deleted','=',0]
            ];

            $whereSql = [];

            if($search){
                foreach($search as $k=>$v){
                    if($v=='') continue;
                    if($k == 'keyword'){
                        $where[] = ['program_name','like',"%$v%"];
                        continue;
                    }
                    if($k == 'ord_at'){
                        $where[] = ['ord_at','between time',setSearchTime($v)];
                        continue;
                    }
                    if($k == 'week'){
                        $whereSql = setWhere($whereSql,"WEEKDAY(FROM_UNIXTIME(ord_at,'%Y-%m-%d')) = ".($v-1));
//                        $where[] = ["WEEKDAY(FROM_UNIXTIME(ord_at,'%Y-%m-%d'))",'=',$v-1];
                        continue;
                    }
                    $where[] = [$k,'=',$v];
                }

            }

//            dd($where);
//            dd($whereSql);
            $field = [
                "FROM_UNIXTIME(ord_at,'%Y') y",//年
                "FROM_UNIXTIME(ord_at,'%m') m",//月
                "FROM_UNIXTIME(ord_at,'%d') d",//日
//                "FROM_UNIXTIME(ord_at,'%Y-%m-%d') title",
                'ord_id',
                'program_id',
                'program_name',
                'channel',
                'ord_at',
                'present_at',
                'duration',
                'position',
                'orig_price',
                'price',
                'profit_ratio',
                'profit_goal',
                'quantity',
                'program_start',
                'program_end',
//                'count(1) ck',//出库
//                'if(isnull(sum(ord_money)),0,sum(ord_money)) ord_money',//总订单金额
//                'sum(waybill.collection_payment) collection_payment',//总COD
//                'count(status=1 or null) tt',//妥投
//                'sum(if(status=1,ord_money,0)) ttMoney',//妥投订单金额
//                'sum(if(status=1,waybill.collection_payment,0)) ttCod',//妥投代收货款金额
//                'count(status=-1 or null) js',//拒收
//                'sum(if(status=-1,ord_money,0)) jsMoney',//拒收订单金额
//                'sum(if(status=-1,waybill.collection_payment,0)) jsCod',//拒收妥投代收货款金额
//                'count(status=0 or null) zt',//在途
//                'sum(if(status=0,ord_money,0)) ztMoney',//在途订单金额
//                'sum(if(status=0,waybill.collection_payment,0)) ztCod',//在途妥投代收货款金额
//                'count(status=-2 or null) qx',//取消
//                'sum(if(status=-2,ord_money,0)) qxMoney',//取消订单金额
//                'sum(if(status=-2,waybill.collection_payment,0)) qxCod',//取消妥投代收货款金额

            ];

            $row = Order::leftJoin('products','products.product_num = order.good_num')
                ->leftJoin('program','find_in_set(products.product_id,program.product_id)')
                ->where($where)
                ->where("TIMESTAMPDIFF(MINUTE,CONCAT_WS(' ',FROM_UNIXTIME(ord_at,'%Y-%m-%d'),present_at),FROM_UNIXTIME(ord_at,'%Y-%m-%d %H:%i:%s')) BETWEEN 0 AND duration")
                ->where('ord_at BETWEEN program_start AND program_end')
                ->where($whereSql)
                ->field($field)
//                ->group('y,m,d')
                ->order($orderby)
//                ->fetchSql()
                ->select()
                ->toArray()
                ;
//            dd($row);die;

            $rows = [];
            $ctime = 0;
            foreach ($row as $k=>$v){
//                dd(date('Y-m-d',$ctime));
                if($search){
                    if(!$ctime){
                        $ctime = strtotime("$v[y]-$v[m]-$v[d]");
                    }else{

                        if(strtotime("$v[y]-$v[m]-$v[d]") - $ctime > 86400){
//                        dd("$v[y]-$v[m]-$v[d]");
//                        dd(date('Y-m-d',$ctime));
//                        dd("$v[y]-$v[m]-$v[d]");
//                        dd(strtotime("$v[y]-$v[m]-$v[d]") - $ctime > 86400);
                            $j = ceil(strtotime("$v[y]-$v[m]-$v[d]") - $ctime)/86400;
                            for($i=1;$i<=$j;$i++){
//                            echo $i;
                                $empty['y'] = date('Y',$ctime);
                                $empty['m'] = date('m',$ctime);
                                $empty['d'] = date('d',$ctime);
                                $empty['title'] = "$empty[y]-$empty[m]-$empty[d]";
                                $empty['week'] = getWeek("$empty[y]-$empty[m]-$empty[d]");
                                $rows["$empty[y]-$empty[m]-$empty[d]"][] = $empty;
                                $ctime += 86400;
                            }
//                        dd(date('Y-m-d',$ctime));
//                        dd($rows);

                            continue;

                        }
                    }
                }

                if(isset($rows["$v[y]-$v[m]-$v[d]"][$v['program_id']])){
                    $arr = &$rows["$v[y]-$v[m]-$v[d]"][$v['program_id']];
                    $arr['count'] += $v['quantity'];
                    $arr['totalMoney'] += $v['price']*$v['quantity'];//档位订购金额
                    $arr['profitMoney'] += bcmul(($v['price'] - $v['orig_price']),$v['quantity'],2);//档位毛利额
                    $arr['totalMin'] += round($v['price']*$v['quantity']/$v['duration'],2);//分钟产值
                    $arr['profitMin'] += round(($v['price'] - $v['orig_price'])*$v['quantity']/$v['duration'],2);//分钟毛利额
//                $arr['profitGoal'] = round($v['profit_goal']/$v['duration'],2);//档位毛利目标/分钟
                    $arr['goalTask'] += round(($v['price'] - $v['orig_price'])*$v['quantity']/$v['duration']/($v['profit_goal']/$v['duration']),2);//档位毛利达成率
                    $arr['orgProfitMin'] += bcmul(($v['price'] - $v['orig_price'])*$v['quantity']/$v['duration'],$v['profit_ratio'],2);//还原档位毛利额/分钟
                    $arr['orgProfit'] += bcmul(($v['price'] - $v['orig_price'])*$v['quantity'],$v['profit_ratio'],2);//还原档位毛利额
                }else{
                    $v['title'] = "$v[y]-$v[m]-$v[d]";
                    $v['week'] = getWeek("$v[y]-$v[m]-$v[d]");
                    $v['count'] = $v['quantity'];
                    $v['totalMoney'] = $v['price']*$v['quantity'];//档位订购金额
                    $v['profitMoney'] = bcmul(($v['price'] - $v['orig_price']),$v['quantity'],2);//档位毛利额
                    $v['totalMin'] = round($v['price']*$v['quantity']/$v['duration'],2);//分钟产值
                    $v['profitMin'] = round(($v['price'] - $v['orig_price'])*$v['quantity']/$v['duration'],2);//分钟毛利额
                    $v['profitGoal'] = round($v['profit_goal']/$v['duration'],2);//档位毛利目标/分钟
                    $v['goalTask'] = round(($v['price'] - $v['orig_price'])*$v['quantity']/$v['duration']/($v['profit_goal']/$v['duration']),2);//档位毛利达成率
                    $v['orgProfitMin'] = bcmul(($v['price'] - $v['orig_price'])*$v['quantity']/$v['duration'],$v['profit_ratio'],2);//还原档位毛利额/分钟
                    $v['orgProfit'] = bcmul(($v['price'] - $v['orig_price'])*$v['quantity'],$v['profit_ratio'],2);//还原档位毛利额
                    $rows["$v[y]-$v[m]-$v[d]"][$v['program_id']] = $v;
                }
//                dd($rows);
                $ctime += 86400;
            }
//            dd($rows);die;
            $list = [];
            if($rows){
                foreach($rows as $k=>$v){

                    $keys = array_keys($v);

                    $first = reset($v);

                    $key = key($v);

                    $except = Program::field('program_id,program_name,channel,present_at,duration,position,profit_goal,program_start,program_end')
                        ->leftJoin('products','find_in_set(products.product_id,program.product_id)')
                        ->leftJoin('order','products.product_num = order.good_num')
                        ->where($where)
                        ->where('program_id','not in',$keys)
                        ->where(strtotime("$first[y]-$first[m]-$first[d]").' BETWEEN program_start AND program_end')
                        ->where('program.is_deleted',0)
                        ->where($whereSql)
                        ->group('program_id')
//                    ->fetchSql()
                        ->select()
                        ->toArray()
                    ;

//                    dd($first);die;
                    foreach($except as $k=>$val){
                        $except[$k]['week'] = $first['week'];
                        $except[$k]['title'] = $first['title'];
                        $except[$k]['count'] = 0;
                        $except[$k]['price'] = 0;
                        $except[$k]['totalMoney'] = 0;
                        $except[$k]['profitMoney'] = 0;//
                        $except[$k]['totalMin'] = 0;
                        $except[$k]['profitMin'] = 0;
                        $except[$k]['profitGoal'] = 0;
                        $except[$k]['goalTask'] = 0;;
                        $except[$k]['orgProfitMin'] = 0;
                        $except[$k]['orgProfit'] = 0;
                        $except[$k]['profit_goal'] = 0;
                    }
                    if($key==0){
                        $list = array_merge($list,$except);
                    }else{
                        $list = array_merge($list,$v,$except);
                    }
//                dd(array_merge($v,$except));die;
//                    $list = array_merge($list,$v,$except);

//            array_push($list,array_merge($except,$v));
//            dd(array_merge($except,$v));
                }
            }else{

            }


            $total['totalMoney'] = $total['profitMoney'] = $total['totalMin'] = $total['profitMin'] = $total['profitGoal']
                = $total['goalTask'] = $total['orgProfitMin'] = $total['orgProfit'] = $total['count'] = $total['price'] = $total['profit_goal'] = 0;
            $total['title'] = '合计';
            $average = [];
            $average['title'] = '平均';
            $c = count($list);
            foreach ($list as $v){
                $total['count'] += isset($v['count'])?$v['count']:0;
                $total['price'] += isset($v['price'])?$v['price']:0;
                $total['totalMoney'] += isset($v['totalMoney'])?$v['totalMoney']:0;
                $total['profitMoney'] += isset($v['profitMoney'])?$v['profitMoney']:0;//
                $total['totalMin'] += isset($v['totalMin'])?$v['totalMin']:0;
                $total['profitMin'] += isset($v['profitMin'])?$v['profitMin']:0;
                $total['profitGoal'] += isset($v['profitGoal'])?$v['profitGoal']:0;
                $total['goalTask'] += isset($v['goalTask'])?$v['goalTask']:0;
                $total['orgProfitMin'] += isset($v['orgProfitMin'])?$v['orgProfitMin']:0;
                $total['orgProfit'] += isset($v['orgProfit'])?$v['orgProfit']:0;
                $total['profit_goal'] += isset($v['profit_goal'])?$v['profit_goal']:0;
            }

            foreach ($total as $k=>$v){
//                echo round($v/$c,2).'<br>';
                if($k!='title')
                    $average[$k] = $c?round($v/$c,2):0;
            }

            return __successData('success',array_merge([$average,$total],$list));
        }

//        dd(array_merge([$average,$total],$list));die;

        $var = [
            'week'=>config('week'),
            'channel'=>Program::group('channel')->column('channel'),
            'position'=>Program::group('position')->column('position'),
            'duration'=>Program::group('duration')->order('duration')->column('duration'),
            'present_at'=>Program::group('present_at')->order('present_at')->column('present_at'),
        ];

        return $this->fetch('',$var);
    }*/

    public function complain(Request $request)
    {
        if ($request->isAjax()){
            $field = [];
            $where = [];
            $search = $request->post('select');

            if($search){
                foreach($search as $k=>$v){
                    $v = !is_array($v)?trim($v):$v;
                    if($v=='') continue;
                    $where[] = [$k,'like',"%$v%"];
                }

            }

            $spu = Spu::field($field)
                ->where($where)
                ->where('is_deleted',0)
                ->select()
                ->each(function($v){
                    $v['tt'] = $waybill = Waybill::where('status',2)->where('spu_id',$v['spu_id'])->count();
                    $v['ks'] = $complain = Complain::leftJoin('waybill','waybill.waybill_id = complain.waybill_id')
                        ->where('spu_id',$v['spu_id'])
                        ->count();
                    $v['percent'] = $complain?round($waybill/$complain,2):0;
                });

//            halt($spu);

            return __successData('success',$spu);
        }
        return $this->fetch('');
    }

    public function traffic()
    {
        $type = $this->request->param('type','date');
        $notPickup = $this->request->param('notPickup');
        $type = $type?:'date';
        $select = $this->request->param('select');

        if($this->request->isAjax()){
            if($type == 'date'){
                $rows = Traffic::efficiency(
                    !empty($select['callTime'])?$select['callTime']:''
                    ,!empty($select['on_work'])?$select['on_work']:config('traffic')['on_work']
                );
            }elseif($type == 'hour'){
                $rows = Traffic::hour_count(
                    !empty($select['callTime'])?$select['callTime']:''
                    ,$notPickup
                    ,!empty($select['hour'])?$select['hour']:''
                    ,!empty($select['on_work'])?$select['on_work']:config('traffic')['on_work']
                );
            }

//            if(count($rows)>0){
//                $rows = setTotalAndAvg($rows,'callTime');
//                $total = $avg = ['callTime'=>'合计'];
//                $avg = ['callTime'=>'平均'];
//                $rows = is_array($rows)?$rows:$rows->toArray();
//                foreach($rows as $v){
//                    foreach ($v as $k=>$val){
//                        if($k=='callTime')
//                            continue;
//                        $total[$k] = isset($total[$k])?$total[$k]+$val:$val;
//                    }
//                }
////                halt($total);
//                foreach ($total as $k=>$v){
//                    if($k=='callTime')
//                        continue;
//                    $avg[$k] = round($v/count($rows));
//                }
////                $total['start'] = $start;
////                $total['end'] = $end;
//                $rows = array_merge([$total,$avg],$rows);
//            }
//            halt($rows);
            return __successData('success',$rows);
        }

        $var = [
            'type'=>$notPickup?'notPickup':$type,
            'hour'=>!empty($select['hour'])?true:false,
        ];

        return $this->fetch('',$var);
    }

    /*public function traffic_out()
    {

    }*/
}
