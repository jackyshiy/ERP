<?php

namespace app\admin\controller;

use app\common\controller\AdminController;
use app\common\model\OrdDetail;
use app\common\model\Route;
use app\common\model\SpuRule;
use app\common\model\Waybill;
use think\Db;
use think\Request;
use app\common\model\Finance as FinanceModel;
use app\common\model\Order;
use app\common\model\Spu;

class Finance extends AdminController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index($sum=0)
    {
        $search = $this->request->post('select',[]);
        $type = $this->request->post('type');
        $where = [
            ['balance.balance_type','=',1],
//            ['status','<>',-1]
        ];
        if(session('user.auth_id')==9)
            $where[] = ['ord_route','=',1];

        $where = get_query_condition($where,$search);
//        if($search){
//            foreach($search as $k=>$v){
//                $v = !is_array($v)?trim($v):$v;
//                if($v=='') continue;
//                if($k == 'keyword'){
//                    $where[] = ['order.ord_num|spu.spu_name','like',"%$v%"];
//                    continue;
//                }
//                if($k == 'balance_at'){
//                    $where[] = ['balance_at','between time',setSearchTime($v)];
//                    continue;
//                }
//                $where[] = [$k,'in',$v];
//            }
//        }
//        halt($where);
        if($this->request->isAjax()){
            if($sum){
                $field = [
                    'sum(collection_payment) collection_payment',
                    'sum(collect_fee) collect_fee',
                    'sum(wx_pay) wx_pay',
                ];
                $sql = FinanceModel::field('order.collection_payment,balance.collect_fee,balance.wx_pay')
                    ->leftJoin('order','order.ord_id = balance.ord_id')
                    ->leftJoin('spu','find_in_set(spu.spu_id,order.spu_id)')
                    ->where($where)
                    ->group('balance.balance_id')->buildSql().' balance';
                $totalMoney = Db::table($sql)->field($field)->find();

                $var = [
                    'totalPay'=>$totalMoney['collect_fee']?:0,
                    'totalCollection'=>$totalMoney['collection_payment']?:0,
                    'wx_pay'=>$totalMoney['wx_pay']?:0,
                    'recieve'=>$totalMoney['wx_pay']+$totalMoney['collect_fee'],
                ];
                return __successData('success',$var);
            }

            $page = $this->request->post('page',1);
            $limit = $this->request->post('limit',10);

            $field = [
                'balance_id',
//                'waybill.waybill_id',
//                'waybill.waybill_num',
                'order.ord_num',
                'order.collection_payment',
                'collect_fee',
                'balance_at',
//                'ord_route',
//                'status',
                'spu_num',
                'group_concat(DISTINCT spu_name) spu_name',
                'wx_pay',
                'route_name ord_route'
            ];


            $row = FinanceModel::leftJoin('waybill','waybill.waybill_id = balance.waybill_id')
                ->leftJoin('order','order.ord_id = balance.ord_id')
                ->leftJoin('route','route.id = order.ord_route')
                ->leftJoin('spu','find_in_set(spu.spu_id,order.spu_id)')
                ->field($field)
                ->where($where)
                ->group('balance.balance_id')
                ->order('balance_at desc')
                ->paginate($limit)
                ->each(function ($v){
                    $v['normal'] = $v['collection_payment']+$v['collect_fee']==0?'否':'是';
//                    $v['ord_route'] = isset(config('ord_route')[$v['ord_route']])?config('ord_route')[$v['ord_route']]:'其余';
//                    $v['collection_payment'] = $v['status'] != -1?$v['collection_payment']:0;
                })
            ;

            return __successData('success',$row->items(),1,$row->total(),$limit,$page);
        }

        $var = [
            'route'=>Route::where('is_deleted',0)->select(),
        ];
        return $this->fetch('',$var);
    }

    public function good_finance()
    {
        $search = $this->request->post('select','');
        $type = $this->request->post('type');
        $export = $this->request->post('export');

        $where = [
            ['balance.balance_type','=',$type],
//            ['status','<>',-1]
        ];
        if(session('user.auth_id')==9)
            $where[] = ['ord_route','=',1];

        if($search){
            foreach($search as $k=>$v){
                $v = !is_array($v)?trim($v):$v;
                if($v=='') continue;
                if($k == 'keyword'){
                    $where[] = ['order.ord_num|spu.spu_name','like',"%$v%"];
                    continue;
                }
                if($k == 'balance_at'){
                    $where[] = ['balance_at','between time',setSearchTime($v)];
                    continue;
                }
                $where[] = [$k,'in',$v];
            }
        }

        $limit = $this->request->post('limit',10);

        $field = [
            'balance_id',
            'supplier_num',
            'supplier_name',
            'spu.balance_type',
            'ord_detail.spu_num',
            'spu_name',
            'route_name',
            'tax',
            'ord_detail.quantity',
            'ord_detail.price',
            'balance_json',
//            'balance_id',
//            'balance.ord_id',
//            'balance.ord_num',
//            'balance_at',
        ];


        $row = FinanceModel::leftJoin('order','order.ord_id = balance.ord_id')
            ->leftJoin('ord_detail','ord_detail.ord_id = `order`.ord_id && ord_detail.spu_id = balance.spu_id && ord_detail.sku_id = balance.sku_id ')
            ->leftJoin('route','route.id = order.ord_route')
            ->leftJoin('spu','spu.spu_id = ord_detail.spu_id')
            ->leftJoin('supplier','supplier.id = spu.supplier_id')
            ->leftJoin('products','products.product_id = ord_detail.sku_id')
            ->field($field)
            ->where($where)
            ->order('balance_at desc');

        if($this->request->isAjax()){
            $row = $row->paginate($limit)->each(function ($v){
                $v['balance_type'] = getEnum($v['balance_type'],config('balance_type'));
                $v['quantity'] = $v['balance_json']['good_quantity'];
                $v['price'] = $v['balance_json']['good_price'];
                $v['money'] = $v['price']*$v['quantity'];
            });

            return __successData('success',$row->items(),1,$row->total(),$limit,$row->currentPage());

        }else{
            if($export){
                $row = $row->hidden(['id','balance_json'])->select()->each(function ($v){
                    $v['balance_type'] = getEnum($v['balance_type'],config('balance_type'));
                    $v['quantity'] = $v['balance_json']['good_quantity'];
                    $v['price'] = $v['balance_json']['good_price'];
                    $v['money'] = $v['price']*$v['quantity'];
                })->toArray();
//                halt($row);
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename=代收货款结算明细.csv' );
                header('Cache-Control: max-age=0');

                $title = [
                    '供应商编号','供应商名称','结算方式','商品编号','商品名称','销售通路','商品单价（含税）','商品税率','本期应开票数量','本期应开票金额',
                ];
                put_csv($row,$title);
            }
        }

        return $this->fetch();
    }
    
    public function fee_index($sum=0)
    {
        $search = $this->request->post('select','');
        $where = [
            ['balance.balance_type','=',3],
        ];
        if($search){
            foreach($search as $k=>$v){
                $v = !is_array($v)?trim($v):$v;
                if($v=='') continue;
                if($k == 'keyword'){
                    $where[] = ['waybill.waybill_num|order.ord_num','like',"%$v%"];
                    continue;
                }
                if($k == 'balance_at'){
                    $where[] = ['balance.balance_at','between time',setSearchTime($v)];
                    continue;
                }
                $where[] = [$k,'in',$v];
            }

        }

        if($this->request->isAjax()){
            if($sum){
                $totalMoney = FinanceModel::field('sum(fee) fee,sum(sys_fee) sys_fee')
                    ->leftJoin('waybill','waybill.waybill_id = balance.waybill_id')
                    ->leftJoin('order','order.ord_id = waybill.ord_id')
                    ->leftJoin('route','route.id = order.ord_route')
                    ->where($where)
//                    ->fetchSql()
                    ->find();
//                dd($totalMoney);die;
                $var = [
                    'fee'=>$totalMoney['fee']?:0,
                    'sys_fee'=>$totalMoney['sys_fee']?:0,
                ];
                return __successData('success',$var);
            }

            $page = $this->request->post('page',1);
            $limit = $this->request->post('limit',10);

            $field = [
                'balance_id',
                'waybill.waybill_id',
                'waybill.waybill_num',
                'balance_weight',
                'balance_volume',
                'balance_volume_weight',
//                'trade_fee',
//                'service_fee',
//                'save_fee',
                'pay',
                'balance.balance_at',
                'waybill.trade_at trade_time',
//                'poundage',
                'spu.weight',
                'spu_length',
                'spu_width',
                'spu_height',
                'good_count',
                'reciever_addr',
                'order.collection_payment',
                'parcel_insurance',
                'waybill.status',
                'order.ord_num',
//                'package_fee',
//                'addition_fee',
//                'damages',
                'is_finish',
                'spu_num',
                'spu_name',
                'order.quantity',
                'waybill.package_num',
                'route_name ord_route',
//                'sys_trade_fee',
                'jd_trade_type',
                'fee_type',
                'fee',
                'sys_fee',
                'discount',
                'normal',
            ];

            $rows = FinanceModel::leftJoin('waybill','waybill.waybill_id = balance.waybill_id')
                ->leftJoin('order','order.ord_id = waybill.ord_id')
                ->leftJoin('route','route.id = order.ord_route')
                ->leftJoin('spu','spu.spu_id = order.spu_id')
                ->field($field)
                ->where($where)
                ->order('balance_at desc')
//                ->page($page,$limit)
                ->paginate()
                ->each(function($v) {
//                    if(!$v['is_finish'])
//                        $v = charge($v,0);

                    $v['normal'] = $v['normal']?'否':'是';
//                    $v['ord_route'] = isset(config('ord_route')[$v['ord_route']])?config('ord_route')[$v['ord_route']]:'未知';
                    $v['fee_type'] = isset(config('jd_fee_type')[$v['fee_type']])?config('jd_fee_type')[$v['fee_type']]:'未知';
                })
            ;


            return __successData('success',$rows->items(),1,$rows->total(),$limit,$page);
        }

        $var = [
            'route'=>Route::where('is_deleted',0)->select(),
        ];
        return $this->fetch('',$var);
    }

    public function fee_export(Request $request)
    {
        set_time_limit(0);

        $search = $request->param();
        $where = [
            ['balance.balance_type','=',3]
        ];
        if($search){
            foreach($search as $k=>$v){
                $v = !is_array($v)?trim($v):$v;
                if($v=='') continue;
                if($k == 'keyword'){
                    $where[] = ['balance.waybill_num','like',"%$v%"];
                    continue;
                }
                if($k == 'balance_at'){
                    $where[] = ['balance.balance_at','between time',setSearchTime($v)];
                    continue;
                }
                $where[] = [$k,'in',$v];
            }

        }

        $field = [
            'balance_id',
            'waybill.waybill_id',
            'waybill.waybill_num',
//                'balance_weight',
//                'balance_volume',
//                'balance_volume_weight',
//                'trade_fee',
//                'service_fee',
//                'save_fee',
//                'pay',
            'balance.trade_at',//结算时间
            'waybill.trade_at trade_time',//物流最新时间
//                'poundage',
//                'spu.weight',
//                'spu_length',
//                'spu_width',
//                'spu_height',
//                'good_count',
            'reciever_addr',
            'order.collection_payment',
            'parcel_insurance',
            'waybill.status',
            'order.ord_num',
//                'package_fee',
//                'addition_fee',
//                'damages',
//                'is_finish',
            'spu_num',
            'spu_name',
            'order.quantity',
            'waybill.package_num',
            'route_name ord_route',
            'fee',
            'sys_fee',
            'fee_type',
            'jd_trade_type',
            'fee_weight',
            'normal',
            'balance_weight'
        ];


//            $count = FinanceModel::leftJoin('waybill','waybill.waybill_id = balance.waybill_id')
//                ->where($where)
//                ->count();

        $row = FinanceModel::leftJoin('waybill','waybill.waybill_id = balance.waybill_id')
            ->leftJoin('order','order.ord_id = waybill.ord_id')
            ->leftJoin('route','route.id = order.ord_route')
            ->leftJoin('spu','spu.spu_id = order.spu_id')
            ->field($field)
            ->where($where)
            ->whereNotNull('balance.waybill_id')
            ->order('balance_at desc')
            ->select()
            ->each(function($v) {
//                    if(!$v['is_finish'])
//                        $v = charge($v);
                $v['fee_type'] = isset(config('jd_fee_type')[$v['fee_type']])?config('jd_fee_type')[$v['fee_type']]:'未知';
                $v['normal'] = $v['normal']?'否':'是';
            })
            ->toArray()
        ;

        $csv = [];
        foreach($row as $v){
            $arr['waybill_num'] = $v['waybill_num'];
            $arr['ord_num'] = $v['ord_num'];
            $arr['ord_route'] = $v['ord_route'];
            $arr['spu_num'] = $v['spu_num'];
            $arr['spu_name'] = $v['spu_name'];
            $arr['quantity'] = $v['quantity'];
            $arr['package_num'] = $v['package_num'];
            $arr['normal'] = $v['normal'];
            $arr['balance_weight'] = $v['balance_weight'];
            $arr['fee_weight'] = $v['fee_weight'];
//            $arr['trade_fee'] = $v['trade_fee'];
//            $arr['sys_trade_fee'] = $v['sys_trade_fee'];
            $arr['fee_type'] = $v['fee_type'];
            $arr['fee'] = $v['fee'];
            $arr['sys_fee'] = $v['sys_fee'];
//            $arr['package_fee'] = $v['package_fee'];
//            $arr['addition_fee'] = $v['addition_fee'];
//            $arr['damages'] = $v['damages'];
//            $arr['pay'] = $v['pay'];
            $arr['trade_at'] = $v['trade_at'];
            $csv[] = $arr;
        }

//        halt($csv);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=运费结算明细.csv' );
        header('Cache-Control: max-age=0');

        $title = [
            '运单号','订单号','通路','商品编码','商品名','商品数量','包裹数','异常','京东计费重量(KG)','系统计费重量(KG)','费用类型','京东费用',
            '系统费用','结算时间',
        ];
        put_csv($csv,$title);
    }

    /**
     * 导入结算表
     * @param Request $request
     * @return \app\common\service\CurlService|mixed|\think\response\Json
     */
    public function balance(Request $request)
    {
        if ($request->isAjax()){
            $row = $request->post('row')?:2;
            $file = $request->post('file');
             $module = $request->post('module');

            if(!$file && !is_file($file))
                return __error('需要导入的文件不存在，请尝试重新上传文件');

            if(!$module)
                return __error('请选择结算类型');

            $data = excelImport($file,config('excel')[$module]['import'],1,$row);
            if($data['code'])
                return $data;
            $info = ['total'=>$data['data']['total'],'row'=>$row,'percent'=>$data['data']['total']==0?0:bcmul($row/$data['data']['total'],100)];
            if($data['data']['total']<2)
                return __error("没有导入信息",$info);
//            dd($data['data']);die;
//            echo $data['data']['ord_date'].' '.$data['data']['ord_time'];
            $arr = $data['data']['data'];
//            dump($arr);
//            $validate = $this->validate($arr,'app\admin\validate\Finance.'.$module);
//            if (true !== $validate) return __error($validate,$info);

            $arr[$module] = 1;
            $arr['balance_type'] = array_search($module,config('finance_type'))?:0;
            if($module=='trade'){
                if(!$arr['waybill_num'])
                    return __error('运单号为空',$info);

                $key = array_search($arr['fee_type'],config('jd_fee_type'))?:'';
                if(!$key)
                    return __error('找不到对应的运费类型',$info);

                $arr['fee_type'] = $key;

                if(!empty($arr['back_waybill_num']))
                    $arr['waybill_num'] = $arr['back_waybill_num'];

                $msg = "$arr[waybill_num] 导入成功";
                $arr['balance_json'] = [
                    'balance_weight'=>$arr['balance_weight'],
                    'fee'=>$arr['fee'],
                    'balance_fee'=>$arr['balance_fee'],
                    'jd_trade_type'=>$arr['jd_trade_type'],
                ];
                $field = [
                    'waybill.waybill_id',
                    'waybill.deliver_id',
                    'waybill.waybill_num',
                    'spu.weight',
                    'spu_length',
                    'spu_width',
                    'spu_height',
                    'good_count',
                    'reciever_addr',
                    'order.collection_payment',
                    'parcel_insurance',
                    'status',
                    'trade_at trade_time',
                    'quantity',
                    'ord_route',
                    'trade_fee',
                    'trade_poundage',
                    'sum(ord_money) ord_money',
                ];
                $finance = Waybill::field($field)
                    ->leftJoin('order','order.ord_id = waybill.ord_id')
                    ->leftJoin('spu','spu.spu_id = order.spu_id')
                    ->where(['waybill_num'=>$arr['waybill_num']])->find();

//            if($arr['waybill_num']){
//                $finance = $finance->where(['waybill_num'=>$arr['waybill_num']])->find();
//            }elseif($arr['ord_num']){
//                $finance = $finance->where(['order.ord_num'=>$arr['ord_num']])->find();
//                if($finance)
//                    $msg = $arr['ord_num'].$msg;
//            }

                if(!array_filter($finance->toArray()))
                    return __error('找不到对应系统信息，请检查运单号',$info);

                Waybill::startTrans();
                Waybill::where(['waybill_id'=>$finance['waybill_id']])->update(['is_'.$module=>1]);

                $arr['waybill_id'] = $finance->waybill_id;

                $arr = tradeCharge(array_merge($finance->toArray(),$arr));
                if(!empty($arr['back_waybill_num']))
                    $arr['fee_type'] = 'back_trade_fee';

                $exist = FinanceModel::where([$module=>1])
                    ->where('waybill_id',$finance['waybill_id'])
                    ->where('fee_type',$arr['fee_type'])
                    ->where('balance_at',strtotime($arr['balance_at']))
                    ->find();
            }elseif($module=='finance'){
                if(!$arr['ord_num'])
                    return __error('订单号为空',$info);
                $msg = "$arr[ord_num] 导入成功";
                $finance = Order::whereLike('ord_num',"%$arr[ord_num]%")->find();
                if(!$finance)
                    return __error('找不到对应系统信息，请检查订单号',$info);

                Order::update(['is_'.$module=>1],['ord_id'=>$finance->ord_id]);
                $arr['collect_fee'] = abs($arr['collect_fee']);

                if(in_array($finance['ord_route'],config('wx_pay_route'))){
                    $arr['wx_pay'] = $arr['collect_fee'];
                    $arr['collect_fee'] = 0;
                }
                if($arr['collect_fee'] != $finance['collection_payment'] || (isset($arr['wx_pay']) && $arr['wx_pay'] != $finance['ord_money'])){
                    $msg .= '，<span style="color:red">金额异常</span>';
                }
                $arr['ord_id'] = $finance->ord_id;
                $exist = FinanceModel::where([$module=>1])
                    ->where('ord_id',$finance['ord_id'])
                    ->where('balance_at',strtotime($arr['balance_at']))
                    ->find();
            }elseif ($module == 'finance_good'){
                $msg = "$arr[ord_num] 导入成功";
                $arr['balance_json'] = [
                    'good_price'=>$arr['good_price'],
                    'good_quantity'=>$arr['good_quantity'],
                    'balance_money'=>$arr['balance_money'],
                ];
                $order = Order::whereLike('ord_num',"%$arr[ord_num]%")->find();
                if(!$order)
                    return __error('找不到对应订单号',$info);

                $spu = Spu::where(['spu_num'=>$arr['spu_num'],'is_deleted'=>0])->find();
                if(!$spu){
                    $rule = SpuRule::where('spu_num',$arr['spu_num'])->find();
                    $spu = Spu::find($rule['spu_id']);
                }
                if(!$spu)
                    return __error('找不到商品记录',$info);

                $order_detail = OrdDetail::where(['ord_id'=>$order->ord_id,'spu_num'=>$spu['spu_num']])->find();
                if(!$order_detail)
                    return __error('找不到对应订单明细',$info);

                if($arr['good_quantity']!=$order_detail->quantity)
                    $msg .= '，<span style="color:red">商品数量异常</span>';
                if($arr['balance_money']!=$order_detail->quantity*$order_detail->price)
                    $msg .= '，<span style="color:red">商品总价异常</span>';

                $arr['ord_id'] = $order->ord_id;
                $arr['sku_id'] = $order_detail->sku_id;
                $arr['spu_id'] = $order_detail->spu_id;
                $exist = FinanceModel::where('balance_type',2)
                    ->where('ord_id',$order['ord_id'])
                    ->where('balance_at',strtotime($arr['balance_at']))
                    ->find();
            }else{
                return __error('未知结算类型');
            }

//            halt($arr);
            if(!empty($exist)){
                FinanceModel::update(save_common_field($arr,1),['balance_id'=>$exist['balance_id']]);
            }else{
                FinanceModel::create(save_common_field($arr));
            }
            Waybill::commit();
            return __successData($msg,$info);
        }
        return $this->fetch();
    }



    public function export(Request $request)
    {
        set_time_limit(0);

        $search = $request->param();

        $where = [
//            ['finance','=',1],
            ['balance.balance_type','=',1]
        ];

        if(session('user.auth_id')==9)
            $where[] = ['ord_route','=',1];

        if($search){
            foreach($search as $k=>$v){
                if($v=='') continue;
                if($k == 'keyword'){
                    $where[] = ['waybill.waybill_num|order.ord_num|spu.spu_name','like',"%$v%"];
                    continue;
                }
                if($k == 'balance_at'){
                    $where[] = ['balance_at','between time',setSearchTime($v)];
                    continue;
                }
                $where[] = [$k,'in',$v];
            }

        }

        $field = [
            'balance_id',
            'waybill.waybill_id',
            'waybill.waybill_num',
            'order.ord_num',
            'route_name ord_route',
            'ord_at',
            'balance_at',
            'collect_fee',
            'spu_num',
            'group_concat(DISTINCT spu_name) spu_name',
            'order.collection_payment',
            'wx_pay',
        ];

        $row = FinanceModel::leftJoin('order','order.ord_id = balance.ord_id')
            ->leftJoin('route','route.id = order.ord_route')
            ->leftJoin('waybill','waybill.waybill_id = balance.waybill_id')
            ->leftJoin('spu','find_in_set(spu.spu_id,order.spu_id)')
            ->field($field)
            ->where($where)
            ->group('balance.balance_id')
            ->order('balance_at desc')
            ->select()
            ->each(function($v) {
//                $v = charge($v);
//                $v['ord_route'] = isset(config('ord_route')[$v['ord_route']])?config('ord_route')[$v['ord_route']]:'未知';
            })
            ->toArray()
        ;

        $csv = [];
        foreach($row as $v){
            $csv[] = [
                'ord_num'=>"$v[ord_num]",
                'waybill_num'=>$v['waybill_num'],
                'ord_route'=>$v['ord_route'],
                'spu_num'=>$v['spu_num'],
                'spu_name'=>$v['spu_name'],
                'collection_payment'=>$v['collection_payment'],
                'collect_fee'=>$v['collect_fee'],
                'wx_pay'=>$v['wx_pay'],
                'ord_at'=>$v['ord_at']?date('Y-m-d',$v['ord_at']):'',
                'balance_at'=>$v['balance_at'],
            ];
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=代收货款结算明细.csv' );
        header('Cache-Control: max-age=0');

        $title = [
            '订单号','运单号','通路','商品编码','商品名','系统代收金额','京东代收金额','微信支付金额','订单时间','结算时间',
        ];
        put_csv($csv,$title);
    }

    public function goods()
    {
        $export = $this->request->param('export');
        $limit = $this->request->param('limit',config('limit'));
        $search = $this->request->param('select',[]);

        $where = [
//                ['balance.balance_type','=',1],
        ];
        $rows = OrdDetail::goods_balance(get_query_condition($where,$search),$limit)
            ->group('detail_id');

        if($this->request->isAjax()){
            $rows = $rows->paginate($limit)->each(function($v){
                    $v['money'] = $v['price']*$v['quantity'];
                    $v['balance_type'] = getEnum($v['balance_type'],config('balance_type'));
                });

            return __successData('sussess',$rows->items(),1,$rows->total(),$limit,$rows->currentPage());
        }

        if($export){
            $row = $rows->select()->each(function ($v){
                $v['money'] = $v['price']*$v['quantity'];
                $v['balance_type'] = getEnum($v['balance_type'],config('balance_type'));
            })->toArray();

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=商品纬度系统流水.csv' );
            header('Cache-Control: max-age=0');

            $title = [
                '供应商编号','供应商名称','结算方式','商品编号','商品名称','销售通路','商品单价（含税）','商品税率','本期应开票数量','本期应开票金额',
            ];
            put_csv($row,$title);
        }

        $var = [
            'route'=>Route::where('is_deleted',0)->select(),
        ];

        return $this->fetch('',$var);
    }

    public function good_balance()
    {

        if($this->request->isAjax()){
            $search = $this->request->param('select',[]);
            $where = [
//                ['spu.on_sale','=',1],
                ['balance.balance_type','=',1]
            ];
            $rows = OrdDetail::goods_balance_gather(get_query_condition($where,$search));

            return __successData('sussess',$rows);
        }

        $var = [
            'route'=>Route::where('is_deleted',0)->select(),
        ];

        return $this->fetch('',$var);
    }
}
