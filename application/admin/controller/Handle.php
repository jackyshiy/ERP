<?php

namespace app\admin\controller;

use app\common\controller\AdminController;
use think\Controller;
use think\Request;

class Handle extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->task = new Task();
    }
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function YzOrder()
    {
        if($this->request->isAjax()){
            $date = $this->request->post('date');
            $tid = $this->request->post('tid');
            $time = $date?explode(' - ',$date):[];
            $param = [
                'status'=>'WAIT_SELLER_SEND_GOODS',//'TRADE_SUCCESS',
                'page_size'=>100,
                'page_no'=>1,
                'tid'=>$tid,
            ];
            if($time){
                $param['start_created'] = $time[0];
                $param['end_created'] = $time[1];
            }
//            halt($param);
            $r = $this->task->YZ_order($param);
            return $r;
        }

        return $this->fetch('');
    }


}
