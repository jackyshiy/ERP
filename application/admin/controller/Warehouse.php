<?php

namespace app\admin\controller;

use app\common\controller\AdminController;
use app\common\model\Order;
use app\common\model\OrgModel;
use app\common\model\WxpayLog;
use think\Controller;
use think\facade\Cache;
use think\Request;
use app\common\model\Warehouse as Wh;
use app\common\service\YouZanService;
use app\common\model\Waybill;

class Warehouse extends AdminController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        if($this->request->isAjax()){
            $page = $this->request->post('page',1);
            $limit = $this->request->post('limit',10);
            $search = $this->request->post('select','');
            $where = [
                ['is_deleted','=',0]
            ];
            $field = [
                'wh_id id',
                'wh_name',
                'wh_address',
                'jd_ware_house_code',
                'create_at'
            ];
            if($search){
                foreach($search as $k=>$v){
                    $v = !is_array($v)?trim($v):$v;
                    if($v=='') continue;
                    if($k == 'keyword'){
                        $where[] = ['wh_name','like',"%$v%"];
                        continue;
                    }
                    if($k == 'create_at'){
                        $where[] = [$k,'between time',setSearchTime($v)];
                        continue;
                    }
                    $where[] = [$k,'in',$v];
                }

            }
//            dd($where);
            $order = Wh::field($field)
                ->where($where)
//                ->append(['deliver.org_name'])
//                ->page($page,$limit)
                ->paginate()

            ;
//            halt($order);
            return __successData('获取成功',$order->items(),1,$order->total(),$limit,$page);
        }

        return $this->fetch('');
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $var= [
            'org'=>OrgModel::all(),
        ];

        return $this->fetch('form',$var);
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $post = $request->post();

        $validate = $this->validate($post, 'app\admin\validate\Warehouse.save');
        if (true !== $validate) return __error($validate);

        if(!empty($post['wh_id'])){
            $r = Wh::update(save_common_field($post));
        }else{
            $r = Wh::create(save_common_field($post));
        }

        return $r?__success('操作成功'):__error('操作失败');
    }


    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $wh = Wh::find($id);

        $var = [
            'wh'=>$wh,
            'org'=>OrgModel::all(),
        ];

        return $this->fetch('form',$var);
    }


    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete()
    {
        $id = $this->request->param('id');
        $r = Wh::deleteRow([['wh_id','in',$id]]);

        return $r;
    }

    //分拣
    public function pack()
    {
        set_time_limit(0);
        $id = $this->request->post('id');
        $id = is_array($id)?$id:explode(',',$id);
        if(!$id)
            return __error('请选择运单');

        $where = [
//            ['ord_type','in',[1,2]],
            ['waybill.waybill_id','in',$id],
            ['waybill.out_instruction','not in',[-1,2]]
        ];

        $waybill = Waybill::hasWhere('ord','ord_type in (1,2)')->where($where)->hidden(['create_at','update_at'])->select()->toArray();
//        halt($waybill);
        if(count($waybill)-count($id)!=0)
            return __error('存在不是待出库状态的销售单、换货单，请检查');

        $error = 0;
        $r = [];
        $youzan = new YouZanService(config('youzan.fs_tv'));
        foreach ($waybill as $v){
            if(!$v['deliver_id']){
                $error++;
                $r['msg'] = '找不到发货车队';
                continue;
            }

            $order = Order::find($v['ord_id']);
            if(!$order){
                $error++;
                $r['msg'] = '订单信息不存在';
                continue;
            }

            if($order->pay_mode==2 && !WxpayLog::where('ord_id',$order->ord_id)->find()){
                $error++;
                $r['msg'] = '订单未支付';
                continue;
            }

            if($order['ord_route']==6){
                $yz_order = $youzan->order(['tid'=>$order['ord_num']]);
                if(isset($yz_order['full_order_info_list']) && count($yz_order['full_order_info_list'])>0){
                    $is_refund = !empty($yz_order['full_order_info_list'][0]['full_order_info']['order_info']['order_tags']['is_refund'])
                                ?$yz_order['full_order_info_list'][0]['full_order_info']['order_info']['order_tags']['is_refund']
                                :0;
                    $is_feedback = $is_refund;
                    if($is_feedback){
                        $error++;
                        $r['msg'] = '订单有退款';
                        continue;
                    }
                }
            }
            $r = bookCharge($v);
            if(!$r['code']){
                Waybill::update(['out_instruction'=>1],['waybill_id'=>$v['waybill_id']]);
            }else{
                $error++;
            }

        }
        return !$error?__success('操作成功'):(count($id)==1?__error($r['msg']):__error('存在操作失败的运单，请逐单尝试'));
    }

    //出库
    public function out()
    {
        $id = $this->request->post('id');
        if(!$id)
            return __error('请选择订单');

        $where = [
            ['waybill_id','in',$id],
        ];

        $count = Waybill::where($where)->where('out_instruction',1)->count();

        if($count-count($id)!=0)
            return __error('存在不是分拣完成状态的订单，请检查');

        Waybill::where($where)->update(['out_instruction'=>2]);
        return __success('操作成功');
    }

    public function checkout()
    {
        if($this->request->isAjax()){
            $waybill_num = $this->request->param('waybill_num');
            $checkout = Cache::get('waybill_checkout')?:[];
            if($waybill_num){
                $check = [
                    'waybill_num'=>$waybill_num,
                    'checkout_at'=>date('Y-m-d H:i:s'),
                ];
                $waybill = Waybill::where('waybill_num',$waybill_num)->find();
                if(!$waybill){
                    $check['status'] = '<span style="color: red;">运单不存在</span>';
                    array_unshift($checkout,$check);
                    Cache::set('waybill_checkout',$checkout,strtotime(date('Y-m-d 24:00:00'))-time());
                    return __success('success');
                }

                if($waybill->out_instruction==2)
                    return __success('success');

                $waybill->out_instruction = 2;
                $waybill->checkout_at = $check['checkout_at'];
                $r = $waybill->save();
                if($r){
                    $check['status'] = '<span style="color: green;">出库完成</span>';
                    array_unshift($checkout,$check);
                    Cache::set('waybill_checkout',$checkout,strtotime(date('Y-m-d 24:00:00'))-time());
                }
            }else{
                return __success('success',$checkout);
            }
        }
        return $this->fetch();
    }
}
