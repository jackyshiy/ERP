<?php

namespace app\admin\controller;

use app\common\controller\AdminController;
use think\Controller;
use think\Request;
use app\common\model\User;
use app\common\model\DeliverSpeciel;

class Route extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->model = new \app\common\model\Route();
    }

    protected function __beforeDelete($id)
    {
        $deliver_rule = DeliverSpeciel::whereIn('route_id',$id)->where('is_deleted',0)->find();
        if($deliver_rule)
            is_array($id)&&count($id)?throw_error('存在已定义车队分配的通路，请检查'):throw_error('该通路存在车队分配定义，请先删除');
    }

    protected function __each($rows)
    {
        return $rows->each(function ($v){
            if($v->pay_mode){
                $pay_mode = [];
                foreach ($v->pay_mode as $vv){
                    $pay_mode[] = getEnum($vv,'pay_mode');
                }
                $v->pay_mode_text = implode(',',$pay_mode);
            }
        });
    }

    public function pay_mode($id)
    {
        $pay_mode = $this->model->where('id',$id)->value('pay_mode');
        if($pay_mode){
            $pay_mode = explode(',',$pay_mode);
            foreach ($pay_mode as &$v){
                $v = [
                    'id'=>$v,
                    'name'=>getEnum($v,'pay_mode'),
                ];
            }
            return __successData('success',$pay_mode);
        }
        return __error('nothing');
    }
}
