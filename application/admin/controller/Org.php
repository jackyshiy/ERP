<?php

namespace app\admin\controller;

use app\common\controller\AdminController;
use app\common\model\OrgModel;
use app\common\model\SystemAddress;
use think\Controller;
use think\Request;
use think\Validate;
use app\common\model\DeliverSpeciel;

class Org extends AdminController
{
    protected $model = '';

    public function __construct()
    {
        parent::__construct();
        $this->model = new OrgModel();
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        if($this->request->isAjax()){
            $page = $this->request->param('page',1);
            $limit = $this->request->param('limit',config('limit'));
            $select = $this->request->param('select');

            $field = [
                'org.org_id',
                'org_name',
                'org_master',
                'org_phone',
                'org.create_at',
                'username'
            ];

            $where = [

            ];

            if($select){
                foreach ($select as $k=>$v){
                    if($v=='')
                        continue;
                    if($k=='keyword'){
                        $where[] = ['org_name|org_master|org_phone','like',"%$v%"];
                    }
                }
            }

            $rows = OrgModel::baseModelSource($field,$where)
                ->leftJoin('system_user','system_user.id = org.create_by')
                ->order('org_id desc')
                ->paginate()
                ;

            return __successData('success',$rows->items(),1,$rows->total(),$limit,$page);
        }
        return $this->fetch();
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $province_id = SystemAddress::where('name','广东省')->value('id');
        $var = [
            'city'=>SystemAddress::where('pid',$province_id)->select(),
        ];
        return $this->fetch('form',$var);
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $post = $request->post();
        $validate = $this->validate($post, 'app\admin\validate\Org.save');
        if (true !== $validate) return __error($validate);

        if(!empty($post['org_id'])){
            $msg = '修改';
//            foreach(explode(',',$post['route_id']) as $v){
//                $exist = $this->model->where("find_in_set($v,route_id)")->where('org_id','<>',$post['route_id'])->find();
//                if($exist)
//                    return __error('存在');
//            }
            $r = $this->model->update(save_common_field($post,1));
        }else{
            $msg = '添加';
            $r = $this->model->create(save_common_field($post));
        }
        return $r?__success($msg.'成功'):__error($msg.'失败');

    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $province_id = SystemAddress::where('name','广东省')->value('id');
        $var = [
            'org'=>OrgModel::find($id),
            'city'=>SystemAddress::where('pid',$province_id)->select(),
        ];

        return $this->fetch('form',$var);
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function del($id)
    {
        $r = OrgModel::deleteRow([['org_id','in',$id]]);

        return $r;
    }

    public function token()
    {
        $param = $this->request->get();
//        dump($param);
        $rule = [
            'app_key'=>'require',
            'app_secret'=>'require',
            'customer_code'=>'require',
        ];
        $msg = [
            'app_key.require'=>'请填写appKey',
            'app_secret.require'=>'请填写appSecret',
            'customer_code.require'=>'请填写客户编码',
        ];
        $validate = Validate::make($rule)->message($msg);
        if(!$validate->check($param))
            return __error($validate->getError());
        $res = jd_token($param);
        return $res?__successData('获取成功',['token'=>$res]):__error('获取失败');
    }

    public function refresh_token($id)
    {
        $field = [
            'jd_app_key app_key',
            'jd_app_secret app_secret',
            'jd_token token',
            'jd_customer_code customer_code',
            'create_at'
        ];
        $org = OrgModel::field($field)->find($id)->toArray();
//        if((time() - strtotime($org['create_at']))<=30931200)
//            return __error('token还没有过期');
        $res = jd_refresh_token($org);
        return $res?__success('刷新成功'):__error('刷新失败');
    }

    public function test()
    {
        $id = 3;
        $org = org($id);
        $res = JDcheck($org['row'],$org['secret']);
        halt($res);
    }
}
