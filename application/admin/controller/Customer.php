<?php

namespace app\admin\controller;

use app\common\model\MemberAddress;
use think\Controller;
use think\Request;
use app\common\model\Member;

class Customer extends Controller
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        if($this->request->isAjax()){
            $page = $this->request->post('page',1);
            $limit = $this->request->post('limit',10);
            $search = $this->request->post('select','');
            $where = [
//                ['products.is_deleted','=',0]
            ];
            $field = [
                '*',
                'member_id id',
            ];
            if($search){
                foreach($search as $k=>$v){
                    $v = !is_array($v)?trim($v):$v;
                    if($v=='') continue;
                    if($k == 'keyword'){
                        $where[] = ['member_id|member_phone|member_tel','like',"%$v%"];
                        continue;
                    }
                    if($k == 'create_at'){
                        $where[] = [$k,'between time',setSearchTime($v)];
                        continue;
                    }
                    $where[] = [$k,'in',$v];
                }

            }
//            dd($where);

            $rows = Member::field($field)
                ->where($where)
                ->paginate()
                ;
//            dd($order);
            return __successData('获取成功',$rows->items(),1,$rows->total(),$limit,$page);
        }

        return $this->fetch('');
    }

    public function address(Request $request,$id)
    {
        if($request->isAjax()){
            $page = $request->post('page',1);
            $limit = $request->post('limit',10);
            $search = $request->post('select','');
            $where = [
                ['member_id','=',$id]
            ];
            $field = [
                '*',
                'address_id id',
            ];
            if($search){
                foreach($search as $k=>$v){
                    $v = !is_array($v)?trim($v):$v;
                    if($v=='') continue;
                    if($k == 'keyword'){
                        $where[] = ['member_id|member_phone|member_tel','like',"%$v%"];
                        continue;
                    }
                    if($k == 'create_at'){
                        $where[] = [$k,'between time',setSearchTime($v)];
                        continue;
                    }
                    $where[] = [$k,'in',$v];
                }

            }

            $count = MemberAddress::where($where)->count();

            $rows = MemberAddress::field($field)
                ->where($where)
                ->page($page,$limit)
                ->select()
                ->each(function($v){
                    $v['province'] = getAddrName($v['province']);
                    $v['city'] = getAddrName($v['city']);
                    $v['county'] = getAddrName($v['county']);
                    $v['street'] = getAddrName($v['street']);
                })
            ;
            return __successData('获取成功',$rows,1,$count,$limit,$page);
        }

        return $this->fetch('');
    }
}
