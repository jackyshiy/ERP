<?php

namespace app\admin\controller;

use app\common\controller\AdminController;
use app\common\model\OrgModel;
use app\common\model\SpuRule;
use app\common\model\SystemLog;
use think\Request;
use app\common\model\Products as ProductsModel;
use app\common\model\Spu;
use Overtrue\Pinyin\Pinyin;
use app\common\model\Warehouse;
use app\common\model\Order;
use app\common\model\Route;
use app\common\model\Supplier;

class Products extends AdminController
{
    protected $Spu = null
                ,$PinYin = null
                ,$Sku = null;



    /**
     * 初始化
     * node constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->Spu = new Spu();
        $this->Sku = new ProductsModel();
        $this->PinYin = new Pinyin();
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        if($this->request->isAjax()){
            $page = $this->request->post('page',1);
            $limit = $this->request->post('limit',10);
            $search = $this->request->post('select','');
//            $count = $this->model->where([])->count();
            $field = [
                'spu.*',
                'spu_id id',
                'GROUP_CONCAT(wh_name) wh_name',
            ];

            $where = [
                ['spu.is_deleted','=',0]
            ];

            if($search){
                foreach($search as $k=>$v){
                    $v = !is_array($v)?trim($v):$v;
                    if($v=='') continue;
                    if($k == 'keyword'){
                        $where[] = ['spu.spu_num|spu.spu_name','like',"%$v%"];
                    }else{
                        $where[] = [$k,'in',$v];
                    }
                }

            }
            $rows = Spu::withJoin('supplier')->leftJoin('warehouse','warehouse.wh_id = spu.wh_id')
                ->field($field)->where($where)->group('spu_id')->paginate($limit);
//            $a = OrderModel::select();
//            dd($a);die;
            return __successData('获取成功',$rows->items(),1,$rows->total(),$limit,$page);
        }

        return $this->fetch();

    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {

        $var = [
            'wh'=>Warehouse::where('is_deleted',0)->select(),
            'supplier'=>\app\common\model\Supplier::where('is_deleted',0)->select(),
            'route'=>\app\common\model\Route::where('is_deleted',0)->select(),
            'deliver'=>OrgModel::where('is_deleted',0)->select(),
        ];

        return $this->fetch('form',$var);
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        if($request->isPost()){
//            halt($request->post());
            //spu处理
            $spu = $request->post('spu');
            $validate = $this->validate($spu,'app\admin\validate\Products.save');
            if (true !== $validate) return __error($validate);

            $rule = $request->post('rule');
            if(!$rule)
                return __error('请至少填写一条车队规则');
            foreach ($rule as &$value){
                $value = array_filter($value)?:'';
            }
            if(empty(array_filter($rule)))
                return __error('请至少填写一条车队规则');

            Spu::startTrans();
            $spu['on_sale'] = !empty($spu['on_sale'])?$spu['on_sale']:0;
            $spu['spu_param'] = !empty($spu['spu_param'])?$spu['spu_param']:'';
//            $spu['spu_param'] = arr_filter($spu['spu_param']);
//            halt($spu);
            if(!empty($spu['spu_id'])){
                $spu_id = $spu['spu_id'];
                $exist = Spu::where('spu_num',$spu['spu_num'])->where('spu_id','<>',$spu_id)->count();
                if($exist){
                    Spu::rollback();
                    return __error('商品编码已存在');
                }
                $spu['update_by'] = session('user.id');
                $r_spu = Spu::update(save_common_field($spu,1));
            }else{
                $exist = Spu::where('spu_num',$spu['spu_num'])->count();
                if($exist){
                    Spu::rollback();
                    return __error('商品编码已存在');
                }
                $spu['create_by'] = $spu['update_by'] = session('user.id');

                $r_spu = Spu::create(save_common_field($spu));
//                halt($r_spu);
                $spu_id = $r_spu->spu_id;
            }

            //sku处理
            ProductsModel::where('spu_id',$spu_id)->update(['is_deleted'=>1]);
            if(!empty($spu['spu_param'])&&$request->post('sku')){
                $sku = $request->post('sku');
//                halt($sku);
                foreach ($sku as $k=>$v){
                    $v['spu_id'] = $spu_id;
                    $arr = [];
                    $where = [];
                    if(isset($v['sku_param'])&&$v['sku_param'])
                    foreach ($v['sku_param'] as $val){
                        $key = toPinYin($val['name'])?:$val['name'];
                        $arr[$key] = $val['value'];
                        $where[] = ['sku_param->'.$key,'=',$val['value']];
                    }
//                    halt($where);
                    $v['sku_param'] = $arr;
//                    halt($v['sku_param']);
//                    if($v['stock']==0)
//                        $v['on_sale'] = 0;
                    $exist = ProductsModel::where('spu_id',$spu_id)->where($where)->find();
//                    halt($exist);
//                    halt($exist);
                    if($exist){
                        $exist->price = $v['price'];
                        $exist->org_price = $v['org_price'];
                        $exist->stock = $v['stock'];
                        $exist->is_deleted = 0;
//                        $exist->on_sale = !empty($v['on_sale'])?$v['on_sale']:0;
                        $exist->save();
//                        stock_log([
//                            'sku_id'=>$exist->product_id,
//                            'stock'=>$v['stock'],
//                            'detail'=>"商品 $spu[spu_name] ，更新 ，良品库存 $v[stock] ，不良品库存0"
//                        ]);
                    }else{
                        $r = ProductsModel::create($v);
                        if(!$r){
                            Spu::rollback();
                        }
                        stock_log([
                            'sku_id'=>$r->product_id,
                            'stock'=>$v['stock'],
                            'detail'=>"商品 $spu[spu_name] ，首次添加 ，良品库存 $v[stock] ，不良品库存0"
                        ]);
                    }
//                    $sku[$k]['sku_param'] = $arr;
                }
            }else{
                $spu['spu_id'] = $spu_id;
                $sku = ProductsModel::where('spu_id',$spu_id)->whereNull('sku_param')->find();
                if($sku){
                    $sku->is_deleted = 0;
                    $sku->save();
                    stock_log([
                        'sku_id'=>$spu['product_id'],
                        'stock'=>$spu['stock'],
                        'detail'=>"商品 $spu[spu_name] ，更新 ，良品库存 $spu[stock] ，不良品库存0"
                    ]);
                }else{
                    $r = ProductsModel::create($spu);
                    if(!$r){
                        Spu::rollback();
//                        halt($r);
                    }
                    stock_log([
                        'sku_id'=>$r->product_id,
                        'stock'=>$spu['stock'],
                        'detail'=>"商品 $spu[spu_name] ，首次添加 ，良品库存 $spu[stock] ，不良品库存0"
                    ]);
                }
            }

            //车队规则处理
            foreach($rule as $vv){
                $validate = $this->validate($vv,'app\common\validate\SpuRule.save');
                if (true !== $validate) return __error($validate);

                $vv['spu_id'] = $spu_id;
                $vv['wh_name'] = Warehouse::where('wh_id',$vv['wh_id'])->value('wh_name');
                $vv['route_name'] = Route::where('id',$vv['route_id'])->value('route_name');
                $vv['deliver_name'] = OrgModel::where('org_id',$vv['deliver_id'])->value('org_name');
                if(!empty($vv['id'])){
                    SpuRule::update($vv);
                }else{
                    $exist = SpuRule::where($vv)->find();
                    if($exist)
                        return __error('存在冲突的规则，请检查');
                    SpuRule::create($vv);
                }

            }

            Spu::commit();

            return __success('操作成功');
        }
    }


    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
//        halt(password(123456));
        $spu = Spu::find($id)->toArray();
        $sku_where = [
            ['spu_id','=',$spu['spu_id']],
        ];
        if($spu['spu_param'])
            $sku_where[] = ['sku_param','is not null'];
        $sku = ProductsModel::where(['spu_id'=>$spu['spu_id'],'is_deleted'=>0])->select()->toArray();
        if(count($sku)==1){
            $spu['sku_id'] = $sku[0]['product_id'];
            $spu['org_price'] = $sku[0]['org_price'];
            $spu['price'] = $sku[0]['price'];
            $spu['stock'] = $sku[0]['stock'];
        }


        $var = [
            'spu'=>$spu,
            'sku'=>$sku,
            'rule'=>SpuRule::where('spu_id',$id)->select(),
            'wh'=>Warehouse::where('is_deleted',0)->select(),
            'wh_rule'=>Warehouse::where('is_deleted',0)->whereIn('wh_id',$spu['wh_id'])->select(),
            'supplier'=>\app\common\model\Supplier::where('is_deleted',0)->select(),
            'route'=>\app\common\model\Route::where('is_deleted',0)->select(),
            'deliver'=>OrgModel::where('is_deleted',0)->select(),
        ];

        return $this->fetch('form',$var);
    }


    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function del($id)
    {
//        dd($id);
        return Spu::deleteRow(['spu_id'=>$id],del_common_field(session('user.id')));
    }

    /**
     * 商品上下架
     *
     * @param $id
     * @return \think\response\Json
     */
    public function on_sale($id)
    {
        $validate = $this->validate(['id'=>$id],'app\admin\validate\Products.on_sale');
        if (true !== $validate) return __error($validate);

        $product = Spu::find($id);

        if($product['on_sale']){
            $msg = '商品下架成功';
            $on_sale = 0;
        }else{
//            if($product['stock']==0)
//                return __error('库存为零，不能上架');
            $msg = '商品上架成功';
            $on_sale = 1;
        }
        $product->on_sale = $on_sale;
        $update = $product->save();
        if ($update) {
            return __success($msg);
        } else {
            return __error('数据有误，请刷新重试！');
        }
    }

    /**
     * 商品下拉列表
     *
     * @param Request $request
     * @return \think\response\Json
     */
    public function get_product(Request $request)
    {
        if($request->isAjax()){
            $spu_id = $request->post('spu_id','');

            $field = [
                'sku_param',
                'product_id',
            ];
            $products = ProductsModel::field($field)
                ->where(['is_deleted'=>0,'spu_id'=>$spu_id,'on_sale'=>1])
                ->select()->each(function($v){
                    $v->sku_param = $v->sku_param?implode('|',$v->sku_param):[];
                });

            return __successData('获取成功',$products);
        }
    }

    /**
     * 库存预警
     *
     * @return \think\response\Json
     */
    public function stock_warning()
    {
        if($this->request->isAjax()){
            $where = [
                ['products.stock','<',90],
                ['spu.on_sale','=',1],
                ['spu.is_deleted','=',0],
            ];
            if(session('user.spu_ids'))
                $where[] = ['spu.spu_id','in',session('user.spu_ids')];

            $rows = ProductsModel::leftJoin('spu','spu.spu_id = products.spu_id')
                ->field('products.stock,product_id,sku_param,products.spu_id')
                ->where($where)
                ->select()
                ->toArray()
                ;
//            halt($rows);
            foreach($rows as $k=>$v){
                $spu = Spu::field('spu_name,spu_num,spu_param')->find($v['spu_id']);
                $rows[$k]['param'] = [];
                if($spu['spu_param']){
                    foreach ($spu['spu_param'] as $val){
                        $rows[$k]['param'][] = $val['name'].':'.$v['sku_param'][toPinYin($val['name'])];
                    }
                }
                $rows[$k]['param'] = $rows[$k]['param']?implode(',',$rows[$k]['param']):'无';
                $rows[$k]['spu_name'] = $spu['spu_name'];
                $rows[$k]['spu_num'] = $spu['spu_num'];
            }
            return __successData('成功',$rows);
        }
    }

    /**
     * 库存修改
     *
     * @return \think\response\Json
     */
    public function add_stock()
    {
        $id = $this->request->post('id');
        $num = $this->request->post('num');
        $type = $this->request->post('type');//stock良品，bad_stock不良品
        if(!$id)
            return __error('商品信息不存在');
        if(!$type)
            return __error('缺少操作类型');
//        if($num<=0)
//            return __error('库存数量必须大于或等于0');
        $r = ProductsModel::stock($id,$num,$type);

        if(!$r['code']){
            $tag = $type=='stock'?'良品':'不良品';
            $stock = $r['data']['stock'];
            $row = [
                'log_type'=>$type,
                'num'=>$num,
                'product_id'=>$id,
                'log_detail'=>"$tag 库存变化 $num ，原库存$stock ，现库存 ".($stock+$num),
            ];
            SystemLog::create(save_common_field($row));
            if($num>0)
                Order::book();
        }

        return $r;
    }

    /**
     * 库存历史
     *
     * @param string $id
     * @return mixed|string
     */
    public function stock_log($id='')
    {
        if(!$id)
            return msg_error('商品信息不存在');

        $row = SystemLog::where('product_id',$id)->order('create_at desc')->select();

        $var = [
            'log'=>$row,
        ];

        return $this->fetch('',$var);
    }

    public function stockOrigin()
    {
        $products = ProductsModel::where('is_deleted',0)->all();
        foreach ($products as $v){
            $stock_change = SystemLog::where(['product_id'=>$v['product_id'],'log_type'=>'stock'])->sum('num');
            $order_quantity = \app\common\model\Order::where('good_num',$v['product_num'])->sum('quantity');
            $num = $v['stock'] - $stock_change + $order_quantity;
            $num = $num<0?0:$num;
            $row = [
                'log_type'=>'stock',
                'product_id'=>$v['product_id'],
                'num'=>$num,
                'log_detail'=>"商品 $v[product_num] ，首次添加，良品库存 $num ，不良品库存0",
                'create_at'=>strtotime($v['create_at']),
            ];

            SystemLog::create(save_common_field($row));
//            halt('111');
//            dump($v['product_id']);
//            dump('现库存:'.$v['stock']);
//            dump('卖掉:'.$order_quantity);
//            dump('变化:'.$stock_change);
//            dump($row);
        }
        exit('完成');
    }

    public function read($id)
    {
        $spu = Spu::field('spu_name,spu_content')->find($id);
        $var = [
            'spu'=>$spu,
        ];
        return $this->fetch('',$var);
    }

    public function stock_edit(Request $request)
    {
        if($request->isAjax()){
            if(!$request->param('id'))
                return __error('缺少spu_id');
            $spu_id = $request->param('id');
            $spu = Spu::find($spu_id)->toArray();
            $sku = ProductsModel::where('spu_id',$spu_id)
                ->select()
                ->toArray();
            $sku = setSkuParam($sku,(array)$spu['spu_param']);
            return __successData('success',$sku);
        }
        $var = [

        ];

        return $this->fetch('',$var);
    }

    public function del_rule()
    {
        $id = $this->request->post('id');

        SpuRule::where('id',$id)->delete();

        return __success('删除成功');
    }

    public function import()
    {
        if ($this->request->isAjax()){
            if(!$this->request->post('file') && !is_file($this->request->post('file'))){
                return __error('需要导入的文件不存在，请尝试重新导入');
            }
            $row = $this->request->post('row')?:2;
            $file = $this->request->post('file');
            $data = excelImport($file,config('excel')['product']['import'],1,$row);
            if($data['code'])
                return $data;
            $info = ['total'=>(int)$data['data']['total'],'row'=>(int)$row,'percent'=>$data['data']['total']==0?0:bcmul($row/$data['data']['total'],100)];
            if($data['data']['total']<=0)
                return __error("没有导入信息",$info);

            $arr = $data['data']['data'];

            $arr['spu_type'] = array_search($arr['spu_type'],config('spu_type'));
//            halt($arr);
            if($arr['spu_type']!=2)
                return __error('只能导入非电购商品',$info);

            $spu = Spu::where('spu_num',$arr['spu_num'])->find();
            if($spu)
                return __error("$arr[spu_num] 相同商品编码已存在",$info);

            $supplier = Supplier::whereLike('supplier_name',"%$arr[supplier_id]%")->find();
            if(!$supplier)
                return __error("没有找到对应的供应商",$info);

            $arr['supplier_id'] = $supplier->id;
            Spu::startTrans();
            $spu_res = Spu::create(save_common_field($arr));
            ProductsModel::create(save_common_field([
                'spu_id'=>$spu_res->spu_id
            ]));
            Spu::commit();

            $msg = '导入成功';

            return __successData($msg,$info) ;
        }

        return $this->fetch();
    }

    public function num($id)
    {
        $rows = SpuRule::where('spu_id',$id)->select();

        $var = [
            'data'=>$rows
        ];

        return $this->fetch('',$var);
    }
}
