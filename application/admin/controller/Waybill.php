<?php

namespace app\admin\controller;

use app\common\controller\AdminController;
use app\common\model\MemberAddress;
use app\common\model\OrdDetail;
use app\common\model\OrgModel;
use app\common\model\Route;
use app\common\service\YouZanService;
use think\Controller;
use think\Request;
use app\common\model\Waybill as WaybillModel;
use app\common\model\Order;
use app\common\model\Products;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;
use app\common\model\Spu;
use app\common\service\JdService;
use think\Db;

class Waybill extends AdminController
{

    protected $model = null;

    public function __construct()
    {
        parent::__construct();
        $this->model = new WaybillModel();
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index($sum=0)
    {

        if($this->request->isAjax()){
            $page = $this->request->post('page',1);
            $limit = $this->request->post('limit',10);
            $search = $this->request->post('select',[]);
            $ord_id = $this->request->param('ord_id');
            $where = [
//                ['products.is_deleted','=',0],
            ];
            $whereSql = '';
            $field = [
                'waybill.waybill_id',
                'waybill_num',
                'org_waybill',
                'waybill.ord_num',
                'order.collection_payment',
                'status_text',
                'is_cancel',
                'order.is_finance',
                'is_trade',
                'order.quantity',
                'ord_at',
                'ord_detail.spu_num',
                'spu_name',
                'member.*',
                'waybill.create_at',
                'waybill.update_at',
                'waybill.waybill_at',
                'waybill.package_num',
                'waybill.out_instruction',
                'waybill.deliver_id',
                'ord_type',
                'GROUP_CONCAT(concat_ws("-",spu_param,sku_param,ord_detail.quantity) SEPARATOR "|") product_detail',
                'verify',
                'verify_at',
                'print_count',
            ];

            if($ord_id)
                $search['waybill.ord_id'] = $ord_id;

            $where = get_query_condition($where,$search,$this->model->getTable());

            if(isset($search['jd']) && $search['jd']!=='')
                $whereSql = setWhere($whereSql,$search['jd']==1?"waybill_num IS NOT NULL OR waybill_num!=''":"waybill_num IS NULL OR waybill_num=''");

            if(session('user.spu_ids'))
                $where[] = ['spu.spu_id','in',session('user.spu_ids')];

            if($sum){

                $total = Db::table(WaybillModel::basic($where,$whereSql)->field('waybill.collection_payment,good_count')->group('waybill_id')->buildSql().' waybill')
                    ->field('sum(collection_payment) collection_payment,sum(good_count) quantity')->find();

                $count = WaybillModel::basic($where,$whereSql)->count('DISTINCT waybill_id');

                $var = [
                    'count'=>$count,
                    'totalMoney'=>$total['collection_payment'],
                    'average'=>$count?round($total['collection_payment']/$count,2):0,
                    'totalQuantity'=>$total['quantity'],
                ];
                return __successData('success',$var);
            }

            $order = WaybillModel::basic($where,$whereSql)
                ->field($field)
                ->group('waybill_id')
//                ->whereNotNull('waybill_num')
                ->order("waybill_id desc,waybill.waybill_at desc,status desc")
//                ->page($page,$limit)
                ->paginate($limit)
                ->each(function($val){
//                    halt($val);
                    $val['is_finance'] = $val['is_finance']==1?'是':'否';
                    $val['is_trade'] = $val['is_trade']==1?'是':'否';
                    $val['status_text'] = $val['status_text']?:'';
                    $val['is_cancel'] = $val['is_cancel']==1?'是':'否';
                    $val['verify'] = getEnum($val['verify'],'yORn');
                    $val['ord_at'] = $val['ord_at']?date('Y-m-d H:i:s',$val['ord_at']):'';
                    $val['member_phone'] = $val['member_phone']?hidePhone($val['member_phone']):'';
                    $val['member_tel'] = $val['member_tel']?hidePhone($val['member_tel']):'';
                    $val['out_instruction'] = getEnum($val['out_instruction'],'out_instruction');
                    $val['ord_type'] = getEnum($val['ord_type'],'ord_type');
                    $product_detail = [];
                    $val['product_detail'] = explode('|',$val['product_detail']);
                    foreach ($val['product_detail'] as $v){
                        $v = explode('-',$v);
                        if(count($v)!=3)
                            continue;
                        $v[0] = json_decode($v[0],true);
                        $v[1] = json_decode($v[1],true);

                        foreach ($v[0] as $vv){
                            $product_detail[] = $v[1][toPinYin($vv['name'])].'*'.$v[2];
                        }
                    }
                    $val['product_info'] = implode('|',$product_detail);
                });
//            dd($order);
            return __successData('获取成功',$order->items(),1,$order->total(),$limit,$page);
        }

        $var = [
            'status'=>config('trade_status'),
            'balance'=>config('balance'),
            'deliver'=>OrgModel::all(),
            'route'=>Route::where('is_deleted',0)->select(),
        ];

        return $this->fetch('',$var);
    }



    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id,$export=0)
    {
        $row = WaybillModel::with('deliver')->find($id);

        $order = Order::find($row['ord_id']);

        $order['is_finance'] = $order['is_finance']==1?'是':'否';
        $row['is_trade'] = $row['is_trade']==1?'是':'否';
        $order['ord_route'] = isset(config('ord_route')[$order['ord_route']])?config('ord_route')[$order['ord_route']]:'无';
        $row['refund_pic'] = $row['refund_pic']?$this->request->domain().'/'.$row['refund_pic']:'';

        $spu_num = $spu_name = [];
        $row['price'] = 0;

        $spu = Spu::where('spu_id','in',$row['spu_id'])->select();

        foreach ($spu as $v){
            $spu_num[] = $v['spu_num'];
            $spu_name[] = $v['spu_name'];
            $row['price'] += $v['price'];
        }

        $product = Products::leftJoin('ord_detail',"ord_detail.sku_id = products.product_id")
            ->leftJoin('spu','spu.spu_id = products.spu_id')
            ->field('products.*,quantity,spu_name')
            ->where('product_id','in',$row['sku_id'])
            ->where('ord_detail.ord_id',$row['ord_id'])
            ->select()->toArray()
        ;

        foreach ($product as &$v){
            $v['param'] = $v['spu_name'];
            if($v['sku_param']){
                foreach ($v['sku_param'] as $val){
                    $v['param'] .= $val;
                }
            }

        }
//        halt($product);
        $member = \app\common\model\Member::find($order['member_id']);

        $address = MemberAddress::find($order['address_id']);

        $addr = getAddrName($address['province'])
            .getAddrName($address['city'])
            .getAddrName($address['county'])
            .getAddrName($address['street'])
            .$address['detail'];

        $var = [
            'waybill'=>$row,
            'ord'=>$order,
            'product'=>$product,
            'member'=>$member,
            'addr'=>$addr,
            'spu_name'=>implode('，',$spu_name),
            'spu_num'=>implode('，',$spu_num),
        ];

        if($export){
            $spu = $spu[0];
            $wordExportData = [
                'ord_num'=>$row['ord_num'],
                'org_ord'=>$order['org_ord'],
                'waybill_num'=>$row['waybill_num'],
                'org_waybill'=>$row['org_waybill'],
                'member_name'=>$member['member_name'],
                'status'=>$row['status_text'],
                'phone'=>$member['member_phone'],
                'tel'=>$member['member_tel'],
                'address'=>$addr,
                'product_name'=>$spu['spu_name'],
                'product_num'=>$spu['spu_num'],
                'product_size'=>$spu['spu_length'].'cm '.$spu['spu_width'].'cm '.$spu['spu_height'].'cm',
                'product_weight'=>$spu['weight'],
                'ord_money'=>$order['ord_money'],
                'finance'=>$order['is_finance']==1?'是':'否',
                'trade'=>$row['is_trade']==1?'是':'否',
                'route'=>getEnum($order['ord_route'],config('ord_route'),'无'),
                'back_reason'=>$row['back_reason'],
                'product_detail'=>"规格属性\n\n\n\n\n\n\n\n单价\n\n\n\n\n\n\n\n数量\n\n\n\n\n\n\n\n总价<w:br/>",
            ];
            if($product)
                foreach ($product as $v){
                    $param = !empty($v['param'])?$v['param']:"无\n\n\n\n\n";
                    $wordExportData['product_detail'] .= "$param\n\n\n\n\n\n\n\n$v[price]\n\n\n\n\n\n\n\n$v[quantity]\n\n\n\n\n\n\n\n".$v['price']*$v['quantity']."<w:br/>";
                }

            $r = wordExport('waybill_read.docx',$wordExportData);
            if($r){
//                header("Location:".$this->request->domain().$r);
                $this->redirect($this->request->domain().$r,302);
            }else{
                alert('导出失败');
            }

        }

        if($this->request->isAjax()){
            return __successData('success',$row);
        }
//        halt($var);
        return $this->fetch('',$var);
    }

    public function import(Request $request)
    {
        if ($request->isAjax()){
            if(!$request->post('file') && !is_file($request->post('file'))){
                return __error('需要导入的文件不存在，请尝试重新导入');
            }
            $row = $request->post('row')?:2;
            $file = $request->post('file');
            $data = excelImport($file,config('excel')['waybill']['import'],1,$row);
            if($data['code'])
                return $data;
            $info = ['total'=>(int)$data['data']['total'],'row'=>(int)$row,'percent'=>$data['data']['total']==0?0:bcmul($row/$data['data']['total'],100)];
            if($data['data']['total']<=0)
                return __error("没有导入信息",$info);

            $arr = $data['data']['data'];
//            halt($arr);
            $arr['over_area'] = $arr['over_area']=='是'?1:0;
            $arr['parcel_insurance'] = $arr['parcel_insurance']=='是'?1:0;
            $arr['refuse'] = $arr['refuse']=='是'?1:0;
            $arr['status'] = $arr['status_text']=='已取消'?-2:0;
            $arr['waybill_at'] = $arr['waybill_at']?strtotime($arr['waybill_at']):'';
            $arr['predict_arrive_at'] = $arr['predict_arrive_at']?strtotime($arr['predict_arrive_at']):'';
            $arr['update_at'] = $arr['update_at']?strtotime($arr['update_at']):'';
//            dd($arr);die;
            //京东接口下单
            //

            $validate = $this->validate($arr,'app\admin\validate\Waybill.add');
            if (true !== $validate) return __error($validate);

            $msg = '导入成功';

//            $order = Order::where(['ord_num'=>$data['data']['data']['ord_num']])->find();
//            if(!$order)
//                return __error("订单号：".$data['data']['data']['ord_num']." 信息不存在，请先导入订单",$info);

            $exist = WaybillModel::where(['waybill_num'=>$arr['waybill_num']])->find();
            if($exist){
                $this->model->update($arr,['waybill_num'=>$arr['waybill_num']]);
            }else{
                $this->model->create($arr);
            }

            return __successData($msg,$info) ;
        }

        return $this->fetch();

    }

    public function importSf(Request $request)
    {
        if ($request->isAjax()){
            if(!$request->post('file') && !is_file($request->post('file'))){
                return __error('需要导入的文件不存在，请尝试重新导入');
            }
            $row = $request->post('row')?:2;
            $file = $request->post('file');
            $data = excelImport($file,config('excel')['waybillSF']['import'],1,$row);
            if($data['code'])
                return $data;
            $info = ['total'=>(int)$data['data']['total'],'row'=>(int)$row,'percent'=>$data['data']['total']==0?0:bcmul($row/$data['data']['total'],100)];
            if($data['data']['total']<=0)
                return __error("没有导入信息",$info);

            $arr = $data['data']['data'];

            $msg = '导入成功';

//            $order = Order::where(['ord_num'=>$data['data']['data']['ord_num']])->find();
//            if(!$order)
//                return __error("订单号：".$data['data']['data']['ord_num']." 信息不存在，请先导入订单",$info);
            $order_num = strpos($arr['ord_num'],'-')!==false?explode('-',$arr['ord_num'])[0]:$arr['ord_num'];
            $order = Order::where('ord_num',$order_num)->find();

            if(!$order)
                return __error('找不到订单信息',$info);

            $waybill = WaybillModel::where(['waybill_num'=>$arr['waybill_num']])->find();
            if($waybill && $waybill->ord_id != $order->ord_id)
                return __error('运单号已存在，但订单号与导入订单号不一致',$info);
            $exist = WaybillModel::where(['ord_num'=>$arr['ord_num']])->find();
            if(!$exist)
                return __error('找不到对应运单记录',$info);

            $deliver = OrgModel::whereLike('org_name',"%$arr[deliver_name]%")->find();
            if(!$deliver)
                return __error('找不到对应车队',$info);

            //有赞发货处理
//            $r = OrdDetail::yzSend($order->ord_route,$order->ord_num,$deliver->youzan_id,$arr['waybill_num'],$oid);
            $route = Route::where(['yz_send_good'=>1,'id'=>$order->ord_route])->find();
            if($route){
                $oid = OrdDetail::where('ord_id',$order->ord_id)->whereIn('sku_id',$exist->sku_id)->column('oid');
                $r = (new YouZanService())->sendGoods([
                    'out_stype'=>(string)$deliver->youzan_id,
                    'out_sid'=>$arr['waybill_num'],
                    'tid'=>$order->ord_num,
                    'oids'=>implode(',',$oid),
                ]);
                if($r)
                    $arr['yz_send'] = 1;
            }

            $arr['deliver_id'] = $deliver->org_id;
            $arr['out_instruction'] = 2;
            $arr['waybill_at'] = time();
            $order->ord_type = 1;
            $order->save();
//            $arr = array_merge($order->toArray(),$arr);
//            halt($arr);
            if(!$exist['waybill_num'] || $exist['waybill_num']==$arr['waybill_num']){
                $this->model->update($arr,['waybill_id'=>$exist['waybill_id']]);
            }else{
                $arr = array_merge($exist->hidden(['waybill_id','create_at','update_at'])->toArray(),$arr);
                unset($arr['waybill_id']);
                WaybillModel::create($arr);
            }
            $order->ord_type = 1;
            $order->save();

            return __successData($msg,$info);
        }

        return $this->fetch();

    }

    public function trade($id)
    {
        $waybill = WaybillModel::find($id);
        $var = [
            'trade'=>(new JdService())->trade($waybill['waybill_num']),
        ];

        return $this->fetch('',$var);
    }

    public function export(Request $request,$ord_id='')
    {
        set_time_limit(0);
//        die('ok');
        $search = $request->param();
//        $where = [
//            ['products.is_deleted','=',0]
//        ];
        $whereSql = '';
        $where = [];
        if($ord_id)
            $where[] = ['waybill.ord_id','=',$ord_id];
        $field = [
            'ord_type',
            'waybill.ord_num',
            'waybill.waybill_num',
            'spu_name',
            'org_name',
            'waybill.out_instruction',
            'status_text',
            'is_cancel',
            'waybill.collection_payment',
            'is_trade',
            'verify',
            'good_count',
            'waybill.package_num',
            'spu_name product_info',
            'member_name',
            'member_phone',
            'member_tel',
            'province',
            'city',
            'county',
            'street',
            'detail',
            'waybill_at',
            'ord_at',
            'waybill.create_at',
//            "is_finance",
//            'spu_num',
            'GROUP_CONCAT(concat_ws("-",spu_param,sku_param,ord_detail.quantity) SEPARATOR "|") product_detail',
        ];

        $where = get_query_condition($where,$search,$this->model->getTable());

        if(isset($search['jd']) && $search['jd']!=='')
            $whereSql = setWhere($whereSql,$search['jd']==1?"waybill_num IS NOT NULL OR waybill_num!=''":"waybill_num IS NULL OR waybill_num=''");

        if(session('user.spu_ids'))
            $where[] = ['spu.spu_id','in',session('user.spu_ids')];

        $order = WaybillModel::field($field)
            ->leftJoin('order','waybill.ord_id = order.ord_id')
            ->leftJoin('ord_detail','ord_detail.ord_id = order.ord_id && find_in_set(ord_detail.sku_id,waybill.sku_id)')
            ->leftJoin('products','products.product_id = ord_detail.sku_id')
            ->leftJoin('spu','spu.spu_id = products.spu_id')
            ->leftJoin('member','member.member_id = order.member_id')
            ->leftJoin('warehouse','warehouse.wh_id = waybill.wh_id')
            ->leftJoin('org','org.org_id = waybill.deliver_id')
            ->leftJoin('member_address','member_address.address_id = order.address_id')
            ->where($where)
            ->where($whereSql)
            ->group('waybill_id')
            ->order("waybill.waybill_at desc,status desc")
//            ->fetchSql()
            ->select()
            ->each(function($val){
//                $val['is_finance'] = $val['is_finance']==1?'是':'否';
                $val['is_trade'] = $val['is_trade']==1?'是':'否';
                $val['status_text'] = $val['status_text']?:'';
                $val['is_cancel'] = $val['is_cancel']==1?'是':'否';
                $val['ord_at'] = $val['ord_at']?date('Y-m-d H:i:s',$val['ord_at']):'';
                $val['member_phone'] = $val['member_phone']?hidePhone($val['member_phone']):'';
                $val['member_tel'] = $val['member_tel']?hidePhone($val['member_tel']):'';
                $val['province'] = getAddrName($val['province']);
                $val['city'] = getAddrName($val['city']);
                $val['county'] = getAddrName($val['county']);
                $val['street'] = getAddrName($val['street']);
                $val['out_instruction'] = getEnum($val['out_instruction'],'out_instruction');
                $val['ord_type'] = getEnum($val['ord_type'],'ord_type');
                $product_detail = [];
                $val['product_detail'] = explode('|',$val['product_detail']);
                foreach ($val['product_detail'] as $v){
                    $v = explode('-',$v);
                    if(count($v)!=3)
                        continue;
                    $v[0] = json_decode($v[0],true);
                    $v[1] = json_decode($v[1],true);

                    foreach ($v[0] as $vv){
                        $product_detail[] = $v[1][toPinYin($vv['name'])].'*'.$v[2];
                    }
                }
                $val['product_info'] = implode('|',$product_detail);
                unset($val['product_detail']);
            })
            ->toArray()
            ;
//        halt($order);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=运单明细.csv' );
        header('Cache-Control: max-age=0');

        $title = [
            '订单类型','订单号','运单号','商品名','车队','出库指示','运单状态','拦截','代收货款','运费结算','核销',
            '商品数量','包裹数量','商品明细','客户','手机','固话','省','市','区','街道','详细地址','下单时间','订单时间','订单时间',
//            '重量(KG)','体积重(KG)','运费','代收手续费','保价费',
        ];
        put_csv($order,$title);
    }


    public function back(Request $request)
    {
        $post = $request->post();
        if(!$post['id'])
            return __error('缺少运单ID');
        $waybill = WaybillModel::find($post['id']);
        if(!$waybill)
            return __error('运单信息不存在');
        if($waybill['status']!=2)
            return __error('妥投订单才能退货确认');
        if($post['is_back']&&empty($post['back_reason']))
            return __error('请填写退货原因');
        $waybill->is_back = $post['is_back'];
        if($waybill->is_back==1){
            $waybill->back_num = 'R'.$waybill->waybill_num;
            $waybill->back_at = time();
        }elseif($waybill->is_back==0){
            $waybill->back_cancel_at = time();
        }
        if(!empty($post['back_reason']))
            $waybill->back_reason = $post['back_reason'];
        $r = $waybill->save();
        return $r?__success('操作成功'):__error('操作失败');
    }

    public function back_index(Request $request)
    {
        if($this->request->isAjax()){
            $page = $request->post('page',1);
            $limit = $request->post('limit',10);
            $search = $request->post('select','');
            $where = [
                ['is_back','=',1]
            ];
            $field = [
                'waybill_id',
                'back_num',
                'order.ord_num',
                'org_ord',
                'waybill_num',
                'org_waybill',
                'order.collection_payment',
                'waybill.is_finance',
                'waybill.is_trade',
                'is_back',
                'is_recieved',
                'is_refund',
                'recieved_at',
                'refund_at',
                'status_text',
                'order.quantity',
                'waybill.reciever',
                'ord_at',
                'spu_num',
                'spu_name',
                'member.*',
                'route.route_name ord_route',
            ];
            if($search){
                foreach($search as $k=>$v){
                    if($v=='') continue;
                    if($k == 'keyword'){
                        $where[] = ['order.ord_num|waybill.waybill_num|spu_name|spu_num|back_num','like',"%$v%"];
                        continue;
                    }elseif($k == 'waybill_at'){
                        $where[] = [$k,'between time',setSearchTime($v)];
                        continue;
                    }else{
                        $where[] = [$k,'in',$v];
                    }
                }

            }

            $order = WaybillModel::field($field)
                ->leftJoin('order','waybill.ord_id = order.ord_id')
                ->leftJoin('products','products.product_id = order.sku_id')
                ->leftJoin('spu','spu.spu_id = products.spu_id')
                ->leftJoin('member','member.member_id = order.member_id')
                ->leftJoin('route','route.id = order.ord_route')
                ->where($where)
                ->order("waybill.waybill_at desc,waybill.status desc")
                ->paginate($limit)
                ->each(function($val){
                    $val['is_finance'] = $val['is_finance']==1?'是':'否';
                    $val['is_trade'] = $val['is_trade']==1?'是':'否';
                    $val['is_back'] = $val['is_back']==1?'是':'否';
                    $val['is_recieved'] = $val['is_recieved']==1?'是':'否';
                    $val['is_refund'] = $val['is_refund']==1?'是':'否';
                    $val['status_text'] = $val['status_text']?:'';
                });
//            dd($order);
            return __successData('获取成功',$order->items(),1,$order->total(),$limit,$page);
        }

        $var = [
            'status'=>config('trade_status'),
            'balance'=>config('balance'),
        ];

        return $this->fetch('',$var);
    }

    public function recieve($id)
    {
        $waybill = WaybillModel::find($id);
        if($waybill['is_back']==0)
            return __error('此单没有退货申请');
        $row = [
            'is_recieved'=>1,
            'recieved_at'=>time(),
        ];
        $r = WaybillModel::update($row,['waybill_id'=>$id]);
        return $r?__successData('操作成功'):__successData('操作失败');
    }

    public function refund(Request $request)
    {
        $post = $request->post();
        if(!$post['waybill_id'])
            return __error('缺少运单ID');
        $waybill = WaybillModel::find($post['waybill_id']);
        if(!$post['waybill_id'])
            return __error('没有运单信息');
        if($waybill['is_recieved']==0)
            return __error('此单没有退货接收确认');
//        if($waybill['is_refund']==1)
//            return __error('此单已回款确认');
        $post['is_refund'] = 1;
        $post['refund_at'] = time();
        $r = WaybillModel::update($post);
        return $r?__successData('操作成功'):__successData('操作失败');
    }

    public function cancel(Request $request)
    {
        $post = $request->post();
        if(!$post['id'])
            return __error('请选择运单');
        if(!$post['cancel_reason'])
            return __error('请输入拦截原因');


        $waybill = WaybillModel::find($post['id']);
        if(!$waybill)
            return __error('运单信息不存在');
        if(in_array($waybill['status'],[-2,2]))
            return __error('已妥投或取消的运单不能拦截');

        $r = (new JdService())->cancel($waybill['waybill_num'],$post['cancel_reason']);
        if($r['code']) return $r;

        if($r['data']['stateCode']==100){
            $waybill->cancel_reason = $post['cancel_reason'];
            $waybill->is_cancel = 1;
            $waybill->cancel_at = time();
            $waybill->save();
            return __success('拦截发起成功');
        }else{
            return __error($r['data']['stateMessage']);
        }

    }

    public function wordExport()
    {
        $template = new TemplateProcessor('./static/word/waybill_read.docx');
        $file_path = './uploads/word/';
        if (!is_dir($file_path)){
            mkdir($file_path,0777,true);
        }
        $template->setValue('ord_num','123123');
        $r = $template->saveAs($file_path.'1.docx');
        halt($r);
        exit;
    }

    public function save()
    {
        if($this->request->isAjax()){
            $post = $this->request->post();
            $order = Order::field(
                'ord_id,ord_num,order.spu_id,sku_id,collection_payment,deliver_id,receiver,phone reciever_phone,
                addr reciever_addr,order.quantity,spu.package_num num,weight,spu_length,spu_width,spu_height,province,city,county,
                street,detail,member_name,member_phone,member_tel'
                )
                ->leftJoin('spu','spu.spu_id = order.spu_id')
                ->leftJoin('member_address','member_address.address_id = order.address_id')
                ->leftJoin('member','member.member_id = order.member_id')
                ->find($post['ord_id'])
//                ->toArray()
                ;
            if(!$order['reciever_addr']){
                $order['province'] = getAddrName($order['province']);
                $order['city'] = getAddrName($order['city']);
                $order['county'] = getAddrName($order['county']);
                $order['street'] = getAddrName($order['street']);
                $order['reciever_addr'] = $order['province'].$order['city'].$order['county'].$order['street'].$order['detail'];
            }
            $order['reciever'] = $order['receiver']?:$order['member_name'];
            $order['reciever_phone'] = $order['reciever_phone']?:$order['member_phone'];
            $order['reciever_tel'] = $order['member_tel'];
            $order['package_num'] = $order['num']>1?ceil($order['quantity']/$order['num']):$order['quantity'];

            $waybill = WaybillModel::where('ord_num',$order['ord_num'])->find();
            if($waybill){
                $waybill->waybill_num = $post['waybill_num'];
                $waybill->save();
            }else{
                $row = charge($order->toArray());
                $row['waybill_num'] = $post['waybill_num'];
                WaybillModel::create($row);
            }

            return __success('修改成功');
        }
    }

    public function printer()
    {
        $id = $this->request->param('id');
        if(!$id)
            return __error('请选择运单');

        $rows = WaybillModel::with(['deliver','warehouse','ord'])->whereNotNull('waybill_num')->whereIn('waybill_id',$id)->select()->each(function($v){
            $v->time = date('Y-m-d H:i:s');
            $v->promise_time_type = getEnum($v->promise_time_type,'promise_time_type');
            $v->trans_type = getEnum($v->trans_type,'trans_type');
            $v->good_detaiil = OrdDetail::with(['sku','spu'])->whereIn('ord_id',$v->ord_id)->select();
            $address = MemberAddress::with(['province','city','county','street'])->find($v->ord->address_id);
            $v->reciever_addr = $address['province_name'].$address['city_name'].$address['county_name'].$address['street_name'].$address['detail'];
            $param = [];
            foreach ($v->good_detaiil as $val){
                $param[] = $val->spu->spu_name.($val->spu->spu_param?implode('',$val->sku->sku_param):'').'*'.$val->quantity;
            }
            $v->param = implode('|',$param);
        });
        if(count($rows)!=count($id))
            return __error('存在没有运单号的运单，请检查');
        return __success('success',$rows);
    }

    public function verify()
    {
        $id = $this->request->param('id');
        if(!$id)
            return __error('请选择运单');

        $r = WaybillModel::whereIn('waybill_id',$id)->update([
            'verify'=>1,
            'verify_at'=>time(),
            'verify_by'=>session('user.id'),
        ]);

        return __success('操作成功');
    }

    public function back_export()
    {
        set_time_limit(0);

        $search = $this->request->param();
        $where = [
            ['is_back','=',1]
        ];
        $field = [
            'back_num',
            'order.ord_num',
            'org_ord',
            'waybill_num',
            'org_waybill',
            'route.route_name ord_route',
            'order.collection_payment',
            'is_recieved',
            'is_refund',
            'member.member_name',
            'member.member_phone',
            'member.member_tel',
            'recieved_at',
            'refund_at',
        ];

        $where = get_query_condition($where,$search,$this->model->getTable());

        $order = WaybillModel::field($field)
            ->leftJoin('order','waybill.ord_id = order.ord_id')
            ->leftJoin('products','products.product_id = order.sku_id')
            ->leftJoin('spu','spu.spu_id = products.spu_id')
            ->leftJoin('member','member.member_id = order.member_id')
            ->leftJoin('route','route.id = order.ord_route')
            ->where($where)
            ->order("waybill_id desc")
            ->select()
            ->each(function($val){
                $val['is_recieved'] = getEnum($val['is_recieved'],'yORn');
                $val['is_refund'] = getEnum($val['is_refund'],'yORn');
            })->toArray()
        ;
//        halt($order);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=退货单明细.csv' );
        header('Cache-Control: max-age=0');

        $title = [
            '退货号','订单号','原订单号','运单号','原运单号','通路','代收货款','退款接收','退款','客户','手机',
            '固话','退货接收时间','退款确认时间'
        ];
        put_csv($order,$title);
    }

    public function print_log()
    {
        $waybill_id = $this->request->param('waybill_id');
        $count = $this->request->param('count');

        if(!$waybill_id)
            return __error('缺少waybill_id');

        WaybillModel::whereIn('waybill_id',$waybill_id)->setInc('print_count',$count);
        return __success('success');
    }
}
