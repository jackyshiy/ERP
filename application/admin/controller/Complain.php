<?php

namespace app\admin\controller;

use app\common\controller\AdminController;
use think\Controller;
use think\Request;
use app\common\model\Complain as ComplainModel;

class Complain extends AdminController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $id = $this->request->param('id');
        if($this->request->isAjax()){
            $page = $this->request->post('page',1);
            $limit = $this->request->post('limit',10);
            $search = $this->request->post('select','');

            $where = [
                ['complain.waybill_id|complain.ord_id','=',$id],
            ];
            $field = [
                'complain.*',
                'waybill_num',
                'ord_num',
                'username',
            ];
            if($search){
                foreach($search as $k=>$v){
                    if($v=='') continue;
                    if($k == 'keyword'){
                        $where[] = ['order.ord_num|waybill.waybill_num|spu_name|spu_num','like',"%$v%"];
                        continue;
                    }
                    if($k == 'waybill_at'){
                        $where[] = [$k,'between time',setSearchTime($v)];
                        continue;
                    }
                    $where[] = [$k,'in',$v];
                }

            }

            $rows = ComplainModel::leftJoin('waybill','waybill.waybill_id = complain.waybill_id')
                ->leftJoin('system_user','system_user.id = complain.create_by')
                ->field($field)
                ->where($where)
                ->paginate()
                ->each(function($val){
                    $val['is_finished'] = $val['is_finished']==1?'是':'否';
                })
            ;

            return __successData('success',$rows->items(),1,$rows->total(),$limit,$page);
        }

        $var = [
            'waybill_num'=>\app\common\model\Waybill::where('waybill_id',$id)->value('waybill_num'),
        ];

        return $this->fetch('',$var);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $waybill_id = $this->request->param('waybill_id');
        $var = [
            'waybill_id'=>$waybill_id,
            'ord_id'=>\app\common\model\Waybill::where('waybill_id',$waybill_id)->value('ord_id'),
        ];
        return $this->fetch('form',$var);
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save()
    {
        $post = $this->request->post();
        $validate = $this->validate($post, 'app\admin\validate\Complain.save');
        if (true !== $validate) return __error($validate);



        if(!empty($post['complain_id'])){
            $r = ComplainModel::update(save_common_field($post));
        }else{
            $r = ComplainModel::create(save_common_field($post));
        }

        return $r?__success('操作成功'):__error('操作失败');
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $complain = ComplainModel::find($id);
        $var = [
            'complain'=>$complain
        ];

        return $this->fetch('form',$var);
    }


    public function finish($id)
    {
        $r = ComplainModel::update(['is_finished'=>1,'finished_at'=>time()],['complain_id'=>$id]);

        return $r?__success('操作成功'):__error('操作失败');
    }

    public function show(Request $request,$spu_id)
    {
        if($request->isAjax()){
            $row = ComplainModel::leftJoin('waybill','waybill.waybill_id = complain.waybill_id')
                ->leftJoin('system_user','system_user.id = complain.create_by')
                ->where('spu_id',$spu_id)
                ->field('complain_id,complain_content,complain_handle,waybill_num,complain.create_at,username')
                ->select()
            ;

            return __successData('success',$row);
        }

        return $this->fetch('');
    }
}
