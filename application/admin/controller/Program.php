<?php

namespace app\admin\controller;

use app\common\controller\AdminController;
use think\Request;
use app\common\model\Program as ProgramModel;
use app\common\model\Products;
use app\common\model\Spu;
class Program extends AdminController
{

    protected $model = null;

    public function __construct()
    {
        parent::__construct();
        $this->model = new ProgramModel();
    }
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $search = $this->request->post('select','');
        $where = [
            ['program.is_deleted','=',0],
//            ['spu.is_deleted','=',0],
        ];
        $whereSql = '';
        if($search){
            foreach($search as $k=>$v){
                if($v=='') continue;
                if($k == 'keyword'){
                    $where[] = ['program_name|spu_name','like',"%$v%"];
                }elseif($k == 'live_at'){
                    $time = setSearchTime($v);
                    $sql = "$time[0] <= program_start && program_end <= $time[1]";
                    $whereSql .= $whereSql?' && '.$sql:$sql;
                }else{
                    $where[] = [$k,'in',$v];
                }

            }

        }
        if($this->request->isAjax()){
            $page = $this->request->post('page',1);
            $limit = $this->request->post('limit',10);
//            $count = $this->model->where([])->count();
            $field = [
                '*',
//                "FROM_UNIXTIME(program_start,'%Y/%m/%d') program_start",
//                "FROM_UNIXTIME(program_end,'%Y/%m/%d') program_end",
            ];
//            $source = ProgramModel::where(['is_deleted'=>0])
//                    ->with(['product'=>function($query) {$query->field('product_id,product_name');}])
//                    ->page($page,$limit);
//            if($search){
//                $count = $source->withSearch(['ord_num'],$search)->count();
//                $programes = $source->field($field)
//                    ->withSearch(['ord_num'],$search)->select();
//            }else{
//                $count = $source->count();
//                $programes = $source->field($field)
//                    ->select();
//            }
            $rows = ProgramModel::field($field)
//                ->leftJoin('spu','spu.spu_id = program.spu_id')
                ->where($where)
                ->where($whereSql)
                ->order('program.create_at','desc')
//                ->page($page,$limit)
//                ->fetchSql()
                ->paginate();
//            dd($programes);die;
            return __successData('获取成功',$rows->items(),1,$rows->total(),$limit,$page);
        }

        $var = [
            'channel'=>ProgramModel::group('channel')->column('channel'),
            'position'=>ProgramModel::group('position')->column('position'),
            'duration'=>ProgramModel::group('duration')->order('duration')->column('duration'),
            'present_at'=>ProgramModel::group('present_at')->order('present_at')->column('present_at'),
        ];

        return $this->fetch('',$var);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $rows = Spu::field('spu_id,spu_name')->where('on_sale',1)->where('is_deleted',0)->select();

        $var = [
            'product'=>$rows
        ];
//        dd($rows);
        return $this->fetch('form',$var);
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        if($request->isPost()){
//            dd($request->post('product'));
            $row = $request->post('program');
            $validate = $this->validate($row,'app\admin\validate\Program.save');
            if (true !== $validate) return __error($validate);

            $live_at = setSearchTime($row['live_at']);
            if ($live_at[0]>$live_at[1])
                return __error('开始时间不能大于结束时间');
            $row['program_start'] = $live_at[0];
            $row['program_end'] = $live_at[1];

            $row['create_by'] = $row['update_by'] = session('user.id');

//            dd($row);die;
            if(!empty($row['program_id'])){
                $this->model->save($row,['program_id'=>$row['program_id']]);
            }else{
                $this->model->save($row);
            }
            return __success('操作成功');
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $row = ProgramModel::find($id);
//        $row['live_at'] = date('Y-m-d',$row['program_start']). ' - '.date('Y-m-d',$row['program_end']);

        $row['live_at'] = "$row[program_start] - $row[program_end]";

        $product = Spu::field('spu_name,spu_id')->where('on_sale',1)->where('is_deleted',0)->select();

//        dump($row);
        $var = [
            'program'=>$row,
            'product'=>$product
        ];
        return $this->fetch('form',$var);
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete()
    {
        $id = $this->request->param('id');
        return $this->model->del($id);
    }
}
