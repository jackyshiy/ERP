<?php

namespace app\admin\controller;

use app\common\controller\AdminController;
use app\common\model\MemberAddress;
use app\common\model\OrdDetail;
use app\common\model\OrgModel;
use app\common\model\SpuRule;
use app\common\model\SystemAddress;
use app\common\service\JdService;
use app\common\service\WechatPaymentService;
use think\Db;
use think\Request;
use app\common\model\Order as OrderModel;
use app\common\model\Products;
use app\common\model\Waybill;
use app\common\model\Member;
use app\common\model\Spu;
use app\common\model\Warehouse;
use app\common\service\YouZanService;
use app\common\model\Route;
use app\common\model\DeliverSpeciel;
use app\common\model\WxpayLog;
use Endroid\QrCode\QrCode;

class Order extends AdminController
{

    protected $model = null;


    /**
     * 初始化
     * node constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->model = new OrderModel();
    }


    /**
     * @param Request $request
     * @param int $sum
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index($sum=0)
    {
        if($this->request->isAjax()){
            $search = $this->request->post('select',[]);
            $where = [];

            $whereSql = '';

            $where = get_query_condition($where,$search,$this->model->getTable());

            if(session('user.spu_ids'))
                $where[] = ['spu.spu_id','in',session('user.spu_ids')];

            if($sum){

                $total = Db::table(OrderModel::basic($where,$whereSql)->field('ord_money,order.quantity')->group('order.ord_id')->buildSql().' ord')
                    ->field('sum(ord_money) ord_money,sum(quantity) quantity')->find();

                $count = OrderModel::basic($where,$whereSql)->count('DISTINCT order.ord_id');

                $var = [
                    'count'=>$count,
                    'totalMoney'=>$total['ord_money'],
                    'average'=>$count?round($total['ord_money']/$count,2):0,
                    'totalQuantity'=>$total['quantity'],
                ];
                return __successData('success',$var);
            }

            $limit = $this->request->post('limit',config('limit'));

            $rows = OrderModel::lists($where,$whereSql,$limit);

            return __successData('获取成功',$rows->items(),1,$rows->total(),$limit,$rows->currentPage());
        }

        $var = [
            'status'=>config('trade_status'),
            'balance'=>config('balance'),
            'route'=>Route::where('is_deleted',0)->select(),
            'kefu'=>\app\common\model\User::where('status',1)->whereLike('auth_id','%"6"%')->select(),
        ];
        return $this->fetch('',$var);
    }


    public function import(Request $request)
    {

//        $data = excelImport($file,[],1,1);//excel表头
        if ($request->isAjax()){
            if(!$request->post('file') && !is_file($request->post('file'))){
                return __error('需要导入的文件不存在，请尝试重新上传文件');
            }
            $row = $request->post('row')?:2;
            $file = $request->post('file');
            $cover = $request->post('cover');

            $data = excelImport($file,config('excel')['order']['import'],1,$row);
            if($data['code'])
                return $data;
            $info = ['total'=>(int)$data['data']['total'],'row'=>(int)$row,'percent'=>$data['data']['total']==0?0:bcmul($row/$data['data']['total'],100)];
            if($data['data']['total']<=0)
                return __error("没有导入信息",$info);



            $arr = $data['data']['data'];
//            halt($arr);
            if(!$arr['ord_num'])
                return __error('缺少订单号',$info);
//            if(!$arr['waybill_num'])
//                return __error('缺少运单号',$info);
            if(!$arr['ord_route'])
                return __error('缺少通路',$info);
            if(!$arr['receiver'])
                return __error('缺少客户名',$info);
            if(!$arr['phone'])
                return __error('缺少联系方式',$info);
            if(!$arr['addr'])
                return __error('缺少地址信息',$info);
            if(!$arr['good_num'])
                return __error('缺少商品编码',$info);
            if(!$arr['quantity'])
                return __error('缺少商品数量',$info);

            $order = OrderModel::where('ord_num',$arr['ord_num'])->find();
            if($order && !$cover)
                return __error('重复订单'.$arr['ord_num'].'，是否<a style="color:blue" href="javascript();" class="cover" data-row="'.$row.'">覆盖</a>',$info);

            $arr['ord_route'] = Route::whereLike('route_name',"%$arr[ord_route]%")->value('id');
            if(!$arr['ord_route'])
                return __error('找不到通路信息',$info);

            $arr['ord_at'] = $arr['ord_date']?strtotime($arr['ord_date']):'';
            $arr['ord_money'] = $arr['ord_money'];

            OrderModel::startTrans();
            //用户处理
            $member = Member::where('member_phone',$arr['phone'])->find();
            if(!$member){
                $member = Member::create(['member_name'=>$arr['receiver'],'member_phone'=>$arr['phone']]);
            }else{
                $member->member_name = $arr['receiver'];
                $member->member_phone = $arr['phone'];
                $member->save();
            }
            $arr['member_id'] = $member->member_id;

            //地址处理
            $address = MemberAddress::where('detail',$arr['addr'])->find();

            $add_rows = [
                'province'=>!empty($arr['province'])?$arr['province']:'',
                'city'=>!empty($arr['city'])?$arr['city']:'',
                'county'=>!empty($arr['county'])?$arr['county']:'',
                'street'=>!empty($arr['street'])?$arr['street']:'',
                'detail'=>!empty($arr['addr'])?$arr['addr']:'',
                'member_id'=>$member->member_id
            ];
            if($address){
                $add_rows['address_id'] = $address->address_id;
                $address = MemberAddress::update($add_rows);
            }else{
                $address = MemberAddress::create($add_rows);
            }
            $arr['address_id'] = $address->address_id;

            //商品处理
            $spu = Spu::where('spu_num',$arr['good_num'])->find();

            if($spu['spu_param'] && !$arr['param'])
                return __error('缺少规格参数',$info);
            $rule = SpuRule::where(['spu_num'=>$arr['good_num'],])->find();
            if(!$rule)
                return __error('找不到车队规则',$info);
            if(!$spu){
                $spu = Spu::find($rule['spu_id']);
                if(!$spu)
                    return __error('找不到对应SPU',$info);
            }


            $order_detail = $sku_id = [];
            if($arr['param']){
                $arr['param'] = str_replace('：',':',$arr['param']);
                $arr['param'] = str_replace('，',',',$arr['param']);
                $param = explode('|',$arr['param']);
                $good_quantity = 0;
                foreach($param as $v){
                    $array = explode(':',$v);
                    if(count($array)!=2)
                        return __error('规格参数错误',$info);
//                    $param_pinyin = toPinYin($array[0]);
                    $quantity = $array[1];
                    $good_quantity += $array[1];
                    if(strpos($array[0],',')!==false){
                        $where_arr = explode(',',$array[0]);
                        $where = [];
                        foreach ($where_arr as $val){
                            $where[] = ['sku_param','like',"%$val%"];
                        }
                        $sku = Products::where('spu_id',$spu->spu_id)->where($where)->find();
                    }else{
                        $sku = Products::where('spu_id',$spu->spu_id)->where([['sku_param','like',"%$array[0]%"]])->find();
                    }
                    if(!$sku)
                        return __error('找不到对应SKU',$info);
                    if(!in_array($sku->product_id,$sku_id)){
                        $sku_id[] = $sku->product_id;
                        $order_detail[] = [
                            'sku_id'=>$sku->product_id,
                            'quantity'=>$quantity,
                            'spu_id'=>$spu->spu_id,
                            'wh_id'=>$rule['wh_id'],
                            'deliver_id'=>$rule['deliver_id'],
                            'spu_num'=>$spu['spu_num'],
                            'price'=>$sku['price'],
                        ];
                    }
                }
                if($arr['quantity']!=$good_quantity)
                    return __error('订单数量与规格数量不一致',$info);
            }else{
                $sku = Products::where('spu_id',$spu->spu_id)->find();
                $sku_id[] = $sku->product_id;
                $order_detail[] = [
                    'sku_id'=>$sku->product_id,
                    'quantity'=>$arr['quantity'],
                    'spu_id'=>$spu->spu_id,
                    'wh_id'=>$rule['wh_id'],
                    'deliver_id'=>$rule['deliver_id'],
                    'spu_num'=>$spu['spu_num'],
                    'price'=>$sku['price'],
                ];
            }

            $arr['ord_num'] = $arr['ord_num']?:set_ord_num();
            $arr['spu_id'] = $spu->spu_id;
            $arr['sku_id'] = implode(',',array_unique($sku_id));
            $arr['deliver_id'] = $rule['deliver_id'];
            $arr['wh_id'] = $rule['wh_id'];

//            halt($arr);
            //保存订单
            $order = OrderModel::where('ord_num',$arr['ord_num'])->find();
            if(!$order){
                $order = OrderModel::create(save_common_field($arr));
                $ord_id = $order->ord_id;
            }else{
                OrderModel::update(save_common_field($arr,1),['ord_id'=>$order->ord_id]);
                $ord_id = $order->ord_id;
            }

            //订单明细处理
            foreach($order_detail as $k=>$v){
                $v['ord_id'] = $order->ord_id;
                $exist = OrdDetail::where('ord_id',$order->ord_id)->where('sku_id',$v['sku_id'])->find();
                if($exist){
                    OrdDetail::update($v,['detail_id'=>$exist->detail_id]);
                }else{
                    OrdDetail::create($v);
                }
            }
            $arr['ord_id'] = $ord_id;
            $arr['status_text'] = '待出库';
            $arr['good_count'] = $arr['quantity'];
            $arr['package_num'] = $spu['package_num']>1?ceil($arr['quantity']/$spu['package_num']):$arr['quantity'];
            $exist = Waybill::where('ord_num',$arr['ord_num'])->find();
            if($exist){
                $arr['waybill_num'] = $exist->waybill_num?:$arr['waybill_num'];
                $exist->save($arr,['waybill_id'=>$exist['waybill_id']]);
            }else{
                Waybill::create($arr);
            }

            OrderModel::commit();
//            dump($arr);
            $msg = $cover?'覆盖':'导入';
            return __successData($arr['ord_num'].$msg.'成功',$info);
        }else{
            return $this->fetch();
        }

    }

    /**
     * 导出订单表
     */
    public function export(Request $request)
    {
        set_time_limit(0);

        $search = $request->param();

        $where = [
//            ['order.is_deleted','=',0]
        ];
        $whereSql = '';
        $where = get_query_condition($where,$search,$this->model->getTable());

        if(session('user.spu_ids'))
            $where[] = ['spu.spu_id','in',session('user.spu_ids')];

        $field = [
            'order.ord_id',
            'order.ord_num',
            'ord_type',
            'route.route_name ord_route',
            'group_concat(DISTINCT waybill.waybill_num) waybill_num',
            "FROM_UNIXTIME(ord_at,'%Y-%m-%d') ord_date",
            "FROM_UNIXTIME(ord_at,'%H:%i:%s') ord_time",
            'status_text',
            'order.is_finance',
            'is_trade',
            'ord_detail.spu_num',
            'group_concat(DISTINCT spu_name) spu_name',
            'spu_name product_info',
            'order.quantity',
            'member_name',
            'member_phone',
            'member_tel',
            'member_address.province',
            'member_address.city',
            'member_address.county',
            'member_address.street',
            'detail',
            'ord_money',
            'order.collection_payment',
            'member_name',
//            'order.out_instruction',
//            'is_cancel',
            'username',
            'GROUP_CONCAT(concat_ws("-",spu_param,sku_param,ord_detail.quantity) SEPARATOR "|") product_detail',
//            'GROUP_CONCAT(sku_param,"|",ord_detail.quantity) sku_param',
//                "FROM_UNIXTIME(ord_at,'%Y-%m-%d %H:%i:%s') ord_at",
//                "FROM_UNIXTIME(order.create_at,'%Y-%m-%d %H:%i:%s') create_at",
//                "FROM_UNIXTIME(order.update_at,'%Y-%m-%d %H:%i:%s') update_at",
        ];

        $order = OrderModel::field($field)->leftJoin('waybill','waybill.ord_id = order.ord_id')
            ->leftJoin('route','route.id = order.ord_route')
//            ->leftJoin('spu','spu.spu_id = products.spu_id')
            ->leftJoin('balance','balance.ord_id = order.ord_id')
            ->leftJoin('system_user','system_user.id = order.update_by')
            ->leftJoin('member','member.member_id = order.member_id')
            ->leftJoin('member_address','member_address.address_id = order.address_id')
            ->leftJoin('ord_detail','ord_detail.ord_id = order.ord_id')
            ->leftJoin('products','products.product_id = ord_detail.sku_id')
            ->leftJoin('spu','spu.spu_id = products.spu_id')
            ->where($where)
            ->where($whereSql)
            ->order('order.ord_at','desc')
            ->group('order.ord_id')
//            ->page($page,$limit)
//                ->fetchSql()
            ->select()
            ->each(function($val){
                $val['ord_num'] = "\t$val[ord_num]";
                $val['is_finance'] = $val['is_finance']==1?'是':'否';
//                $val['is_trade'] = $val['is_trade']==1?'是':'否';
                $val['ord_type'] = getEnum($val['ord_type'],'ord_type');
                $val['status_text'] = $val['status_text']?$val['status_text']:'待出库';
//                $val['ord_route'] = getEnum($val['ord_route'],'ord_route','无');
                $val['member_phone'] = $val['member_phone']?hidePhone($val['member_phone']):'';
                $val['member_tel'] = $val['member_tel']?hidePhone($val['member_tel']):'';
                $val['province'] = getAddrName($val['province']);
                $val['city'] = getAddrName($val['city']);
                $val['county'] = getAddrName($val['county']);
                $val['street'] = getAddrName($val['street']);
//                $val['out_instruction'] = getEnum($val['out_instruction'],'out_instruction');
//                $val['is_cancel'] = getEnum($val['is_cancel'],'is_cancel','无');

                $product_detail = [];
                $val['product_detail'] = explode('|',$val['product_detail']);
                foreach ($val['product_detail'] as $v){
                    $v = explode('-',$v);
                    if(count($v)!=3)
                        continue;
                    $v[0] = json_decode($v[0],true);
                    $v[1] = json_decode($v[1],true);

                    foreach ($v[0] as $vv){
                        $product_detail[] = $v[1][toPinYin($vv['name'])].'*'.$v[2];
                    }
                }
                $val['product_info'] = implode('|',$product_detail);
                unset($val['product_detail']);
//                halt($val);
            })
            ->toArray()
        ;
//            dd($order);die;
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=订单明细.csv' );
        header('Cache-Control: max-age=0');

        $title = [
            '订单ID','订单号','订单类型','通路','运单号','订单日期','进线时间','运单状态','代收货款结算','运费结算','商品编码','商品名','商品明细','数量','客户','手机','固话',
            '省','市','区','街道','详细地址','订单金额','代收货款金额','下单人',
        ];
        put_csv($order,$title);
    }



    public function read($id)
    {
        $row = OrderModel::find($id)->toArray();

        $waybill = Waybill::where('ord_id',$row['ord_id'])->find();

//        dump($row);
        $row['is_finance'] = $row['is_finance']==1?'是':'否';

        $spu_num = $spu_name = [];

        $spu = Spu::where('spu_id','in',$row['spu_id'])->select();

        foreach ($spu as $v){
            $spu_num[] = $v['spu_num'];
            $spu_name[] = $v['spu_name'];
        }

        $product = Products::leftJoin('ord_detail',"ord_detail.sku_id = products.product_id")
            ->leftJoin('spu','spu.spu_id = products.spu_id')
            ->field('products.*,quantity,spu_name')
            ->where('product_id','in',$row['sku_id'])
            ->where('ord_detail.ord_id',$row['ord_id'])
            ->select()->toArray()
        ;

        foreach ($product as &$v){
            $v['param'] = $v['spu_name'];
            if($v['sku_param']){
                foreach ($v['sku_param'] as $val){
                    $v['param'] .= $val;
                }
            }

        }

//        halt($product);
        $member = Member::find($row['member_id']);
        $member['member_phone'] = hidePhone($member['member_phone']);
        $member['member_tel'] = hidePhone($member['member_tel']);

        $address = MemberAddress::find($row['address_id']);

        $addr = getAddrName($address['province'])
            .getAddrName($address['city'])
            .getAddrName($address['county'])
            .getAddrName($address['street'])
            .$address['detail'];

        $var = [
            'ord'=>$row,
            'waybill'=>$waybill,
            'spu_name'=>implode('，',$spu_name),
            'spu_num'=>implode('，',$spu_num),
            'product'=>$product,
            'member'=>$member,
            'addr'=>$addr,
        ];

        return $this->fetch('',$var);
    }

    //手动推送订单到车队
    public function waybill_order()
    {
        if($this->request->isAjax()){
            $id = $this->request->post('id');
            if(!$id)
                return __error('订单信息不存在');

            return bookCharge($id);
        }
    }

    //更新地址信息
    public function edit($id='')
    {
        if($this->request->isAjax()){
            $param = $this->request->param();
//            $validate = $this->validate($param,'app\admin\validate\Member.edit');
//            if (true !== $validate) return __error($validate);

//            dd($param);die;

//            $r = OrderModel::update($param);

            Member::update($param,['member_id'=>$param['member_id']]);
            MemberAddress::update($param,['address_id'=>$param['address_id']]);
            $addr = getAddrName($param['province']).getAddrName($param['city']).getAddrName($param['county']).getAddrName($param['street']).$param['detail'];
            OrderModel::update(
                ['addr'=>$addr],
                ['ord_id'=>$param['ord_id']]
            );
            Waybill::update(['reciever_addr'=>$addr],['ord_id'=>$param['ord_id']]);

            return __success('更新成功');

        }

        $order = OrderModel::get($id);
        $member = Member::find($order['member_id']);
        $address = MemberAddress::find($order['address_id']);

        $var = [
            'order'=>$order,
            'member'=>$member,
            'address'=>$address,
            'province'=>SystemAddress::where('pid',0)->orderRaw("field(`name`,'广东省') desc")->select(),
            'city'=>SystemAddress::where('pid',$address['province'])->select(),
            'county'=>SystemAddress::where('pid',$address['city'])->select(),
            'street'=>SystemAddress::where('pid',$address['county'])->select(),
        ];

        return $this->fetch('form',$var);
    }

    public function make_order(Request $request)
    {
        if($request->isAjax()){
//            halt($request->post());
            $post = $request->post();
            $ord_type = 1;

            if(empty($post['phone']) && empty($post['tel']))
                return __error('请填写联系方式');

            if($post['phone'] && !checkPhone($post['phone']))
                return __error('手机号码错误');

            if($post['tel'] && !checkPhone($post['tel']))
                return __error('固话号码错误');

//            if(empty($post['deliver_id']))
//                return __error('没有车队信息');

            if(empty($post['spu_id']))
                return __error('请选择商品');

            if(empty($post['sku']) && empty($post['quantity']) && (empty($post['quantity']) || $post['quantity']==0))
                return __error('请输入商品数量');

            if(!empty($post['is_change'])&&!$post['org_ord'])
                return __error('请输入原订单号');

            if(empty($post['ord_route']))
                return __error('请选择通路');

            if($post['ord_route']==6)
                return __error('有赞通路下单已关闭');

            $org_ord = isset($post['org_ord'])&&$post['org_ord']?OrderModel::where('ord_num',$post['org_ord'])->find():'';
//            if(!$org_ord)
//                return __error('没有找到原订单号信息');


            //用户信息处理
            $member = [
                'member_name'=>$post['receiver'],
                'member_phone'=>$post['phone'],
                'member_tel'=>$post['tel'],
            ];

            $res_member = Member::where('member_phone',$post['phone'])->where('member_tel',$post['tel'])->find();
            if(!$res_member){
                $res_member = Member::create($member);
            }else{
                Member::update($member,['member_id'=>$res_member['member_id']]);
            }

//            halt($res_member);
            //用户地址信息处理
            $address = [
                'member_id'=>$res_member->member_id,
                'province'=>$post['province'],
                'city'=>$post['city'],
                'county'=>$post['county'],
                'street'=>$post['street'],
                'detail'=>$post['detail'],
            ];
            $addr_where = [];
            foreach ($address as $k=>$v){
                $addr_where[] = [$k,'=',$v];
            }
            $res_addr = MemberAddress::where($addr_where)->find();
            if(!$res_addr){
                $res_addr = MemberAddress::create($address);
            }else{
                $res_addr->hits = $res_addr->hits + 1;
                $res_addr->save();
            }

            Member::startTrans();

            //订单，运单处理
            $spu = Spu::find($post['spu_id']);
            if(!$spu)
                return __error('找不到SPU');
            if(!$spu['on_sale'])
                return __error('商品已下架商品');

            $rule = SpuRule::where(['spu_id'=>$post['spu_id'],'route_id'=>$post['ord_route']])->find();
            if(!$rule)
                return __error('找不到车队规则，请配置');
//            $wh_ids = explode(',',$spu['wh_id']);
//            if(count($wh_ids)>1){
//                $deliver_info = DeliverSpeciel::getDeliverInfo($post['ord_route'],$post['spu_id']);
//                if(!$deliver_info)
//                    return __error('商品有多个仓库,未找到车队,请设置车队匹配');
//                if(!in_array($deliver_info['deliver_id'],$wh_ids))
//                    return __error('车队匹配错误');
//                $deliver_id = $deliver_info['deliver_id'];
//                $wh_id = $deliver_info['wh_id'];
//            }else{
//                $wh_id = $spu['wh_id'];
//                $deliver_id = Warehouse::where('wh_id',$spu['wh_id'])->value('org_id');
//            }

            $param = $sku_id = $ord_detail = [];
            $ord_money = $quantity = 0;
            if(isset($post['sku'])){
                $sku = $post['sku'];
                foreach($sku as $k=>$v){
                    $exist = Products::find($v['sku_id']);
                    if(!$exist)
                        continue;
                    if(!$v['quantity'])
                        return __error('商品没有填写数量,请修改');
                    if(in_array($v['sku_id'],$sku_id))
                        return __error('已选择相同的sku,请修改');
                    if($exist['stock']<=0 || $v['quantity']>$exist['stock'])
                        $ord_type = 3;//预售单
//                        return __error('存在库存不足商品,请增加库存');
                    $sku_id[] = $v['sku_id'];
                    $ord_money += $exist['price']*$v['quantity'];
                    if($exist['sku_param']){
                        $str = '';
                        foreach ($exist['sku_param'] as $key=>$val){
                            $str .= $val;
                        }
                        $ord_detail[] = [
                            'spu_id'=>$post['spu_id'],
                            'sku_id'=>$v['sku_id'],
                            'quantity'=>$v['quantity'],
                            'spu_num'=>$spu['spu_num'],
                            'price'=>$exist['price'],
                        ];
                        $quantity += $v['quantity'];
                        $param[] = "$str*$v[quantity]";
                    }

                }
            }else{
                $row = Products::where('spu_id',$post['spu_id'])->find()->toArray();
                if($row['stock']<=0 || $post['quantity']>$row['stock'])
                    $ord_type = 3;//预售单
//                    return __error('商品库存不足,请增加库存');
                $quantity = $post['quantity'];
                $ord_money = $post['quantity']*$row['price'];
                $sku_id[] = $row['product_id'];
                $ord_detail[] = [
                    'spu_id'=>$post['spu_id'],
                    'sku_id'=>$row['product_id'],
                    'quantity'=>$post['quantity'],
                    'spu_num'=>$spu['spu_num'],
                    'price'=>$row['price'],
                ];
            }

            if($spu['buy_limit'] && $quantity > $spu['buy_limit'])
                return __error('此商品每单最多只能购买'.$spu['buy_limit'].'个');

            if($spu['buy_limit'] && $quantity < $spu['buy_limit'])
                return __error('此商品每单至少购买'.$spu['buy_limit'].'个');

            //绑定节目
            $date = date('Y-m-d');
            $concat = "concat('$date ',present_at)";
            $program_where = time()." between program_start and program_end";//有效期内
            $program_where .= ' && '.time()." between UNIX_TIMESTAMP($concat) and UNIX_TIMESTAMP($concat)+1800";//播出时间内
            $program_where .= ' && '."find_in_set($post[spu_id],product_id)";
            $program_id = \app\common\model\Program::where($program_where)->value('program_id');

            //完整地址
            $addr = getAddrName($post['province']).getAddrName($post['city']).getAddrName($post['county']).getAddrName($post['street']).$post['detail'];

            //生成订单
            $order = [
                    'ord_num'=>in_array(16,session('user.auth_id'))&&$post['ord_route']==6
                        ?$post['ord_num']
                        :set_ord_num(),
                    'member_id'=>$res_member->member_id,
                    'address_id'=>$res_addr->address_id,
                    'spu_id'=>$post['spu_id'],
                    'sku_id'=>implode(',',$sku_id),
                    'ord_money'=>in_array(16,session('user.auth_id'))&&$post['ord_route']==6
                        ?$post['ord_money']
                        :(!empty($post['is_change'])?0:$ord_money),
                    'collection_payment'=>!empty($post['is_change'])?0:$ord_money,
                    'is_change'=>!empty($post['is_change'])?$post['is_change']:0,
                    'ord_type'=>!empty($post['is_change'])&&$post['is_change']==1?2:$ord_type,//换货单
                    'org_ord'=>isset($post['org_ord'])?$post['org_ord']:'',
                    'quantity'=>$quantity,
                    'package_num'=>$spu['package_num']>1?ceil($quantity/$spu['package_num']):$quantity,
                    'remark'=>$post['remark'],
                    'ord_at'=>time(),
//                    'deliver_id'=>$post['deliver_id'],
                    'ord_route'=>$post['ord_route'],
                    'program_id'=>$program_id,
                    'connection_id'=>$post['connection_id']?:'',
                    'addr'=>$addr,
                    'receiver'=>$post['receiver'],
                    'phone'=>$post['phone'],
                    'tel'=>$post['tel'],
                    'order_by'=>session('user.id'),
                    'pay_mode'=>!empty($post['pay_mode'])?$post['pay_mode']:1,
                ];
            $order['out_trade_no'] = $order['pay_mode']==2?$order['ord_num']:'';
            $order['collection_payment'] = in_array(16,session('user.auth_id'))&&$post['ord_route']==6
                ?$post['collection_payment']
                :($order['ord_route']==6?0:$order['collection_payment']);
            $order['collection_payment'] = $order['pay_mode']==1?$order['collection_payment']:0;
//            halt($order);
            $res_ord = OrderModel::create(save_common_field($order));//保存订单

            //订单商品明细
            if(count($ord_detail)>0){
                foreach ($ord_detail as $k=>$v){
                    $ord_detail[$k]['ord_id'] = $res_ord->ord_id;
                }
                $res_ord_detail = OrdDetail::insertAll($ord_detail);
            }

            if($ord_type==1){//正常单才会走运单流程
                //库存操作
                foreach($ord_detail as $v){
                    Products::where('product_id',$v['sku_id'])->setDec('stock',$v['quantity']);
                }
            }

            //运单处理
            if(isset($post['is_change'])&&$post['is_change']){
                $org_waybill = isset($post['org_ord'])&&$post['org_ord']?Waybill::where('ord_num',$post['org_ord'])->value('waybill_num'):'';
//                if(!$org_waybill)
//                    return __error('没有找到原运单号信息');
            }

            $waybill = [
                'ord_id'=>$res_ord->ord_id,
                'orderId'=>$order['ord_num'],
                'ord_num'=>$order['ord_num'],
                'org_waybill'=>isset($org_waybill)?$org_waybill:'',
                'spu_id'=>$post['spu_id'],
                'sku_id'=>implode(',',$sku_id),
                'deliver_id'=>$rule['deliver_id'],
                'wh_id'=>$rule['wh_id'],
                'reciever'=>$post['receiver'],
                'reciever_phone'=>$post['phone'],
                'reciever_tel'=>$post['tel'],
                'receiveTel'=>$post['tel'],
                'reciever_addr'=>$addr,
                'package_num'=>$spu['package_num']>1?ceil($quantity/$spu['package_num']):$quantity,//包裹数量
//                'salePlat'=>$deliver['jd_sale_plat'],
//                'customerCode'=>$deliver['jd_customer_code'],
//                'senderName'=>$wh['wh_person'],
//                'senderAddress'=>$wh['wh_address'],
                'product_name'=>$spu['spu_name'],
//                'senderMobile'=>$wh['wh_phone'],
                'addr'=>$address,
                'phone'=>$post['phone'],
                'quantity'=>$quantity,//商品数量
                'weight'=>$spu['weight'],
                'product_length'=>$spu['spu_length'],
                'product_width'=>$spu['spu_width'],
                'product_height'=>$spu['spu_height'],
                'collection_payment'=>$order['ord_money'],
//                        'parcel_insurance'=>1,//是否保价
//                        'insurance_prc'=>300,//保价金额
//                        'signReturn'=>1,//签还单
                'param'=>implode('|',$param),//商品规格
//                'ware_house_code'=>$deliver['jd_ware_house_code'],
                'status_text'=>'待出库',
                'good_count'=>$quantity,
//                'is_finance'=>!empty($post['is_change'])&&$post['is_change']==1?1:0,//如果是换货单，代收货款默认已结算
            ];
//                halt($waybill);
            Waybill::create(save_common_field($waybill));
//                dump($request->domain());
//                dump(config('domain'));
//                halt($request->domain() == config('domain'));


            Member::commit();
            if($order['pay_mode']==2){
                $wx_order = (new WechatPaymentService())->order([
                    'body'=>$spu['spu_name'].($param?implode('|',$param):$quantity),
                    'out_trade_no'=>$order['out_trade_no'],
                    'product_id'=>$post['spu_id'],
                    'total_fee'=>$order['ord_money']*100,
                    'trade_type'=>'NATIVE',
                ]);
                if($wx_order['return_code']=='FAIL')
                    return __error('下单成功，生成付款二维码失败：'.$wx_order['return_msg']);
                if($wx_order['result_code']=='FAIL')
                    return __error('下单成功，生成付款二维码失败：'.$wx_order['result_msg']);
                $qrCode = new QrCode($wx_order['code_url']);
                cache($order['out_trade_no'],$qrCode->writeDataUri(),7200);
                return __success('下单成功',['wxpay_qrcode'=>$qrCode->writeDataUri()]);
            }
            return __success('下单成功');
        }
        $var = [
            'province'=>SystemAddress::where('pid',0)->orderRaw("field(`name`,'广东省') desc")->select(),
            'spu'=>\app\common\model\Spu::field('spu_id,spu_name')
                ->where('on_sale',1)
                ->where('is_deleted',0)
                ->where('spu_type',1)
                ->select(),
            'route'=>Route::where(['is_deleted'=>0,'status'=>1])->select(),
        ];

        return $this->fetch('order',$var);
    }

    public function cancel(Request $request)
    {
        $ord_id = $request->post('id');
        if(!$ord_id)
            return __error('缺少订单ID');
        $ord = OrderModel::find($ord_id);
        if(!$ord)
            return __error('没有订单信息');
        if($ord['ord_type']==4)
            return __error('此订单已取消');
//        if($ord['out_instruction']==2)
//            return __error('出库完成的订单不能取消');

        $row = [
            'is_deleted'=>1,
            'delete_at'=>time(),
            'delete_by'=>session('user.id'),
            'ord_type'=>4,
//            'out_instruction'=>-1,
            'ord_money'=>0,
        ];

        $cancel = 0;

        $waybill = Waybill::where('ord_id',$ord_id)->whereNotNull('waybill_num')->select();
        if(count($waybill)>0){
            $row['ord_type'] = 6;
            //运单拦截/取消
            foreach($waybill as $v){
                if($v['status']==2 || $v['status']==-2)
                    continue;
                $r = (new JdService())->cancel($v['waybill_num'],'订单取消');
                if($r['statusCode']==0){
                    $update_row = [
                        'cancel_reason'=>session('user.username').'取消',
                        'is_cancel'=>1,
                        'cancel_at'=>time(),
                        'out_instruction'=>-1,
                    ];
                }else{
                    $r = (new JdService())->intercept($v['waybill_num'],'订单拦截');
                    if($r['stateCode']==100){
                        $update_row = [
                            'cancel_reason'=>session('user.username').'拦截',
                            'is_cancel'=>1,
                            'cancel_at'=>time(),
//                            'out_instruction'=>-1,
                        ];
                    }else{
                        $update_row = [
                            'is_cancel'=>0,
                        ];
                        $cancel++;
                    }
                }

                Waybill::update($update_row,['waybill_id'=>$v['waybill_id']]);
            }
        }
//        OrderModel::startTrans();

        $r = OrderModel::update($row,['ord_id'=>$ord_id]);
        if($r){
            $ord_detail = OrdDetail::where('ord_id',$ord_id)->select();
            //库存操作
            foreach($ord_detail as $v){
                Products::where('product_id',$v['sku_id'])->setInc('stock',$v['quantity']);
            }
//            OrderModel::commit();
            return __success('取消成功'.($cancel?'但有运单不能通过接口拦截，请手动拦截':''));
        }else{
//            OrderModel::rollback();
            return __error('取消失败');
        }
    }

    public function wxpay_qrcode($id)
    {
        $order = OrderModel::find($id);
        if(!$order)
            return __error('没有订单信息');
        if(WxpayLog::where('ord_id',$id)->value('result_code')=='SUCCESS')
            return __error('该订单已付款');
        $qrcode = cache($order->out_trade_no);
        if(!$qrcode){
            $order_detail = OrdDetail::with(['spu','sku'])->where('ord_id',$id)->select()->toArray();
            $body = [];
            foreach ($order_detail as $v){
                $body[] = $v['spu_name'].(!empty($v['sku_param'])?implode('',$v['sku_param']):'').'*'.$v['quantity'];
            }
            $out_trade_no = strpos($order->out_trade_no,'_')!==false?explode('_',$order->out_trade_no):$order->out_trade_no;
            $out_trade_no = is_array($out_trade_no)?$out_trade_no[0].'_'.($out_trade_no[1]+1):$out_trade_no.'_1';
            $wx_order = (new WechatPaymentService())->order([
                'body'=>implode(',',$body),
                'out_trade_no'=>$out_trade_no,
                'product_id'=>implode(',',array_column($order_detail,'spu_id')),
                'total_fee'=>$order['ord_money']*100,
                'trade_type'=>'NATIVE',
            ]);
            if($wx_order['return_code']=='FAIL')
                exit(alert($wx_order['return_msg'],'',3,2));
            if($wx_order['result_code']=='FAIL')
                exit(alert($wx_order['result_msg'],'',3,2));
            $qrCode = new QrCode($wx_order['code_url']);
            cache($out_trade_no,$qrCode->writeDataUri(),7200);
            $order->out_trade_no = $out_trade_no;
            $order->save();
            $qrcode = $qrCode->writeDataUri();
//            return __success('success',['wxpay_qrcode'=>$qrCode->writeDataUri()]);
        }
        exit('<div style="margin: 10px auto;width: 250px;"><img src="'.$qrcode.'" width="250"><p style="color: red;text-align: center;">二维码2小时内有效，请尽快支付</p></div>');
    }
}
