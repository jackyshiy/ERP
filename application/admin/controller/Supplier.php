<?php

namespace app\admin\controller;

use app\common\controller\AdminController;
use think\Controller;
use think\Request;
use app\common\model\User;
use app\common\model\Spu;

class Supplier extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->model = new \app\common\model\Supplier();
    }

    protected function __list($model,$where=[])
    {
        return $model->withJoin('user');
    }

    protected function __beforeSave($action)
    {
        $param = $this->request->param();
        switch ($action){
            case 'create':
                break;
            case 'update':
                break;
        }

        \app\common\model\Supplier::update(['user_id'=>0],['user_id'=>$param['user_id']]);
        return $param;
    }

    protected function __beforeDisplay($action,$data=[])
    {
        $var = [
            'index'=>[
                ''
            ],
            'form'=>[
                'user'=>User::where('status',1)->select(),
            ],
        ];

        return isset($var[$action])?$var[$action]:[];
    }

    protected function __beforeDelete($id)
    {
        $spu = Spu::whereIn('supplier_id',$id)->where(['is_deleted'=>0,'on_sale'=>1])->find();
        if($spu)
            is_array($id)&&count($id)?throw_error('存在商品销售中的供应商，请检查'):throw_error('该供应商下有商品在销售，请先删除');
    }
}
