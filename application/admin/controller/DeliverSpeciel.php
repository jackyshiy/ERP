<?php

namespace app\admin\controller;

use app\common\controller\AdminController;
use app\common\model\OrgModel;
use think\Controller;
use think\Request;
use app\common\model\User;
use app\common\model\Spu;
use app\common\model\Route;
use app\common\model\Warehouse;

class DeliverSpeciel extends AdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->model = new \app\common\model\DeliverSpeciel();
    }

    /**
     * 列表额外处理
     *
     * @param $model
     * @param array $where
     * @return mixed
     */
    protected function __list($model,$where=[])
    {
        return $model->withJoin(['spu','route','deliver'],'left');
    }

    protected function __each($rows)
    {
        return $rows->each(function($v){
            $v['wh_name'] = Warehouse::leftJoin('spu','spu.spu_id = warehouse.wh_id')->where('spu_id',$v['spu_id'])->value('wh_name');
        });
    }

    /**
     * 添加/修改前处理
     *
     * @param string $action
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    protected function __beforeSave($action)
    {
        $param = $this->request->param();
        $where = [
            'route_id'=>$param['route_id'],
//            'wh_id'=>$param['wh_id'],
            'spu_id'=>$param['spu_id'],
            'deliver_id'=>$param['deliver_id'],
            'is_deleted'=>0
        ];
        switch ($action){
            case 'create':
                $divide = $this->model->where($where)->find();
                if($divide)
                    throw_error('分配条件已存在');
                break;
            case 'update':
                $divide = $this->model->where($where)->where('id','<>',$param['id'])->find();
                if($divide)
                    throw_error('分配条件已存在');
                break;
        }

//        $spu = Spu::find($param['spu_id']);
//        if(count(explode(',',$spu['wh_id']))==1)
//            throw_error('商品只有一个仓库，不需要分配');

//        $param['deliver_id'] = Warehouse::where('wh_id',$param['wh_id'])->value('org_id');
        return $param;
    }

    /**
     * 渲染模板前处理
     *
     * @param string $action
     * @return array|mixed|void
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    protected function __beforeDisplay($action,$data=[])
    {
        $param = $this->request->param();
        $var = [
            'index'=>[
                ''
            ],
            'form'=>[
                'route'=>Route::where('is_deleted',0)->select(),
                'spu'=>Spu::leftJoin('deliver_speciel','find_in_set(spu.spu_id,deliver_speciel.spu_id)')
                    ->field('spu.spu_id,spu_num,spu_name,deliver_speciel.spu_id choose')
                    ->where(['spu.is_deleted'=>0,'on_sale'=>1])
//                    ->whereNull('deliver_speciel.spu_id')
                    ->select(),
//                'org'=>OrgModel::where('is_deleted',0)->select(),
                'deliver'=>OrgModel::where('is_deleted',0)->select(),
            ],
        ];

        //仓库
//        if(!empty($param['id'])){
//            $spu_id = $this->model->where('id',$param['id'])->value('spu_id');
//            $var['form']['wh'] = Warehouse::field('warehouse.wh_id,wh_name')
//                ->leftJoin('spu',"find_in_set(warehouse.wh_id,spu.wh_id)")
//                ->where(['warehouse.is_deleted'=>0])->where('spu_id',$spu_id)
//                ->select();
//        }


        return isset($var[$action])?$var[$action]:[];
    }

    /**
     * 删除前处理
     */
    protected function __beforeDelete($id)
    {

    }


}
