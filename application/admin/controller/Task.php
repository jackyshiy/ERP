<?php

namespace app\admin\controller;

use app\common\model\CurlLog;
use app\common\model\DeliverSpeciel;
use app\common\model\MemberAddress;
use app\common\model\OrdDetail;
use app\common\model\OrgModel;
use \app\common\model\Order;
use app\common\model\Spu;
use app\common\model\SpuRule;
use app\common\model\Traffic as TrafficModel;
use app\common\model\Warehouse;
use app\common\service\JdService;
use app\common\service\YouZanService;
use think\Controller;
use think\Log;
use think\Request;
use app\common\model\Waybill;
use app\common\model\Products;
use app\common\model\Member;
use app\common\model\Route;

class Task extends Controller
{

    protected $waybill;

    public function __construct()
    {
        parent::__construct();
        $this->waybill = new Waybill();
    }
    /**
     * 更新物流状态
     *
     * @return \think\Response
     */
    public function trade_update()
    {
        set_time_limit(0);
        $debug = $this->request->get('debug');
        $waybill_num = $this->request->get('waybill_num');
        $where = [
            ['status','in',[0,1]],
            ['deliver_id','=',1],
            ['out_instruction','>',0],
            ['waybill_num','<>',''],
//            ['trade_failed','<',5],
        ];
        if($waybill_num)
            $where[] = ['waybill_num','=',$waybill_num];

        $waybill = Waybill::field('waybill_num,deliver_id,waybill_id,ord_id')
            ->where($where)
            ->whereNotNull('waybill_num')
            ->limit(500)
            ->order('waybill_at desc')
            ->select()
        ;
        if($debug)
            dump($waybill);
//        Waybill::startTrans();
        foreach($waybill as $v){
            $trade = (new JdService())->trade($v['waybill_num']);
            if($debug)
                dump($trade);
            if(empty($trade)){
                Waybill::where('waybill_id',$v->waybill_id)->setInc('trade_failed');
                continue;
            }

            foreach ($trade as $val){
                if(!in_array($val['opeTitle'],config('waybill_end')))
                    continue;

                $ord_detail = OrdDetail::where('ord_id',$v->ord_id)->select();

                switch ($val['opeTitle']){
                    case '拒收':
                        $row = [
                            'status'=>-1,
                            'status_text'=>$val['opeTitle'],
                            'trade_at'=>strtotime($val['opeTime'])
                        ];
                        $this->stock($ord_detail);
                        break;
                    case '换单打印':
                        $row = [
                            'status'=>-1,
                            'status_text'=>'拒收',
                            'trade_at'=>strtotime($val['opeTime'])
                        ];
                        $this->stock($ord_detail);
                        break;
                    case '退货运单打印':
                        $row = [
                            'status'=>-1,
                            'status_text'=>'拒收',
                            'trade_at'=>strtotime($val['opeTime'])
                        ];
//                        foreach ($ord_detail as $detail_v){
//                            Products::where('product_id',$detail_v->sku_id)->setInc('back_stock',$detail_v->quantity);
//                        }
                        $this->stock($ord_detail);
                        break;
                    case '妥投':
                        $row = [
                            'status'=>2,
                            'status_text'=>$val['opeTitle'],
                            'trade_at'=>strtotime($val['opeTime'])
                        ];
//                        dump($row);
                        break;
                    case '下单取消':
                        $row = [
                            'status'=>-2,
                            'status_text'=>'已取消',
                            'trade_at'=>strtotime($val['opeTime'])
                        ];
                        break;
                    case '终止揽收':
                        $row = [
                            'status'=>-2,
                            'status_text'=>$val['opeTitle'],
                            'trade_at'=>strtotime($val['opeTime'])
                        ];
                        break;
                    case '上门接货退货完成':
                        $row = [
                            'status'=>-2,
                            'status_text'=>$val['opeTitle'],
                            'trade_at'=>strtotime($val['opeTime'])
                        ];
                        break;
                    default:
                        break;
                }

                if($row){
                    $r = Waybill::where('waybill_id',$v->waybill_id)->update($row);
                    if($row['status']==-2){
//                        $ord_id = Waybill::where('waybill_num',$val['waybillCode'])->value('ord_id');
                        Order::update(['ord_type'=>4,'ord_money'=>0],['ord_id'=>$v->ord_id]);
                    }
                    continue 2;
                }
            }

            $current = end($trade);

            if($current['opeTitle'] == '揽件再取'|| $current['opeTitle'] == '揽收任务分配配送员'){
                $row = [
                    'status_text'=>$current['opeTitle'],
                    'status'=>0,
                    'trade_at'=>strtotime($current['opeTime'])
                ];
            }else{
                $row = [
                    'status_text'=>$current['opeTitle'],
                    'status'=>1,
                    'trade_at'=>strtotime($current['opeTime'])
                ];
            }
            if($debug)
                halt($row);
            $r = Waybill::where('waybill_id',$v['waybill_id'])->update($row);
        }
        exit('处理成功');
    }

    public function bookCharge()
    {
        set_time_limit(0);
        $book = Order::field('order.ord_id,waybill_id,waybill_num')
            ->leftJoin('waybill','waybill.ord_id = order.ord_id')
            ->where('ord_type',1)//销售单
            ->where('out_instruction',1)//分拣完成
            ->whereNull('waybill_num')
//            ->where('order.is_deleted',0)
            ->order('ord_at asc')
            ->select();
//        dump($book);
        if(!$book)
            exit;
//        dump($book);
        foreach ($book  as $v){
            if($v['waybill_num']){
                Order::update(['ord_type'=>1],['ord_id'=>$v['ord_id']]);
                continue;
            }
            $r = bookCharge($v['ord_id']);
//            dump($r);
        }
        exit('处理成功');
    }

    /**
     * 订单挂上节目ID
     *
     */
    public function setProgram()
    {
        set_time_limit(0);

        $program = \app\common\model\Program::where('is_deleted',0)
//            ->where('program_id',124)
            ->select()
            ->toArray();
//        dump($program);

        foreach ($program as $v){
            $program_start = strtotime($v['program_start']);
            $program_end = strtotime($v['program_end']);
            $s_day = date('j',$program_start);
            $e_day = date('j',$program_end);
            $t = $e_day - $s_day;
            $t = $t>0?$t:1;
//            dump($v['program_start']);
//            dump($v['program_end']);

            $timeCondition = [];
            for($i=0;$i<$t;$i++){
                $start = date('Y-m-d',$program_start+86400*$i).' '.$v['present_at'];//开始
                $end = date('Y-m-d H:i:s',strtotime($start)+($v['duration']+30)*60);
                $timeCondition[]  = "(ord_at BETWEEN ".strtotime($start)." AND ".strtotime($end).')';
            }
//            halt(implode(' OR ',$timeCondition));
//            $ord = Order::where('is_deleted',0)->where('spu_id','in',$v['product_id'])->where(implode(' OR ',$timeCondition))->select();
//            halt($ord);
            $ord = Order::where('is_deleted',0)->where('spu_id','in',$v['product_id'])->where(implode(' OR ',$timeCondition))->update(['program_id'=>$v['program_id']]);

        }

    }

    //有赞订单
    public function YZ_order($params=[])
    {
        set_time_limit(0);
        $page = $this->request->param('page',1);
        $t = $this->request->param('t',1);
        if($t == 5)
            exit;
        $youzan = new YouZanService(config('youzan.fs_tv'));
        $date = $this->request->get('date')?:date('Y-m-d');
        $time = date("Y-m-d",strtotime($date." -1 day"));
        $param = $params?:[
//            'start_created'=>date("$time 16:00:00"),
            'end_created'=>date("$date 16:00:00"),
            'status'=>'WAIT_SELLER_SEND_GOODS',//'TRADE_SUCCESS',
            'page_size'=>100,
            'page_no'=>$page,
//            'tid'=>'E20200207161121083700001',//订单号搜索
        ];
//        dump($param);
        $result = $youzan->order($param);
//        halt($result);
        if(!$result && $params)
            return __error('没有查询到待发货订单');
        if(!$result){
            sleep(5000);
            $this->redirect($this->request->url(),['t'=>++$t,'page'=>$page]);
        }

        if(isset($result['full_order_info_list']) && count($result['full_order_info_list'])>0 && $rows = $result['full_order_info_list']){
            $total = $result['total_results'];
            Member::startTrans();
            foreach($rows as $v){
                $row = $v['full_order_info'];
                $address = $row['address_info'];
                $order = $row['orders'];
                $pay_info = $row['pay_info'];
                $remark_info = $row['remark_info'];
                $order_info = $row['order_info'];
                $order_exist = Order::where('ord_num',$order_info['tid'])->find();
//                if($order_row)
//                    continue;

                //用户信息处理
                $member = [
                    'member_name'=>$address['receiver_name'],
                    'member_phone'=>$address['receiver_tel'],
                ];
                $res_member = Member::where('member_phone',$address['receiver_tel'])->find();
                if(!$res_member){
                    $res_member = Member::create($member);
                }else{
                    Member::update($member,['member_id'=>$res_member['member_id']]);
                }

                //用户地址信息处理
                $address_row = [
                    'member_id'=>$res_member->member_id,
                    'province'=>getAddrId($address['delivery_province']),
                    'city'=>getAddrId($address['delivery_city']),
                    'county'=>getAddrId($address['delivery_district']),
//                    'street'=>getAddrId($address['delivery_province']),
                    'detail'=>$address['delivery_address'],
                ];

                $addr_where = [];
                foreach ($address_row as $k=>$v){
                    $addr_where[] = [$k,'=',$v];
                }
                $res_addr = MemberAddress::where($addr_where)->find();
                if(!$res_addr){
                    $res_addr = MemberAddress::create($address_row);
                }else{
                    $res_addr->hits = $res_addr->hits + 1;
                    $res_addr->save();
                }

                //订单处理
                $ord_type = 1;
                $quantity = 0;
                $sku_id = $spu_id = $ord_detail = $ord_route = [];
                foreach ($order as $k=>$v){
                    $spuId = $v['outer_sku_id']?:$v['outer_item_id'];
                    $spu = Spu::where('spu_num',$spuId)->find();
//                    $spu = Spu::find(80);

                    if($spu && $spu['spu_type']==2){
                        $ord_type = 7;//非电购订单
                        $spu_id[] = $spu->spu_id;
                        $sku_id[] = Products::where('spu_id',$spu->spu_id)->value('sku_id');
                        $spu_param = [];
                        $yz_sku_param = json_decode($v['sku_properties_name'],true);
                        if($yz_sku_param)
                        foreach ($yz_sku_param as $val){
                            $spu_param[] = "$val[k]："."$val[v]*".$order[$k]['num'];
                        }
                        $order_row = [
                            'ord_num'=>$row['order_info']['tid'].(count($order)>1?'-'.($k+1):''),
                            'member_id'=>$res_member->member_id,
                            'address_id'=>$res_addr->address_id,
                            'spu_id'=>implode(',',array_unique($spu_id)),
                            'sku_id'=>implode(',',array_unique($sku_id)),
                            'ord_money'=>$pay_info['payment'],
                            'collection_payment'=>0,
                            'ord_route'=>10,
                            'ord_type'=>$ord_type,

                            'quantity'=>$v['num'],
                            'remark'=>$remark_info['buyer_message'].($spu_param?implode(',',$spu_param):''),
                            'ord_at'=>$order_info['created'],

                            'ord_route'=>implode(',',$ord_route),
                        ];
//                        halt($order_row);
                        $res_ord = Order::where('ord_num',$row['order_info']['tid'])->find();
                        if($res_ord){
                            Order::update(save_common_field($order_row,1),['ord_id'=>$res_ord->ord_id]);
                        }else{
                            $res_ord = Order::create(save_common_field($order_row));
                        }

                        $exist = Waybill::where('ord_id',$res_ord->ord_id)->find();
                        $waybill = [
                            'ord_id'=>$res_ord->ord_id,
                            'deliver_id'=>0,
                            'spu_id'=>implode(',',array_unique($spu_id)),
                            'sku_id'=>implode(',',array_unique($sku_id)),
                            'wh_id'=>0,
                            'ord_num'=>$order_row['ord_num'],
//                        'collection_payment'=>$order_row['ord_money'],
                            'reciever'=>$address['receiver_name'],
                            'reciever_phone'=>$address['receiver_tel'],
                            'reciever_tel'=>'',
                            'reciever_addr'=>$address['delivery_province'].$address['delivery_city'].$address['delivery_district'].$address['delivery_address'],
                            'good_count'=>$v['num'],
                            'package_num'=>1,//包裹数量
                            'status_text'=>$exist?$exist->status_text:'待出库',
                        ];

                        if($exist){
                            Waybill::update($waybill,['waybill_id'=>$exist->waybill_id]);
                        }else{
                            Waybill::create($waybill);
                        }
                        Member::commit();
                        exit($order_row['ord_num']);
                        continue;
                    }

                    $rule = SpuRule::where(['spu_num'=>$spuId,])->find();
                    if(!$rule)
                        continue;
                    $spu = Spu::where('spu_id',$rule['spu_id'])->find();
                    if(!$spu)
                        continue;

                    $ord_route[] = $rule['route_id'];
                    $spu_id[] = $spu->spu_id;

                    //规格条件
                    $where = [];
                    if($spu['spu_param']){
                        $yz_sku_param = json_decode($v['sku_properties_name'],true);
                        foreach ($yz_sku_param as $val){
                            $where[] = ['sku_param','like',"%$val[v]%"];
                            $param[] = "$val[v]*".$order[$k]['num'];
                        }
                    }

                    $sku = Products::where('spu_id',$spu->spu_id)->where($where)->find();
//                    dump($sku);
                    if(!$sku)
                        continue;

                    if(($sku['stock']<=0 || $order[$k]['num']>$sku['stock'])&&!$order_exist)
                        $ord_type = $ord_type==5?$ord_type:3;//预售单

//                    $wh_id = Spu::where('spu_id',$spu->spu_id)->value('wh_id');//仓库ID
//                    $deliver_id = DeliverSpeciel::where(['is_deleted'=>0,'route_id'=>6])
//                        ->where("find_in_set($spu->spu_id,spu_id)")->value('deliver_id');
//                    if(!$deliver_id && $params)
//                        return __error('找不到对应车队');
//                    if(!$deliver_id)
//                        return __error('找不到对应车队');
//                    $wh_ids = explode(',',$spu['wh_id']);
//                    if(count($wh_ids)>1){
//                        $deliver_info = DeliverSpeciel::getDeliverInfo(6,$spu->spu_id);
//                        if(!$deliver_info && $params)
//                            return __error($spu['spu_name'].'有多个仓库,未找到车队,请设置车队匹配');
//                        if(count($wh_ids)>1&&!in_array($deliver_info['deliver_id'],$wh_ids) && $params)
//                            return __error('车队匹配错误');
////                        $deliver_id = $deliver_info['deliver_id'];
//                        $wh_id = $deliver_info['wh_id'];
//                    }
                    $sku_id[] = $sku->product_id;
                    $quantity += $order[$k]['num'];
                    $ord_detail[] = [
                        'sku_id'=>$sku->product_id,
                        'quantity'=>$order[$k]['num'],
                        'oid'=>$order[$k]['oid'],
                        'spu_id'=>$spu->spu_id,
                        'wh_id'=>$rule['wh_id'],
                        'deliver_id'=>$rule['deliver_id'],
                        'spu_num'=>$spu['spu_num'],
                        'price'=>$sku['price'],
                    ];
                }

                if(empty($ord_detail))
                    continue;
                //生成订单
//                if(count($ord_route)>1)
//                    $ord_type = 5;
                $order_row = [
                    'ord_num'=>$row['order_info']['tid'],
                    'member_id'=>$res_member->member_id,
                    'address_id'=>$res_addr->address_id,
                    'spu_id'=>implode(',',array_unique($spu_id)),
                    'sku_id'=>implode(',',array_unique($sku_id)),
                    'ord_money'=>$pay_info['payment'],
                    'collection_payment'=>0,
//                    'is_change'=>!empty($post['is_change'])?$post['is_change']:0,
                    'ord_type'=>$ord_type,//换货单
//                    'org_ord'=>isset($post['org_ord'])?$post['org_ord']:'',
                    'quantity'=>$quantity,
                    'remark'=>$remark_info['buyer_message'],
                    'ord_at'=>$order_info['created'],
//                    'deliver_id'=>1,
                    'ord_route'=>implode(',',$ord_route),
//                    'program_id'=>$program_id,
//                    'connection_id'=>$post['connection_id']?:'',
                ];
//                halt($order_row);
                $res_ord = Order::where('ord_num',$row['order_info']['tid'])->find();
//                halt($res_ord);
                if($res_ord){
                    Order::update(save_common_field($order_row,1),['ord_id'=>$res_ord->ord_id]);
                }else{
                    $res_ord = Order::create(save_common_field($order_row));
                }

                //订单明细处理
                if(count($ord_detail)>0){
                    foreach ($ord_detail as $k=>$v){
                        $v['ord_id'] = $res_ord->ord_id;
                        $detail = OrdDetail::where('ord_id',$v['ord_id'])->where('sku_id',$v['sku_id'])->find();
                        if($detail){
                            OrdDetail::update($v,['detail_id'=>$detail->detail_id]);
                        }else{
                            OrdDetail::create($v);
                        }
                    }
//                    dump($ord_detail);
//                    $res_ord_detail = OrdDetail::insertAll($ord_detail,true);
                }

                //库存操作
                if($ord_type == 1 && !$order_exist)
                    foreach($ord_detail as $v){
                        Products::where('product_id',$v['sku_id'])->setDec('stock',$v['quantity']);
                    }

                //运单处理
                $waybill_arr = $waybill_rows = [];
                foreach ($ord_detail as $v){
                    if(!$v['deliver_id'])
                        continue 2;
                    $waybill_arr[$v['wh_id']][] = $v;
                }

                foreach ($waybill_arr as $v){
                    if(count($v)==1){
                        $waybill_rows[] = $v;
                    }elseif(count($v)>1){
                        $arr = [];
                        foreach($v as $vv){
                            $arr[$vv['deliver_id']][] = $vv;
                        }
                        $waybill_rows = array_merge($waybill_rows,$arr);
                    }else{
                        continue;
                    }
                }

                $ord_flag = count($waybill_rows)>1?1:0;
                foreach($waybill_rows as $key=>$v){
                    $spu_id = implode(',',array_unique(array_column($v,'spu_id')));
                    $sku_id = implode(',',array_unique(array_column($v,'sku_id')));
                    $waybill_quantity = $package_num = 0 ;
                    foreach ($v as $val){
                        $waybill_quantity += $val['quantity'];
                        $waybill_spu = Spu::find($val['spu_id']);
                        $package_num += $waybill_spu['package_num']>1?ceil($val['quantity']/$waybill_spu['package_num']):$val['quantity'];
                    }

//                    $org_id = Warehouse::where('wh_id',$v[0]['wh_id'])->value('org_id');
//                    if(!$org_id)
//                        continue 2;
                    $exist = Waybill::where('ord_id',$res_ord->ord_id)->where('spu_id',$spu_id)->find();
                    $waybill = [
                        'ord_id'=>$res_ord->ord_id,
                        'deliver_id'=>$v[0]['deliver_id'],
                        'spu_id'=>$spu_id,
                        'sku_id'=>$sku_id,
                        'wh_id'=>$v[0]['wh_id'],
                        'ord_num'=>$row['order_info']['tid'].($ord_flag?"-$ord_flag":''),
//                        'collection_payment'=>$order_row['ord_money'],
                        'reciever'=>$address['receiver_name'],
                        'reciever_phone'=>$address['receiver_tel'],
                        'reciever_tel'=>'',
                        'reciever_addr'=>$address['delivery_province'].$address['delivery_city'].$address['delivery_district'].$address['delivery_address'],
                        'good_count'=>$waybill_quantity,
                        'package_num'=>$package_num,//包裹数量
                        'status_text'=>$exist?$exist->status_text:'待出库',
                    ];

                    if($exist){
                        Waybill::update($waybill,['waybill_id'=>$exist->waybill_id]);
                    }else{
                        Waybill::create($waybill);
                    }
                    $ord_flag++;
                }
            }
            Member::commit();

            if($total>100 && $params)
                return __success('抓取时间段内订单达到100或以上，请抓取多次');
            if($total>100){
                $param['page_no'] = $page++;
                $this->YZ_order($param);
//                $this->redirect('/admin/task/yz_order',['page'=>++$page]);
            }
        }

        return __success('抓取完成');
    }

    public function yz_send_good()
    {
        $debug = $this->request->param('debug');
        $waybill_num = $this->request->param('num');
        $route = Route::where(['yz_send_good'=>1])->column('id');
        $where = [
            ['ord_type','=',1],
            ['yz_send','=',0],
            ['waybill.status','=',1],
        ];
        if($waybill_num)
            $where[] = ['waybill_num','=',$waybill_num];

        $waybill = Waybill::leftJoin('order','order.ord_id = waybill.ord_id')
            ->leftJoin('org','org.org_id = waybill.deliver_id')
            ->whereNotNull('waybill_num')
            ->where($where)
            ->whereIn('ord_route',$route)
//            ->where('waybill.deliver_id',1)
            ->field('waybill_num,order.ord_num,yz_send,waybill_id,waybill.deliver_id,youzan_id,ord_route,order.ord_id,waybill.sku_id')->select();

        if($debug)
            dump($waybill);

        if($waybill)
            foreach ($waybill as $v){
                $oid = OrdDetail::where('ord_id',$v->ord_id)->whereIn('sku_id',$v['sku_id'])->column('oid');
                $deliver = OrgModel::find($v['deliver_id']);
                $r = (new YouZanService())->sendGoods([
                    'out_stype'=>(string)$deliver->youzan_id,
                    'out_sid'=>$v['waybill_num'],
                    'tid'=>$v->ord_num,
                    'oids'=>implode(',',$oid),
                ]);
                if($r){
//                    dump($v);
                    Waybill::update(['yz_send'=>1],['waybill_id'=>$v->waybill_id]);
                }
                if($debug)
                    halt($r);
            }
        exit('处理完成');
    }

    public function logistics()
    {
        $youzan = new YouZanService(config('youzan.fs_tv'));
        $r = $youzan->logistics();
        halt($r);
    }

    /**
     * 获取话务流水
     */
    public function traffic()
    {
        set_time_limit(0);
        $page = 1;
        $limit = 100;
        $date = date('Y-m-d',strtotime('-1 day'));
        $sCallTime = $this->request->param('start')?:"$date 00:00:00";
        $eCallTime = $this->request->param('end')?:date("$date 23:59:59");
        $on_work = $this->request->param('on_work','09:00:00 - 18:00:00');
        $on_work = $on_work?:config('traffic')['on_work'];
        $off_work = $this->request->param('off_work',config('traffic')['off_work']);

        $query = [
            'page'=>$page,
            'rows'=>$limit,
            'search_GTE_callTime'=>$sCallTime,
            'search_LTE_callTime'=>$eCallTime,
            'search_LTE_callTime'=>$eCallTime,
        ];
//        dump($query);
        $rows = getTrafficData($query);
//        halt($rows);

        $page_sum = ceil($rows['total']/$limit);
        for ($page;$page<=$page_sum;$page++){
            $query['page'] = $page;
            $rows = getTrafficData($query);

            foreach ($rows['rows'] as &$v){
                if(!in_array($v['caller'],config('traffic_number')))
                    continue;

                if($v['callType']==1){//呼入
                    //标记重复呼入
                    $repeat = TrafficModel::where('called',$v['called'])
                        ->where('callType',1)
                        ->where('callTime','>=',date('Y-m-d H:i:s',strtotime($v['callTime'].'- 30 minute')))
                        ->where('callTime','<',$v['callTime'])
                        ->find();
                    $v['repeat'] = $repeat?1:0;
                    if(!$v['repeat']){
                        $v = trafficStatusByCalled($v,$on_work,$off_work);
                    }

                }else{//呼出
                    trafficStatusByCall($v,$on_work,$off_work);
                }
                $v['deal'] = 1;
//                halt($v);
                $exist = TrafficModel::where('connectionid',$v['connectionid'])->find();
                if(!$exist){
                    TrafficModel::create($v);
                }else{
                    $v['comments'] = $exist->comments?:$v['comments'];
                    TrafficModel::update($v,['traffic_id'=>$exist['traffic_id']]);
                }
                $v['callType'] = isset(config('callType')[$v['callType']])?config('callType')[$v['callType']]:'未知';
            }
        }
        exit('处理成功');
    }

    public function checkTrafficRepeat()
    {
        set_time_limit(0);
        $rows = TrafficModel::where('callType',1)
            ->where('repeat',0)
            ->where('deal',0)
            ->order('callTime asc')
//            ->limit(0,3000)
            ->select();
//        halt($rows);
        foreach($rows as $v){
            $v->deal = 1;
            $v->save();

            //标记重复呼入
            $time = date('Y-m-d H:i:s',strtotime($v['callTime'].'+ 30 minute'));
            $arr = TrafficModel::where('called',$v['called'])
                ->where('callTime','>',$v['callTime'])
                ->where('callTime','<=',$time)
                ->select();
            if(count($arr)==0)
                continue;

            TrafficModel::where('called',$v['called'])
                ->where('callTime','>',$v['callTime'])
                ->where('callTime','<=',$time)->update(['repeat'=>1]);
            //
        }
        exit('处理完成');
    }

    //处理呼入
    public function setCalledStatus()
    {
        set_time_limit(0);
        $rows = TrafficModel::where('callType',1)
            ->where('repeat',0)
            ->where('status',0)
            ->order('callTime asc')
//            ->limit(0,3000)
            ->select();

        foreach($rows as $v){
            trafficStatusByCalled($v);
//            if($v['code']){//已处理
//                $v->status = 1;
//            }elseif (!$v['code']){
//                if(strtotime($v['callTime'])>strtotime(date('Y-m-d 18:30:00',strtotime($v['callTime'])))){
//                    $date = date('Y-m-d 18:30:00',strtotime($v['callTime']));
//                    $between = [
//                        $date,
//                        date('Y-m-d H:i:s',strtotime($date."+ 18 hour"))
//                    ];
//                }elseif (strtotime($v['callTime'])>strtotime(date('Y-m-d 8:30:00',strtotime($v['callTime'])))){
//                    $date = date('Y-m-d 8:30:00',strtotime($v['callTime']));
//                    $between = [
//                        $date,
//                        date('Y-m-d H:i:s',strtotime($date."+ 10 hour"))
//                    ];
////                    dump($between);
//                }
//                $reply = TrafficModel::field("count(if(callResult='接通',true,null)) reply,count(if(callResult!='接通',true,null)) replyNoAnswer")
//                    ->where('callType',2)
//                    ->where('called',$v['called'])
//                    ->whereBetween('callTime',$between)
//                    ->find()
//                    ;
//                if(!$reply['reply'] && !$reply['replyNoAnswer']){
//                    $v->status = 2;
//                }else{
//                    $v->status = $reply['reply']?3:4;
////                    dump($v->called);
////                    halt($v->status);
//                }
////                halt($v->status);
//            }
////            dump($v->status);
//            $v->save();

        }

        exit('处理完成');
    }

    //处理呼出
    public function setCallStatus()
    {
        set_time_limit(0);
        $rows = TrafficModel::where('callType',2)
            ->where('deal',0)
            ->order('callTime asc')
//            ->limit(0,3000)
            ->select();

        foreach ($rows as $v){
            $v->deal = 1;
            trafficStatusByCall($v);
            $v->save();
        }
        exit('处理完成');
    }

    public function waybillResetTrade()
    {
        set_time_limit(0);
        $field = [
            'waybill_id',
            'spu.weight',
            'quantity',
            'spu_length',
            'spu_width',
            'spu_height',
            'reciever_addr',
            'order.collection_payment',
        ];

        $rows = Waybill::leftJoin('order','order.ord_id = waybill.ord_id')
            ->leftJoin('spu','spu.spu_id = order.spu_id')
            ->field($field)
//            ->where('waybill.is_deleted',0)
//            ->whereNull('trade_fee')
//            ->limit(1)
            ->select();
//        halt($rows);
        if(count($rows)>0)
            foreach($rows as $v){
                $v = charge($v);
//                halt($v);
                $v->save();
            }

        exit('处理完成');
    }

    public function waybill_intercept()
    {
        $waybill = Order::leftJoin('waybill','waybill.ord_id = order.ord_id')
            ->where('ord_type',6)
            ->where('is_cancel',0)
            ->select()
            ;

        if($waybill)
            foreach ($waybill as $v){
                $r = waybill_cancel($v['waybill_id']);
                if($r['stateCode']==100){
                    $waybill->cancel_reason = '拦截';//$post['cancel_reason'];
                    $waybill->is_cancel = 1;
                    $waybill->cancel_at = time();
                    $waybill->save();
                }
            }
        exit('处理完成');
    }

    public function create_waybill()
    {
//        $order = Order::hasWhere('waybill','waybill_id is null')->select();
        $order = Order::field('order.*')
            ->leftJoin('waybill','waybill.ord_id = order.ord_id')
            ->whereNull('waybill_id')->whereIn('ord_type',[1,2,3])
            ->whereNotIn('ord_route',6)
            ->select();
//        halt($order);
        foreach ($order as $v) {
            if(count(explode(',',$v['spu_id']))>1)
                continue;
            $address = MemberAddress::find($v['address_id']);
            $member = Member::find($v['member_id']);
            $spu = Spu::find($v['spu_id']);
            $wh = Warehouse::find($spu['wh_id']);

//            $org = OrgModel::find($wh['org_id']);
            $waybill = [
                'ord_id'=>$v['ord_id'],
                'deliver_id'=>$v['deliver_id'],
                'spu_id'=>$v['spu_id'],
                'sku_id'=>$v['sku_id'],
                'wh_id'=>$spu['wh_id'],
                'ord_num'=>$v['ord_num'],
                'status_text'=>'待出库',
                'collection_payment'=>$v['collection_payment'],
                'reciever'=>$member['member_name'],
                'reciever_phone'=>$member['member_phone'],
                'reciever_tel'=>$member['member_tel'],
                'reciever_addr'=>getAddrName($address['province']).getAddrName($address['city']).getAddrName($address['county']).getAddrName($address['street']).$address['detail'],
                'good_count'=>$v['quantity'],
                'package_num'=>$spu['package_num']>1?ceil($v['quantity']/$spu['package_num']):$v['quantity'],//包裹数量
            ];
            Waybill::create($waybill);
        }
        exit('操作完成');
    }

    public function change_route()
    {
        Order::leftJoin('ord_detail','ord_detail.ord_id = order.ord_id')
            ->whereIn('ord_detail.spu_num',['600001','600002','600003','600004','600005','600006','600007','600008'])
            ->update(['ord_route'=>7]);

        exit('操作完成');
    }

    protected function stock($ord_detail)
    {
        foreach ($ord_detail as $v){
            Products::where('product_id',$v->sku_id)->setInc('back_stock',$v->quantity);
        }
    }

    public function waybill_handle()
    {
        $waybill = Waybill::where('deliver_id',1)->whereNull('promise_time_type')->order('waybill_id desc')->select();

        foreach ($waybill as $v){
            $curl_log = CurlLog::where('curl_url','jingdong.ldop.waybill.receive')
                ->whereLike('response_data',"%$v->waybill_num%")->order('curl_id asc')->value('response_data');
            $curl_log = json_decode($curl_log,true);
            if(!empty($curl_log['jingdong_ldop_waybill_receive_responce']['receiveorderinfo_result']['resultCode'])
                &&$curl_log['jingdong_ldop_waybill_receive_responce']['receiveorderinfo_result']['resultCode']==100){
                $data = $curl_log['jingdong_ldop_waybill_receive_responce']['receiveorderinfo_result'];
                $v->promise_time_type = !empty($data['promiseTimeType'])?$data['promiseTimeType']:0;
                $v->pre_sort_result = !empty($data['preSortResult'])?$data['preSortResult']:[];
                $v->trans_type = !empty($data['transType'])?$data['transType']:0;
                $v->save();
            }
        }
    }
}
