<?php

namespace app\admin\controller;

use app\common\controller\AdminController;
use function Sodium\library_version_major;
use think\Controller;
use think\Request;
use app\common\model\Member;
use app\common\model\Traffic as TrafficModel;

class Traffic extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this->model = new TrafficModel();
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    /*public function index()
    {
        if($this->request->isAjax()){
            $page = $this->request->post('page',1);
            $limit = $this->request->post('limit',100);
            $call_at = $this->request->post('select.call_at');
            $call_at = $call_at?explode(' - ',$call_at):'';
            $sCallTime = $call_at?$call_at[0].' 00:00:00':date('Y-m-d 00:00:00');
            $eCallTime = $call_at?$call_at[1].' 23:59:59':date('Y-m-d 23:59:59');
            $callResult = $this->request->post('select.callResult');
            $callType = $this->request->post('select.callType');
//            $callTypes = $request->post('select.callType');
//            $order = $request->post('order','desc');
            $empCode = $this->request->post('select.empCode');
            $caller = $this->request->post('select.caller');
            $called = $this->request->post('select.called');
            $connectionid = $this->request->post('select.connectionid');

            $query = [
                'page'=>$page,
                'rows'=>$limit,
            ];

            if($sCallTime)
                $query['search_GTE_callTime'] = $sCallTime;
            if($sCallTime)
                $query['search_LTE_callTime'] = $eCallTime;
            if($callResult)
                $query['search_EQ_callResult'] = $callResult;
            if($callType){
                $query['search_EQ_callType'] = $callType;
//                $query['callTypes'] = $callTypes;
            }
            if($connectionid)
                $query['search_EQ_serviceconnectionid'] = $connectionid;
            if($empCode)
                $query['search_EQ_emp'] = $empCode;
            if($caller)
                $query['search_EQ_caller'] = $caller;
            if($called)
                $query['search_EQ_called'] = $called;

//            dump($query);
            $rows = getTrafficData($query);

            foreach ($rows['rows'] as $k=>&$v){
//                $exist = TrafficModel::where('connectionid',$v['connectionid'])->find();
//                if(!$exist){
//                    if($v['callType']==1 && $v['code']){//呼入已处理
//                        $v['status'] = 1;
//                    }elseif ($v['callType']==1 && !$v['code']){//未处理
//                        $v['status'] = 2;
//                    }elseif ($v['callType']==2 && $v['callResult']=='接通'){//已回访
//                        $v['status'] = 3;
//                    }
//                    elseif ($v['callType']==2 && $v['callResult']!='接通'){//已回访未接通
//                        $v['status'] = 4;
//                    }
//                    TrafficModel::create($v);
//                }else{
//                    $v['comments'] = $exist->comments?:$v['comments'];
//                    TrafficModel::update($v,['traffic_id'=>$exist['traffic_id']]);
//                }
                if(!empty(config('traffic_number'))&&!in_array($v['caller'],config('traffic_number'))){
                    unset($rows['rows'][$k]);continue;
                }

                $v['callType'] = isset(config('callType')[$v['callType']])?config('callType')[$v['callType']]:'未知';
            }
//            halt($rows);
            if($rows){
                return __successData('success',$rows['rows'],1,$rows['total'],$limit,$page);
            }else{
                return __error('failed');
            }

        }

        return $this->fetch();
    }*/

    public function index()
    {
        if($this->request->isAjax()){
            $limit = $this->request->param('limit',100);
            $search = $this->request->param('select',[]);

            $whereSql = '';
            $where = [
                ['caller','in',config('traffic_number')],
            ];
            if(!empty($search['traffic_type'])){
                $whereSql = "find_in_set($search[traffic_type],traffic_type)";
                unset($search['traffic_type']);
            }
            $rows = $this->model->withJoin('spu')->where(get_query_condition($where,$search))->where($whereSql)->order('callTime desc')->paginate($limit)->each(function ($v){
                $v->callType = getEnum($v->callType,'callType');
                if($v->traffic_type){
                    $traffic_type_text = [];
                    foreach ($v->traffic_type as $vv){
                        $traffic_type_text[] = getEnum($vv,'traffic_type');
                    }
                    $v->traffic_type_text = implode(',',$traffic_type_text);
                }
            });

            return __successData('success',$rows->items(),1,$rows->total(),$limit,$rows->currentPage());
        }

        $var = [
            'spu'=>\app\common\model\Spu::where(['is_deleted'=>0,'on_sale'=>1])->select(),
        ];

        return $this->fetch('',$var);
    }
    
    public function download($connectionid)
    {
        $row = trafficDownload($connectionid);

        if($row['success']){
            return __successData($row['message'],['entity'=>$row['entity']]);
        }else{
            return __error($row['message']);
        }
    }

    public function read(Request $request)
    {
        $id = $request->get('id');
        $phone = $request->get('phone');
        $member = Member::where('member_phone',$phone)->find();
        $query = [
            'search_EQ_serviceconnectionid'=>$id,
            'search_GTE_callTime'=>date('Y-m-d 00:00:00'),
            'search_LTE_callTime'=>date('Y-m-d 23:59:59'),
        ];

        $traffic = $rows = getTrafficData($query);
//        dump($traffic);

        if($rows['rows']){
            $exist = TrafficModel::where('connectionid',$rows['rows'][0]['connectionid'])->find();
            if($exist){
                TrafficModel::where('connectionid',$id)->update($rows['rows'][0]);
            }else{
//                dump('保存成功');
                if($rows['rows'][0]){
                    TrafficModel::create($rows['rows'][0]);
                }else{
                    TrafficModel::create(['connectionid'=>$id]);
                }
            }

        }

        $var = [
            'member'=>$member,
            'traffic'=>$traffic,
        ];

        return $this->fetch('form',$var);
    }

    public function save(Request $request)
    {
        $post = $request->post();

        if(empty($post['member_name']))
            return __error('请填写客户名');
        if(empty($post['member_phone']))
            return __error('请填写联系方式');
        if(empty($post['traffic_type']))
            return __error('请选择话务类型');
        if(empty($post['connectionid']))
            return __error('缺少通话流水ID');
//        if(empty($post['comments']))
//            return __error('请填写沟通内容');
//        if(empty($post['member_id']))
//            return __error('缺少客户ID');

//        $member = Member::where('member_phone',$post['member_phone']);
//        if($member){
//            $member->member_name = $post['member_name'];
//            $member->save();
//        }else{
//            Member::create($post);
//        }

        TrafficModel::where('connectionid',$post['connectionid'])->update($post);

        return __success('保存成功');
    }

    public function export(Request $request)
    {
        set_time_limit(0);
        $page = $request->param('page',1);
        $limit = $request->param('limit',100);
        $call_at = $request->param('call_at');
        $call_at = $call_at?explode(' - ',$call_at):'';
        $sCallTime = $call_at?$call_at[0].' 00:00:00':date('Y-m-d 00:00:00');
        $eCallTime = $call_at?$call_at[1].' 23:59:59':date('Y-m-d 23:59:59');
        $callResult = $request->param('callResult');
        $callType = $request->param('callType');
//            $callTypes = $request->post('select.callType');
//            $order = $request->post('order','desc');
        $empCode = $request->param('empCode');
        $caller = $request->param('caller');
        $called = $request->param('called');
        $connectionid = $request->param('connectionid');

        $query = [
            'page'=>$page,
            'rows'=>$limit,
        ];

        if($sCallTime)
            $query['search_GTE_callTime'] = $sCallTime;
        if($sCallTime)
            $query['search_LTE_callTime'] = $eCallTime;
        if($callResult)
            $query['search_EQ_callResult'] = $callResult;
        if($callType){
            $query['search_EQ_callType'] = $callType;
//                $query['callTypes'] = $callTypes;
        }
        if($connectionid)
            $query['search_EQ_serviceconnectionid'] = $connectionid;
        if($empCode)
            $query['search_EQ_emp'] = $empCode;
        if($caller)
            $query['search_EQ_caller'] = $caller;
        if($called)
            $query['search_EQ_called'] = $called;

        $rows = getTrafficData($query);
//        halt($rows);
        $page_sum = ceil($rows['total']/$limit);
        $data = [];

        for ($page;$page<=$page_sum;$page++){
            $query['page'] = $page;
            $rows = getTrafficData($query);
            foreach ($rows['rows'] as &$v){
                if(!empty(config('traffic_number')) && !in_array($v['caller'],config('traffic_number')))
                    continue;
                $v['callType'] = isset(config('callType')[$v['callType']])?config('callType')[$v['callType']]:'未知';
//                $comments = TrafficModel::where('connectionid',$v['connectionid'])->value('comments');
//                $v['comments'] = $comments?:$v['comments'];
////                $v['callResult'] = isset(config('callResult')[$v['callResult']])?config('callResult')[$v['callResult']]:'未知';
//                $order = \app\common\model\Order::where('connection_id',$v['connectionid'])->find();
//                if($order)
//                    $spu = \app\common\model\Spu::find($order->spu_id);
                array_push($data,[
                    'connectionid'=>$v['connectionid'],
                    'callType'=>$v['callType'],
                    'callResult'=>$v['callResult'],
                    'caller'=>$v['caller'],
                    'called'=>$v['called'],
                    'callTime'=>!empty($v['callTime'])?date('Y-m-d H:i:s',strtotime($v['callTime'])):'',
                    'connectTime'=>!empty($v['connectTime'])?date('Y-m-d H:i:s',strtotime($v['connectTime'])):'',
                    'hangupTime'=>!empty($v['hangupTime'])?date('Y-m-d H:i:s',strtotime($v['hangupTime'])):'',
                    'times'=>$v['times'],
                    'comments'=>$v['comments'],
//                    'ord_route'=>!empty($order['ord_route'])?$order['ord_route']:'',
//                    'spu_name'=>!empty($spu['spu_name'])?$spu['spu_name']:'',
//                    'ord_num'=>!empty($order['ord_num'])?$order['ord_num']:'',
//                    'ord_money'=>!empty($order['ord_money'])?$order['ord_money']:'',
                    'code'=>$v['code'],
                ]);
            }
        }
//        halt($data);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=话务明细.csv' );
        header('Cache-Control: max-age=0');

        $title = [
            '流水ID','呼叫类型','呼叫结果','主叫号码','被叫号码','呼入时间','接通时间','挂机时间','通话时长(秒)','客服备注',
            /*'通路','商品名','订单号','订单金额',*/'坐席'
        ];
        put_csv($data,$title);
    }

    public function local()
    {
        $status = $this->request->param('status',1);
        if($this->request->isAjax()){
            $limit = $this->request->param('limit');
            $start = $this->request->param('start',date('Y-m-d 00:00:00'));
            $end = $this->request->param('end',date('Y-m-d 23:59:59'));

            $field = [
                'traffic_id id',
                'callTime',
                'called',
                'code',
            ];

            $where = [
                ['callType','=',1],
                ['callTime','between',[$start,$end]],
                ['status','=',$status],
                ['repeat','=',0],
            ];

            if(!empty(config('traffic_number')))
                $where[] = ['caller','in',config('traffic_number')];

            $rows = TrafficModel::field($field)->where($where)->order('callTime desc')->select();

            return __successData('success',$rows);
        }

        $var = [
            'status'=>$status,
            'status_text'=>config('traffic_status')[$status],
        ];
        return $this->fetch('',$var);

    }

    public function respond($id)
    {
        if($this->request->isAjax()){
            $row = TrafficModel::find($id);

            $where = [
                ['callType','=',2],
                ['called','=',$row['called']],
                ['callTime','>',$row['callTime']],
            ];
            $respond = TrafficModel::where($where)->order('callTime asc')->select();

            return __successData('success',$respond);
        }

        return $this->fetch('');
    }

    public function traffic_type()
    {
        $param = $this->request->param();

        $r = $this->model->update($param);
        return $r?__success('操作成功'):__error('操作失败');
    }
}


