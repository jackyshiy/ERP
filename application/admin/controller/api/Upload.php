<?php

namespace app\admin\controller\api;

use app\common\controller\AdminController;
use think\Request;

class Upload extends AdminController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        if($this->request->isAjax()){
            $type = $this->request->post('type');
            if($type=='excel'){
                $module = $this->request->post('module');
//                dump($module);
                if(!$module)
                    return __error('请选择类型');
                if(!isset(config('excel')[$module]))
                    return __error("$module 配置不存在");
                $path = './uploads'.config('excel')[$module]['path'];
                $r = upload($this->request->file('excel'),$path,['ext'=>'xls,xlsx']);
                if($r['code'])
                    return $r;
                $excel_title = excelImport($path.$r['data']['saveName'],config('excel')[$module]['import'],1,1);//excel表头
                if($excel_title['code'])
                    return $excel_title;
//                dump(config('excel')[$module]['model']);
//                dump($excel_title['data']['data']);
//                dump(array_diff($excel_title['data']['data'],config('excel')[$module]['model']));
                $diff = array_diff(array_filter($excel_title['data']['data']),config('excel')[$module]['model']);
                $diff2 = array_diff(config('excel')[$module]['model'],array_filter($excel_title['data']['data']));
//                halt($diff2);
                if(count($diff)>0 || count($diff2)>0){
                    return __error('上传文件与模板不符('.($diff?implode(',',$diff).',':'').($diff2?implode(',',$diff2):'').')');
                }
                if(!$r['code']){
                    $r['data']['src'] = $path.$r['data']['saveName'];
                    $r['data']['total'] = $excel_title['data']['total'];
                }
                return $r;
            }elseif($type == 'img'){
                $path = './uploads/refund/';
                $r = upload($this->request->file('file'),$path,['ext'=>'gif,jpg,jpeg,png']);
                $r['data']['url'] = $this->request->domain().'/'.$path.$r['data']['saveName'];
                $r['data']['saveName'] = $path.$r['data']['saveName'];
                return $r;
            }
        }
    }

}
