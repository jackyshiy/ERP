<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;

class Spu extends Controller
{

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        if($request->isPost()){
            halt($request->post());
            $row = $request->post('product');
            $validate = $this->validate($row,'app\admin\validate\Products.save');
            if (true !== $validate) return __error($validate);
            $id = $row['product_id'];
            unset($row['product_id']);
            if($row['stock']==0)
                $row['on_sale'] = 0;
            $row['create_by'] = $row['update_by'] = session('user.id');
//            dd($row);die;
            if($id){
                $this->model->save($row,['product_id'=>$id]);
            }else{
                $this->model->save($row);
            }
            $row = [
                'log_type'=>'stock',
                'product_id'=>$id,
                'num'=>$row['stock'],
                'log_detail'=>"商品 $row[product_num] ，首次添加 ，良品库存 $row[stock] ，不良品库存0",
            ];
            SystemLog::create(save_common_field($row));
            return __success('操作成功');
        }
    }

    public function getWhBySpuId($spu_id)
    {
        $spu = \app\common\model\Spu::find($spu_id);
        $wh = \app\common\model\Warehouse::where('is_deleted',0)->whereIn('wh_id',$spu['wh_id'])->select();

        return __success('success',$wh);
    }
}
