<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use app\common\model\ShoppingWebsite;

class Shopping extends Controller
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $var = [
            'website'=>ShoppingWebsite::all(),
        ];
        return $this->fetch('',$var);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $post = $this->request->post();
        $row = ShoppingWebsite::where('title',$post['title'])->find();
        if($row){
            $row->url = $post['url'];
            $row->save();
            return __successData('修改成功');
        }else{
            $row = ShoppingWebsite::create($post);
            return __successData('添加成功',$row);
        }

    }

}
