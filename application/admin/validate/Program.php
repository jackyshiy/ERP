<?php
// +----------------------------------------------------------------------
// | 99PHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018~2020 https://www.99php.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Mr.Chung <chung@99php.cn >
// +----------------------------------------------------------------------

namespace app\admin\validate;

use think\Validate;
use app\common\model\Program as ProgramModel;
/**
 * 订单管理验证类
 * Class Order
 * @package app\admin\validate
 */
class Program extends Validate {

    /**
     * 验证规则
     * @var array
     */
    protected $rule = [
        'id'=>'require|number|exist',
        'product_id'=>'require',
        'program_name'     => 'require',
        'channel'    => 'require',
        'present_at'  => 'require',
        'duration'   => 'require|number|gt:0',
        'position'   => 'require',
        'live_at'   => 'require',
    ];

    /**
     * 错误提示
     * @var array
     */
    protected $message = [
        'program_name.require'   => '节目名称必须填写',
        'channel.require'    => '节目频道必须填写',
        'present_at.require'      => '播放时间必须填写',
        'duration.require'     => '播放时长必须为数字',
        'duration.number'   => '播放时长为数字',
        'duration.gt'   => '播放时长必须大于0',
        'position.require' => '节目位置必须填写',
        'gross_profit.require' => '毛利目标必须填写',
        'product_id.require'=>'请选择商品',
//        'product_id.number'=>'商品ID错误',
        'live_at.require'   => '请填写有效日期',
    ];

    /**
     * 应用场景
     * @var array
     */
    protected $scene = [
        //添加或编辑
        'save'  => ['program_name', 'channel', 'present_at', 'duration', 'position','gross_profit','product_id'],

        //删除
        'del'  => ['id'],

    ];

    /**
     * 检测菜单是否存在
     * @param $value
     * @param $rule
     * @param array $data
     * @return bool|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
//    protected function checkId($value, $rule, $data = []) {
//        $menu = \app\common\model\menu::where(['id' => $value])->find();
//        if (empty($menu)) return '暂无菜单数据，请稍后再试！';
//        return true;
//    }

    protected function exist($value)
    {
        $exist = ProgramModel::where(['program_id'=>$value])->count();
        if(!$exist) return '暂无节目数据，请稍后再试！';
        return true;
    }
}