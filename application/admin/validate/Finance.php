<?php

namespace app\admin\validate;

use think\Validate;

class Finance extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
        'waybill_id'=>'require',
	    'waybill_num'=>'require',
        'balance_weight'=>'require',
        'balance_volume'=>'require',
        'balance_volume_weight'=>'require',
        'balance_at'=>'require',
        'collect_fee'=>'require',
        'poundage'=>'require',
        'discount'=>'require',
        'damages'=>'require',
        'trade_fee'=>'require',
        'service_fee'=>'require',
        'save_fee'=>'require',
        'pay'=>'require',
        'package_fee'=>'require',
        'addition_fee'=>'require',
        'money'=>'require',
        'money_type'=>'require',
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        'waybill_id.require'=>'运单ID不能为空',
        'waybill_num.require'=>'运单号不能为空',
        'balance_weight.require'=>'计费重量不能为空',
        'balance_volume.require'=>'体积不能为空',
        'balance_volume_weight.require'=>'体积重不能为空',
        'balance_at.require'=>'计费时间不能为空',
        'collect_fee.require'=>'代收货款不能为空',
        'poundage.require'=>'代收货款手续费不能为空',
        'discount.require'=>'折扣费不能为空',
        'damages.require'=>'赔偿费不能为空',
        'trade_fee.require'=>'运费不能为空',
        'service_fee.require'=>'服务费不能为空',
        'save_fee.require'=>'保价金额不能为空',
        'pay.require'=>'应付不能为空',
        'package_fee.'=>'包装费不能为空',
        'addition_fee.require'=>'高峰期附加费不能为空',
        'fee.require'=>'费用金额不能为空',
        'fee_type.require'=>'费用类型不能为空',
    ];

    /**
     * 应用场景
     * @var array
     */
    protected $scene = [
        //添加
        'add'  => ['waybill_num', 'balance_weight', 'balance_volume', 'balance_volume_weight', 'balance_at'],

        //修改
        'edit' => ['id', 'pid', 'title', 'href', 'icon', 'sort', 'remark'],

        'finance' => ['collect_fee', 'finance_at'],

        'trade' => ['waybill_num', 'trade_at','balance_weight','balance_volume','fee','fee_type'],

        //删除
        'del'  => ['id'],

        //更改状态
        'status'  => ['id'],

        //修改字段值
        'edit_field' => ['id', 'field', 'value'],
    ];
}
