<?php
// +----------------------------------------------------------------------
// | 99PHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018~2020 https://www.99php.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Mr.Chung <chung@99php.cn >
// +----------------------------------------------------------------------

namespace app\admin\validate;

use think\Validate;

/**
 * 订单管理验证类
 * Class Order
 * @package app\admin\validate
 */
class Waybill extends Validate {

    /**
     * 验证规则
     * @var array
     */
    protected $rule = [
        'ord_num'     => 'require',
        'waybill_num'    => 'require',
        'reciever'  => 'require',
        'reciever_phone'   => 'require',
        'reciever_addr'   => 'require',
        'predict_fee'   => 'require',
    ];

    /**
     * 错误提示
     * @var array
     */
    protected $message = [
        'ord_num.require'   => '订单号不能为空',
        'waybill_num.require'    => '运单号不能为空',
        'reciever.require' => '收件人不能为空',
        'reciever_phone.require'     => '收件人联系方式不能为空',
        'reciever_addr.require'      => '收件人地址不能为空',
        'predict_fee.require'     => '运费预估不能为空',
    ];

    /**
     * 应用场景
     * @var array
     */
    protected $scene = [
        //添加
        'add'  => ['ord_num', 'waybill_num', 'reciever', 'reciever_phone', 'reciever_addr', 'predict_fee'],

        //修改
        'edit' => ['id', 'pid', 'title', 'href', 'icon', 'sort', 'remark'],

        //删除
        'del'  => ['id'],

        //更改状态
        'status'  => ['id'],

        //修改字段值
        'edit_field' => ['id', 'field', 'value'],
    ];

    /**
     * 检测菜单是否存在
     * @param $value
     * @param $rule
     * @param array $data
     * @return bool|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
//    protected function checkId($value, $rule, $data = []) {
//        $menu = \app\common\model\menu::where(['id' => $value])->find();
//        if (empty($menu)) return '暂无菜单数据，请稍后再试！';
//        return true;
//    }
}