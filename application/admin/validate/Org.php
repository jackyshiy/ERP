<?php

namespace app\admin\validate;

use think\Validate;

class Org extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    'org_id'=>'require|number',
        'org_name'=>'require',
        'org_master'=>'require',
        'org_phone'=>'require|number',
        'org_sender'=>'require',
        'org_sender_phone'=>'require|number',
        'org_sender_addr'=>'require',
        'jd_app_key'=>'require',
        'jd_app_secret'=>'require',
        'jd_customer_code'=>'require',
        'jd_ware_house_code'=>'require',
        'jd_sale_plat'=>'require',
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        'org_id.require'=>'缺少机构ID',
        'org_id.number'=>'机构ID必须为数字',
        'org_name.require'=>'请填写机构名字',
        'org_master.require'=>'请填写负责人',
        'org_phone.require'=>'请填写负责人手机',
        'org_phone.number'=>'手机必须为数字',
        'org_sender.require'=>'请填写寄件人姓名',
        'org_sender_phone.require'=>'请填写寄件人手机',
        'org_sender_phone.number'=>'寄件人手机必须为数字',
        'org_sender_addr.require'=>'请填写寄件人地址',
        'jd_app_key.require'=>'请填写京东appKey',
        'jd_app_secret.require'=>'请填写京东appSecret',
        'jd_customer_code.require'=>'请填写京东客户编码',
        'jd_ware_house_code.require'=>'请填写京东发货仓号码，具体请咨询京东',
        'jd_sale_plat.require'=>'请填写京东销售平台，具体请咨询京东',
    ];

    protected $scene = [
        'save'=>[
            'org_name','org_master','org_phone',
//            'jd_app_key','jd_app_secret','jd_customer_code','jd_ware_house_code','jd_sale_plat'
        ],

    ];
}
