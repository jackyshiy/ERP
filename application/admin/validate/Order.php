<?php
// +----------------------------------------------------------------------
// | 99PHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018~2020 https://www.99php.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Mr.Chung <chung@99php.cn >
// +----------------------------------------------------------------------

namespace app\admin\validate;

use think\Validate;

/**
 * 订单管理验证类
 * Class Order
 * @package app\admin\validate
 */
class Order extends Validate {

    /**
     * 验证规则
     * @var array
     */
    protected $rule = [
        'ord_num'     => 'require',
        'good_num'    => 'require',
        'good_name'  => 'require',
        'receiver'   => 'require',
        'phone'   => 'require|number',
        'addr'   => 'require',
        'prc'   => 'require|number',
        'ord_money' => 'require|number',
    ];

    /**
     * 错误提示
     * @var array
     */
    protected $message = [
        'ord_num.require'   => '订单号必须',
        'good_num.require'    => '商品号必须',
        'good_name.require' => '商品名必须',
        'receiver.require'     => '客户名必须',
        'phone.require'      => '联系方式必须',
        'phone.number'     => '联系方式必须为数字',
        'prc.number'   => '单价必须为数字',
        'prc.require' => '单价必须',
        'ord_money.require' => '订单金额必须',
        'ord_money.number' => '订单金额必须为数字',
        'addr.require'   => '请填写详细地址',
    ];

    /**
     * 应用场景
     * @var array
     */
    protected $scene = [
        //添加
        'add'  => ['ord_num', 'good_num', 'good_name', 'receiver', 'phone', 'prc','ord_money'],

        //修改
        'edit' => ['id', 'pid', 'title', 'href', 'icon', 'sort', 'remark'],

        //修改
        'addr_edit' => ['ord_id','receiver','phone','addr'],

        //删除
        'del'  => ['id'],

        //更改状态
        'status'  => ['id'],

        //修改字段值
        'edit_field' => ['id', 'field', 'value'],
    ];

    /**
     * 检测菜单是否存在
     * @param $value
     * @param $rule
     * @param array $data
     * @return bool|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
//    protected function checkId($value, $rule, $data = []) {
//        $menu = \app\common\model\menu::where(['id' => $value])->find();
//        if (empty($menu)) return '暂无菜单数据，请稍后再试！';
//        return true;
//    }
}