<?php
// +----------------------------------------------------------------------
// | 99PHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018~2020 https://www.99php.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Mr.Chung <chung@99php.cn >
// +----------------------------------------------------------------------

namespace app\admin\validate;

use think\Validate;
use app\common\model\Products as ProductModel;
/**
 * 订单管理验证类
 * Class Order
 * @package app\admin\validate
 */
class Products extends Validate {

    /**
     * 验证规则
     * @var array
     */
    protected $rule = [
        'id'=>'require|number',
        'wh_id'=>'require',
        'spu_num'     => 'require',
        'spu_name'    => 'require',
        'org_price'  => 'require',
        'price'   => 'require',
        'stock'   => 'require|number',
        'on_sale'   => 'require',
        'profit_ratio'=>'require',
        'package_num'=>'require|gt0',
        'buy_limit'=>'require',
        'tax'=>'require',
        'balance_type'=>'require',
        'rule'=>'require',
    ];

    /**
     * 错误提示
     * @var array
     */
    protected $message = [
        'product_num.require'   => '商品编码必须填写',
        'product_name.require'    => '商品名称必须填写',
        'org_price.require'      => '商品原价必须填写',
        'org_price.number'     => '商品原价必须为数字',
        'price.number'   => '商品售价为数字',
        'price.require' => '商品售价必须填写',
        'stock.require' => '商品库存必须填写',
        'stock.number' => '商品库存必须为数字',
        'on_sale.require'=>'请选择是否上架',
        'profit_ratio.require'=>'请填写毛利系数',
        'package_num.require'=>'请填写包裹合并数量',
        'buy_limit.require'=>'请填写商品购买限制数量',
        'wh_id.require'=>'请选择仓库',
        'tax.require'=>'请填写税率',
        'balance_type.require'=>'请选择结算方式',
        'rule.require'=>'请配置车队规则',
    ];

    /**
     * 应用场景
     * @var array
     */
    protected $scene = [
        //添加或编辑
        'save'  => ['product_num', 'product_name', 'org_price', 'price', 'stock','profit_ratio','package_num','buy_limit','wh_id','tax','balance_type'],

        //删除
        'del'  => ['id'],

        //更改状态
        'on_sale'  => ['id'],

        //修改字段值
        'edit_field' => ['id', 'field', 'value'],
    ];

    /**
     * 检测菜单是否存在
     * @param $value
     * @param $rule
     * @param array $data
     * @return bool|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
//    protected function checkId($value, $rule, $data = []) {
//        $menu = \app\common\model\menu::where(['id' => $value])->find();
//        if (empty($menu)) return '暂无菜单数据，请稍后再试！';
//        return true;
//    }

//    protected function exist($value)
//    {
//        $exist = ProductModel::where(['product_id'=>$value])->count();
//        if(!$exist) return '暂无商品数据，请稍后再试！';
//        return true;
//    }

    protected function gt0($value)
    {
        if($value<=0)
            return '包裹合并数量必须大于0';
        return true;
    }
}