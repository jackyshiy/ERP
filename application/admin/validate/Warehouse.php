<?php

namespace app\admin\validate;

use think\Validate;

class Warehouse extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    'wh_id'=>'require',
        'org_id'=>'require',
        'wh_name'=>'require',
        'wh_address'=>'require',
        'wh_person'=>'require',
        'wh_phone'=>'require',
        'jd_ware_house_code'=>'require',
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        'wh_name.require'=>'请填写仓库名',
        'wh_address.require'=>'请填写仓库完整详细地址',
        'wh_person.require'=>'请填写联系人',
        'wh_phone.require'=>'请填写联系方式',
        'jd_ware_house_code.require'=>'请填写京东仓库码',
        'org_id.require'=>'请选择车队',
    ];

    protected $scene = [
        'save' =>['wh_name','wh_address','wh_person','wh_phone'],
    ];
}
