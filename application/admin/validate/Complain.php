<?php

namespace app\admin\validate;

use think\Validate;

class Complain extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    'complain_id'=>'require',
        'waybill_id'=>'require',
        'complain_content'=>'require',
        'complain_handle'=>'require',
//        'is_finished'=>'require',
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        'complain_id.require'=>'缺少客诉ID',
        'waybill_id.require'=>'缺少运单ID',
        'complain_content.require'=>'请填写客诉内容',
        'complain_handle.require'=>'请填写客诉处理内容',
    ];

    protected $scene = [
        'save'=>['complain_content','complain_handle'],
    ];
}
